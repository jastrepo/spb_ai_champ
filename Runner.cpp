//
// Created by nikita on 9/23/21.
//
#include <log/Loggers.h>

#include <utility>
#include "Runner.h"
#include "codegame/ServerMessage.hpp"
#include "codegame/ClientMessage.hpp"

Runner::Runner(const std::string &host, const std::string &port, const std::string &token,
               std::string threadName)
    : threadName(std::move(threadName)), tcpStream(host, port) {
  tcpStream.write(token);
  tcpStream.write(int(1));
  tcpStream.write(int(0));
  tcpStream.write(int(1));
  tcpStream.flush();
}

void Runner::run() {
  Loggers::logger("std", "log" + threadName + ".txt");
  while (true) {
    auto message = codegame::ServerMessage::readFrom(tcpStream);
    if (auto getActionMessage = std::dynamic_pointer_cast<codegame::ServerMessage::GetAction>(message)) {
      auto currentTick = getActionMessage->playerView.currentTick;
      Loggers::logger("std").startBlock("tick " + std::to_string(currentTick));

      if (tasksPtr == nullptr) {
        tasksPtr = std::make_unique<Tasks>();
      }

      tasksPtr->newTick(getActionMessage->playerView);

      if (strategyPtr == nullptr) {
        strategyPtr = std::make_unique<Strategy>(*tasksPtr);
      }
      if (currentTick == 0) {
        strategyPtr->firstTick();
      } else {
        strategyPtr->newTick(currentTick);
      }

      const auto &actions = model::Action(tasksPtr->strategyData.moveActions.get(),
                                          tasksPtr->strategyData.buildActions.builds, strategyPtr->specialty);

      codegame::ClientMessage::ActionMessage(actions).writeTo(tcpStream);
      tcpStream.flush();

      tasksPtr->finishTurn();
      Loggers::logger("std").endBlock();
    } else if (std::dynamic_pointer_cast<codegame::ServerMessage::Finish>(message)) {
      break;
    } else if (std::dynamic_pointer_cast<codegame::ServerMessage::DebugUpdate>(message)) {
      //myStrategy.debugUpdate(debugUpdateMessage->playerView, debugInterface);
      codegame::ClientMessage::DebugUpdateDone().writeTo(tcpStream);
      tcpStream.flush();
    }
  }
}