//
// Created by nikita on 11/25/21.
//
#pragma once

#include <ostream>

struct LogLine {
  std::ostream &out;

  explicit LogLine(std::ostream &out);

  ~LogLine();

  template<typename T>
  LogLine &operator<<(const T &message);
};

template<typename T>
LogLine &LogLine::operator<<(const T &message) {
  out << " " << message;
  out.flush();
  return *this;
}