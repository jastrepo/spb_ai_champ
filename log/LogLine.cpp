//
// Created by nikita on 11/25/21.
//
#include "LogLine.h"

LogLine::~LogLine() {
  out << std::endl;
}

LogLine::LogLine(std::ostream &out) : out(out) {}