//
// Created by nikita on 11/25/21.
//
#pragma once

#include <fstream>
#include <vector>
#include "LogLine.h"

class Loggers;

class Logger {
  std::string indent;
  std::vector<std::string> namesToPrint;
  std::ofstream &out;

  explicit Logger(std::ofstream &out);

public:
  template<typename T>
  LogLine operator<<(const T &message);

  void startBlock(const std::string &name);

  void endBlock();

  friend Loggers;
};

template<typename T>
LogLine Logger::operator<<(const T &message) {
  if (out.is_open()) {
    for (const auto &name: namesToPrint) {
      out << name;
    }
    namesToPrint.clear();
    out << indent << message;
  }
  return LogLine(out);
}