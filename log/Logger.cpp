//
// Created by nikita on 11/25/21.
//
#include "Logger.h"

Logger::Logger(std::ofstream &out) : out(out) {}

void Logger::startBlock(const std::string &name) {
  namesToPrint.emplace_back(indent + name + ":\n");
  indent += "  ";
}

void Logger::endBlock() {
  if (indent.size() > 1) {
    indent.erase(indent.size() - 2);
  }
  if (!namesToPrint.empty()) {
    namesToPrint.pop_back();
  }
}
