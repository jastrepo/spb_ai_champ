//
// Created by nikita on 11/25/21.
//

#include "Loggers.h"

Loggers &Loggers::singleton() {
  static thread_local Loggers loggers;
  return loggers;
}

Logger &Loggers::logger(const std::string &name) {
  auto &loggers = singleton();
  auto loggerIt = loggers.loggers.find(name);
  if (loggerIt == loggers.loggers.end()) {
    return logger(name, name);
  } else {
    return loggerIt->second.second;
  }
}

Logger &Loggers::logger(const std::string &name, const std::filesystem::path &logFile) {
  auto &loggers = singleton();

  std::ofstream newFile;
  std::string fileName = logFile.string();

#ifndef NDEBUG
  newFile.open(logFile);
  fileName = std::filesystem::canonical(logFile).string();
  {
    auto fileIt = loggers.files.find(fileName);
    if (fileIt != loggers.files.end()) {
      fileIt->second.close();
      loggers.files.erase(fileIt);
    }
  }
#endif

  {
    auto loggerIt = loggers.loggers.find(name);
    if (loggerIt != loggers.loggers.end()) {
      auto fileIt = loggers.files.find(loggerIt->second.first);
      if (fileIt != loggers.files.end()) {
        fileIt->second.close();
        loggers.files.erase(fileIt);
      }

      loggers.loggers.erase(loggerIt);
    }
  }

  loggers.files.emplace(fileName, std::move(newFile));
  loggers.loggers.emplace(name, std::make_pair(fileName, Logger(loggers.files.at(fileName))));

  return loggers.loggers.at(name).second;
}

void Loggers::close(const std::string &name) {
  auto &loggers = singleton();

  auto loggerIt = loggers.loggers.find(name);
  if (loggerIt != loggers.loggers.end()) {
    auto fileIt = loggers.files.find(loggerIt->second.first);
    if (fileIt != loggers.files.end()) {
      fileIt->second.close();
      loggers.files.erase(fileIt);
    }

    loggers.loggers.erase(loggerIt);
  }
}
