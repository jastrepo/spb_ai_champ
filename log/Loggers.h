//
// Created by nikita on 11/25/21.
//
#pragma once

#include <filesystem>
#include <unordered_map>
#include "Logger.h"

class Loggers {
  Loggers() = default;

  static Loggers &singleton();

  std::unordered_map<std::string, std::ofstream> files;
  std::unordered_map<std::string, std::pair<std::string, Logger>> loggers;
public:
  static Logger &logger(const std::string &name);

  static Logger &logger(const std::string &name, const std::filesystem::path &logFile);

  static void close(const std::string &name);
};