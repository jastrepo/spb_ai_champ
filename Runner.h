//
// Created by nikita on 9/23/21.
//
#pragma once

#include "TcpStream.hpp"
#include <memory>
#include <strategy/BalancerTestStrategy.h>
#include <strategy/PackageTestStrategy.h>
#include <strategy/FleeStrategy.h>
#include <strategy/AttackStrategy.h>
#include <strategy/RouteTestStrategy.h>
#include <strategy/FinalStrategy.h>

class Runner {
public:
  Runner(const std::string &host, const std::string &port, const std::string &token, std::string threadName = "");

  void run();

private:
  std::string threadName;
  using Strategy = FinalStrategy;
  std::unique_ptr<Strategy> strategyPtr;
  std::unique_ptr<Tasks> tasksPtr;
  TcpStream tcpStream;
};