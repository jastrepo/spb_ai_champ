#include "Stream.hpp"
#include <algorithm>
#include <cstring>

// Check if current machine is little endian
bool isLittleEndianMachine() {
  union {
    unsigned short value;
    char bytes[2];
  } test = {0x0201};
  return test.bytes[0] == 1;
}

bool IS_LITTLE_ENDIAN_MACHINE = isLittleEndianMachine();

// Read a bool from this stream
bool InputStream::readBool() {
  char buffer[1];
  readBytes(buffer, 1);
  return buffer[0] != 0;
}

// Read an int32 from this stream
int InputStream::readInt32() {
  char buffer[sizeof(int32_t)];
  readBytes(buffer, sizeof(int32_t));
  if (!IS_LITTLE_ENDIAN_MACHINE) {
    std::reverse(buffer, buffer + sizeof(int32_t));
  }
  return *reinterpret_cast<int32_t *>(buffer);
}

// Read an unsigned int32 from this stream
uint32_t InputStream::readUInt32() {
  return uint32_t(readInt32());
}

// Read an int64 from this stream
int64_t InputStream::readInt64() {
  char buffer[sizeof(int64_t)];
  readBytes(buffer, sizeof(int64_t));
  if (!IS_LITTLE_ENDIAN_MACHINE) {
    std::reverse(buffer, buffer + sizeof(int64_t));
  }
  return *reinterpret_cast<int64_t *>(buffer);
}

// Read a float from this stream
float InputStream::readFloat() {
  char buffer[sizeof(float)];
  readBytes(buffer, sizeof(float));
  if (!IS_LITTLE_ENDIAN_MACHINE) {
    std::reverse(buffer, buffer + sizeof(float));
  }
  return *reinterpret_cast<float *>(buffer);
}

// Read a double from this stream
double InputStream::readDouble() {
  char buffer[sizeof(double)];
  readBytes(buffer, sizeof(double));
  if (!IS_LITTLE_ENDIAN_MACHINE) {
    std::reverse(buffer, buffer + sizeof(double));
  }
  return *reinterpret_cast<double *>(buffer);
}

// Read a string from this stream
std::string InputStream::readString() {
  std::vector<char> buffer((size_t(readInt32())));
  readBytes(&buffer[0], buffer.size());
  return {&buffer[0], buffer.size()};
}

// Write a bool into this stream
void OutputStream::write(bool value) {
  char buffer[sizeof(bool)];
  std::memcpy(buffer, &value, sizeof(bool));
  writeBytes(buffer, sizeof(bool));
}

// Write an int32 into this stream
void OutputStream::write(int32_t value) {
  char buffer[sizeof(int32_t)];
  std::memcpy(buffer, &value, sizeof(int32_t));
  if (!IS_LITTLE_ENDIAN_MACHINE) {
    std::reverse(buffer, buffer + sizeof(int32_t));
  }
  writeBytes(buffer, sizeof(int32_t));
}

// Write an unsigned int32 into this stream
void OutputStream::write(uint32_t value) {
  write(int32_t(value));
}

// Write an int64 into this stream
void OutputStream::write(int64_t value) {
  char buffer[sizeof(int64_t)];
  std::memcpy(buffer, &value, sizeof(int64_t));
  if (!IS_LITTLE_ENDIAN_MACHINE) {
    std::reverse(buffer, buffer + sizeof(int64_t));
  }
  writeBytes(buffer, sizeof(int64_t));
}

// Write a float into this stream
void OutputStream::write(float value) {
  char buffer[sizeof(float)];
  std::memcpy(buffer, &value, sizeof(float));
  if (!IS_LITTLE_ENDIAN_MACHINE) {
    std::reverse(buffer, buffer + sizeof(float));
  }
  writeBytes(buffer, sizeof(float));
}

// Write a double into this stream
void OutputStream::write(double value) {
  char buffer[sizeof(double)];
  std::memcpy(buffer, &value, sizeof(double));
  if (!IS_LITTLE_ENDIAN_MACHINE) {
    std::reverse(buffer, buffer + sizeof(double));
  }
  writeBytes(buffer, sizeof(double));
}

// Write a string into this stream
void OutputStream::write(const std::string &value) {
  write(int(value.length()));
  writeBytes(value.c_str(), value.length());
}
