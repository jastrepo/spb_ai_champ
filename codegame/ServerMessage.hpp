#ifndef __MODEL_SERVER_MESSAGE_HPP__
#define __MODEL_SERVER_MESSAGE_HPP__

#include <memory>
#include <model/Game.hpp>

namespace codegame {
// Message sent from server
  class ServerMessage {
  public:
    virtual ~ServerMessage() = default;

    // Get action for next tick
    class GetAction;

    // Signifies end of the game
    class Finish;

    // Debug update
    class DebugUpdate;

    // Read ServerMessage from input stream
    static std::shared_ptr<ServerMessage> readFrom(InputStream &stream);

    // Write ServerMessage to output stream
    virtual void writeTo(OutputStream &stream) const = 0;

    // Get string representation of ServerMessage
    [[nodiscard]] virtual std::string toString() const = 0;
  };

// Get action for next tick
  class ServerMessage::GetAction : public ServerMessage {
  public:
    static const int TAG = 0;
    // Player's view
    model::Game playerView;
    // Whether app is running with debug interface available
    bool debugAvailable;

    GetAction(model::Game playerView, bool debugAvailable);

    // Read GetAction from input stream
    static GetAction readFrom(InputStream &stream);

    // Write GetAction to output stream
    void writeTo(OutputStream &stream) const override;

    // Get string representation of GetAction
    std::string toString() const override;
  };

// Signifies end of the game
  class ServerMessage::Finish : public ServerMessage {
  public:
    static const int TAG = 1;

    Finish() = default;

    // Read Finish from input stream
    static Finish readFrom(InputStream &stream);

    // Write Finish to output stream
    void writeTo(OutputStream &stream) const override;

    // Get string representation of Finish
    [[nodiscard]] std::string toString() const override;

    bool operator==(const Finish &other) const;
  };

// Debug update
  class ServerMessage::DebugUpdate : public ServerMessage {
  public:
    static const int TAG = 2;
    // Player's view
    model::Game playerView;

    explicit DebugUpdate(model::Game playerView);

    // Read DebugUpdate from input stream
    static DebugUpdate readFrom(InputStream &stream);

    // Write DebugUpdate to output stream
    void writeTo(OutputStream &stream) const override;

    // Get string representation of DebugUpdate
    std::string toString() const override;
  };
}
#endif