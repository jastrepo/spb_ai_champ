//
// Created by nikita on 11/3/21.
//
#pragma once

#include "PlanObjects.h"
#include "strategyData/strategy/BuildingPlan.h"

struct StrategyData;

struct LoadGraph {
  struct Building {
    uint32_t planetId;
    model::BuildingType buildingType;
    const model::BuildingProperties &properties;
    double load = 0;

    Building(uint32_t planetId, model::BuildingType buildingType, const model::BuildingProperties &properties);
  };

  struct Route {
    uint32_t startPlanetId, endPlanetId;
    double robots;
    double resources;

    Route(uint32_t startPlanetId, uint32_t endPlanetId, double robots, double resources);
  };

  std::unordered_map<uint32_t, Building> buildings;
  std::unordered_map<PlanetPair, Route> routes;
  double robotsOnRoutes = 0;

  StrategyData &strategyData;

  explicit LoadGraph(StrategyData &strategyData);

  LoadGraph(const LoadGraph &second) = default;

  LoadGraph &operator=(const LoadGraph &second);

  static LoadGraph copyActiveBuildings(const LoadGraph &second);

  static LoadGraph copyBuildings(const BuildingPlan &plan);

  void add(const LoadGraph &second, double multiplier = 1);

  bool connect(uint32_t startPlanetId, uint32_t endPlanetId, double resources);
};