//
// Created by nikita on 11/3/21.
//
#include "maxLoad.h"

std::pair<LoadGraph, model::Resource> findNearestProducer(const LoadGraph &currentGraph, uint32_t planetId,
                                                          model::Resource resource, double amount) {
  bool foundGraph = false;
  LoadGraph bestGraph = LoadGraph::copyActiveBuildings(currentGraph);
  model::Resource resourceNotFound = resource;
  for (const auto &[_, building]: bestGraph.buildings) {
    if (building.properties.produceResource.has_value() && building.properties.produceResource.value() == resource) {
      double rounds = amount / building.properties.produceAmount;
      LoadGraph graph = LoadGraph::copyActiveBuildings(currentGraph);

      bool foundAllProducers = true;
      for (const auto &[secondResource, secondResourceAmount]: building.properties.workResources) {
        const auto &[secondGraph, secondResourceNotFound] = findNearestProducer(currentGraph, building.planetId,
                                                                                secondResource,
                                                                                secondResourceAmount * rounds);
        if (secondGraph.robotsOnRoutes < 1e-5) {
          resourceNotFound = secondResourceNotFound;
          foundAllProducers = false;
          break;
        } else {
          graph.add(secondGraph);
        }
      }
      if (foundAllProducers) {
        if (graph.connect(building.planetId, planetId, amount)) {
          auto &graphBuilding = graph.buildings.at(building.planetId);
          graphBuilding.load += building.properties.workAmount * rounds / building.properties.maxWorkers;

          if (!foundGraph || graph.robotsOnRoutes < bestGraph.robotsOnRoutes) {
            bestGraph = graph;
            foundGraph = true;
          }
        }
      }
    }
  }

  return {bestGraph, resourceNotFound};
}

std::pair<LoadGraph, model::Resource> findMaxLoad(uint32_t planetId, const BuildingPlan &buildingPlan) {
  LoadGraph maxGraph = LoadGraph::copyBuildings(buildingPlan);
  auto &building = maxGraph.buildings.at(planetId);
  double maxMultiplier = double(building.properties.maxWorkers) / building.properties.workAmount;

  while (building.load + 1e-5 < 1) {
    LoadGraph currentGraph = LoadGraph::copyActiveBuildings(maxGraph);

    for (const auto &[resource, resourceAmount]: building.properties.workResources) {
      const auto &[graph, resourceNotFound] = findNearestProducer(maxGraph, planetId, resource, resourceAmount);
      if (graph.robotsOnRoutes < 1e-5) {
        return {maxGraph, resourceNotFound};
      } else {
        currentGraph.add(graph);
      }
    }

    double minMultiplier = (1 - building.load) * maxMultiplier;
    for (const auto &[_, currentBuilding]: currentGraph.buildings) {
      double additionalLoad = 1 - maxGraph.buildings.at(currentBuilding.planetId).load;
      double multiplier = additionalLoad / currentBuilding.load;
      minMultiplier = std::min(minMultiplier, multiplier);
    }

    building.load += minMultiplier / maxMultiplier;
    maxGraph.add(currentGraph, minMultiplier);
  }
  return {maxGraph, model::Resource::STONE};
}
