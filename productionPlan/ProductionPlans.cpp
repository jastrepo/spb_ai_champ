//
// Created by nikita on 11/1/21.
//

#include "ProductionPlans.h"
#include <strategyData/StrategyData.h>
#include <tasks/Tasks.h>

ProductionPlans::ProductionPlans(Tasks &newTasks) : strategyData(newTasks.strategyData), tasks(newTasks) {}

void ProductionPlans::newTick() {
  completed = true;
  balancePackages.erase(std::remove_if(balancePackages.begin(), balancePackages.end(), [&](size_t packageId) {
    return tasks.finished(packageId);
  }), balancePackages.end());

  for (const auto &[_, planBuilding]: planBuildings) {
    strategyData.resourcePlanner.fireRobotsOnPlanet(planBuilding.planetId, planBuilding.number);

    if (!isBuilt(planBuilding.planetId)) {
      tasks.builds.add(planBuilding.planetId, planBuilding.type);
      completed = false;
    }
  }

  totalRobots = 0;
  for (const auto &planet: strategyData.planets) {
    totalRobots += strategyData.resourcePlanner.getTotalRobotsOnPlanet(planet.id);
  }
  for (const auto &[_, planRoute]: planRoutes) {
    const auto &routeIt = tasks.routes.withId(planRoute.routeId);
    if (!routeIt.finished) {
      totalRobots += routeIt.robotsInFly;
    }
  }
  for (auto packageId: balancePackages) {
    totalRobots += tasks.logistics.withId(packageId).number;
  }

  double k = double(totalRobots) / double(planRobots);
  if (k + 1e-5 > 1) {
    k = 1;
  } else {
    completed = false;
  }

  for (auto &[_, planBuilding]: planBuildings) {
    if (isBuilt(planBuilding.planetId)) {
      planBuilding.number = uint32_t(planBuilding.planNumber * k);
      auto robotsOnPlanet = strategyData.resourcePlanner.getTotalRobotsOnPlanet(planBuilding.planetId);
      for (const auto &packageId: balancePackages) {
        const auto &package = tasks.logistics.withId(packageId);
        if (package.endPlanet == planBuilding.planetId) {
          robotsOnPlanet += package.number;
        }
      }
      if (robotsOnPlanet < planBuilding.number) {
        planBuilding.deficiency = planBuilding.number - robotsOnPlanet;
      } else {
        planBuilding.deficiency = 0;
      }
      strategyData.resourcePlanner.hireRobotsOnPlanet(planBuilding.planetId, planBuilding.number);
    }
  }
  for (auto &[_, planRoute]: planRoutes) {
    if (isBuilt(planRoute.startPlanetId) && isBuilt(planRoute.endPlanetId)) {
      planRoute.number = uint32_t(planRoute.planNumber * k);
      uint32_t robotsInFly = 0;

      if (tasks.routes.withId(planRoute.routeId).finished) {
        const auto &route = tasks.routes.add(planRoute.startPlanetId, planRoute.endPlanetId,
                                                        planRoute.number, planRoute.resource);
        if (route != nullptr) {
          planRoute.routeId = route->id;
          robotsInFly = route->robotsInFly;
        }
      } else {
        auto routePtr = tasks.routes.setNumber(planRoute.routeId, planRoute.number);
        robotsInFly = routePtr->robotsInFly;
      }

      robotsInFly += strategyData.resourcePlanner.getTotalRobotsOnPlanet(planRoute.startPlanetId);
      if (robotsInFly < planRoute.number) {
        planRoute.deficiency = planRoute.number - robotsInFly;
      } else {
        planRoute.deficiency = 0;
      }
    }
  }

  for (auto &[_, planBuilding]: planBuildings) {
    auto deficiency = planBuilding.deficiency;
    if (deficiency > 0) {
      for (const auto &planetId: strategyData.planetGraph.rangedPlanets[planBuilding.planetId]) {
        if (deficiency == 0) {
          break;
        }
        for (auto specialty: {model::Specialty::PRODUCTION, model::Specialty::LOGISTICS,
                              model::Specialty::COMBAT}) {
          if (deficiency == 0) {
            break;
          }
          const auto &package = tasks.logistics.add(planetId, planBuilding.planetId, deficiency,
                                                               std::nullopt,
                                                               strategyData.team.teamPlayerBySpecialty[specialty]);
          if (package != nullptr) {
            balancePackages.emplace_back(package->id);
            if (package->number < deficiency) {
              deficiency -= package->number;
            } else {
              deficiency = 0;
            }
          }
        }
      }
    }
  }
  for (auto &[_, planRoute]: planRoutes) {
    if (!tasks.routes.withId(planRoute.routeId).finished) {
      auto deficiency = planRoute.deficiency;
      if (deficiency > 0) {
        for (const auto &planetId: strategyData.planetGraph.rangedPlanets[planRoute.startPlanetId]) {
          if (deficiency == 0) {
            break;
          }
          for (auto specialty: {model::Specialty::LOGISTICS, model::Specialty::COMBAT,
                                model::Specialty::PRODUCTION}) {
            if (deficiency == 0) {
              break;
            }
            const auto &package = tasks.logistics.add(planetId, planRoute.startPlanetId, deficiency,
                                                                 std::nullopt,
                                                                 strategyData.team.teamPlayerBySpecialty[specialty]);
            auto routePtr = tasks.routes.addLogisticsTask(planRoute.routeId, package);
            if (routePtr->robotsInFly < planRoute.number) {
              deficiency = planRoute.number - routePtr->robotsInFly;
            } else {
              deficiency = 0;
            }
          }
        }
      }
    }
  }
}

#include <iostream>

using std::cout;
using std::endl;

void ProductionPlans::addBuilding(uint32_t planetId, model::BuildingType buildingType, uint32_t planNumber) {
  if (planBuildings.count(planetId) == 0) {
    PlanBuilding planBuilding(planetId, 0, planNumber, buildingType,
                              strategyData.constants.buildingProperties.at(buildingType), 0);
    planBuildings.emplace(planetId, planBuilding);
    planRobots += planNumber;
    //cout << "place " << planNumber << " robots in " << model::buildingTypeToString(buildingType) << endl;
  }
}

void ProductionPlans::connectBuildings(uint32_t startPlanetId, uint32_t endPlanetId, model::Resource resource,
                                       double planNumber) {
  /*cout << "send " << planNumber << " " << model::resourceToString(resource) << " from "
       << model::buildingTypeToString(planBuildings.at(startPlanetId).type) << " to "
       << model::buildingTypeToString(planBuildings.at(endPlanetId).type) << endl;*/
  auto realNumber = uint32_t(
      2 * planNumber * double(strategyData.planetGraph.getRealDistance(startPlanetId, endPlanetId)));
  PlanRoute planRoute(startPlanetId, endPlanetId, 0, realNumber, resource, std::numeric_limits<uint32_t>::max(), 0);
  planRoutes.emplace(PlanetPair(startPlanetId, endPlanetId), planRoute);
  planRobots += realNumber;
}

bool ProductionPlans::isBuilt(uint32_t planetId) {
  const auto &planBuildingIt = planBuildings.find(planetId);
  if (planBuildingIt != planBuildings.end()) {
    const auto &planBuilding = planBuildingIt->second;
    const auto &planet = strategyData.planets[planetId];
    if (planet.building.has_value()) {
      const auto &planetBuilding = planet.building.value();
      if (planetBuilding.buildingType == planBuilding.type) {
        if (planetBuilding.health == planBuilding.properties.maxHealth) {
          return true;
        }
      }
    }
  }
  return false;
}

void ProductionPlans::changeBuildingPlanNumber(uint32_t planetId, uint32_t newPlanNumber) {
  const auto &oldPlanBuildingIt = planBuildings.find(planetId);
  if (oldPlanBuildingIt != planBuildings.end()) {
    auto &oldPlanBuilding = oldPlanBuildingIt->second;
    planRobots -= oldPlanBuilding.planNumber;
    planRobots += newPlanNumber;
    oldPlanBuilding.planNumber = newPlanNumber;
  }
}

void ProductionPlans::changeConnectionPlanNumber(uint32_t startPlanetId, uint32_t endPlanetId, double newPlanNumber) {
  const auto &oldPlanRouteIt = planRoutes.find({startPlanetId, endPlanetId});
  if (oldPlanRouteIt != planRoutes.end()) {
    auto newRealNumber = uint32_t(
        2 * newPlanNumber * double(strategyData.planetGraph.getRealDistance(startPlanetId, endPlanetId)));
    auto &oldPlanRoute = oldPlanRouteIt->second;
    planRobots -= oldPlanRoute.planNumber;
    planRobots += newRealNumber;
    oldPlanRoute.planNumber = newRealNumber;
  }
}

void ProductionPlans::removeBuilding(uint32_t planetId) {
  auto planBuildingIt = planBuildings.find(planetId);
  if (planBuildingIt != planBuildings.end()) {
    planRobots -= planBuildingIt->second.planNumber;
    strategyData.resourcePlanner.fireRobotsOnPlanet(planetId, planBuildingIt->second.number);
    planBuildings.erase(planBuildingIt);
  }
}
