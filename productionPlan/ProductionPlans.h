//
// Created by nikita on 11/1/21.
//
#pragma once

#include <tasks/Tasks.h>
#include <vector>
#include "PlanObjects.h"

struct ProductionPlans {
  std::vector<uint32_t> balancePackages;

  uint32_t planRobots = 0, totalRobots = 0;

  std::unordered_map<uint32_t, PlanBuilding> planBuildings;
  std::unordered_map<PlanetPair, PlanRoute> planRoutes;

  bool completed = false;

  void addBuilding(uint32_t planetId, model::BuildingType buildingType, uint32_t planNumber);

  void changeBuildingPlanNumber(uint32_t planetId, uint32_t newPlanNumber);

  void removeBuilding(uint32_t planetId);

  void connectBuildings(uint32_t startPlanetId, uint32_t endPlanetId, model::Resource resource, double planNumber);

  void changeConnectionPlanNumber(uint32_t startPlanetId, uint32_t endPlanetId, double newPlanNumber);

  StrategyData &strategyData;
  Tasks &tasks;

  explicit ProductionPlans(Tasks &newTasks);

  void newTick();

  bool isBuilt(uint32_t planetId);
};