//
// Created by nikita on 11/1/21.
//
#include "PlanObjects.h"

PlanBuilding::PlanBuilding(uint32_t planetId, uint32_t number, uint32_t planNumber, model::BuildingType type,
                           model::BuildingProperties properties, uint32_t deficiency)
    : planetId(planetId), number(number), planNumber(planNumber), type(type), properties(std::move(properties)),
      deficiency(deficiency) {}

PlanRoute::PlanRoute(uint32_t startPlanetId, uint32_t endPlanetId, uint32_t number, uint32_t planNumber,
                     model::Resource resource, size_t routeId, uint32_t deficiency)
    : startPlanetId(startPlanetId), endPlanetId(endPlanetId), number(number), planNumber(planNumber),
      resource(resource), routeId(routeId), deficiency(deficiency) {}

PlanetPair::PlanetPair(uint32_t startPlanetId, uint32_t endPlanetId)
    : startPlanetId(startPlanetId), endPlanetId(endPlanetId) {}


std::size_t std::hash<PlanetPair>::operator()(const PlanetPair &planetPair) const noexcept {
  size_t result = 0;
  result = (result << 8) + size_t(planetPair.startPlanetId);
  result = (result << 8) + size_t(planetPair.endPlanetId);
  return result;
}

std::size_t std::equal_to<PlanetPair>::operator()(const PlanetPair &first, const PlanetPair &second) const noexcept {
  return std::hash<PlanetPair>()(first) == std::hash<PlanetPair>()(second);
}