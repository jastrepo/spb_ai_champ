//
// Created by nikita on 11/3/21.
//

#include "LoadGraph.h"
#include <strategyData/StrategyData.h>

LoadGraph::LoadGraph(StrategyData &strategyData) : strategyData(strategyData) {}

bool LoadGraph::connect(uint32_t startPlanetId, uint32_t endPlanetId, double resources) {
  if (strategyData.planetGraph.getRealDistance(startPlanetId, endPlanetId) < PlanetGraph::infinity) {
    Route route(startPlanetId, endPlanetId,
                2 * strategyData.planetGraph.getRealDistance(startPlanetId, endPlanetId) * resources, resources);
    const auto &routeIt = routes.emplace(PlanetPair(startPlanetId, endPlanetId), route);
    if (!routeIt.second) {
      routeIt.first->second.robots += route.robots;
    }
    robotsOnRoutes += route.robots;
    return true;
  } else {
    return false;
  }
}

LoadGraph LoadGraph::copyActiveBuildings(const LoadGraph &second) {
  LoadGraph loadGraph(second.strategyData);
  for (const auto &[planetId, building]: second.buildings) {
    if (building.load + 1e-5 < 1) {
      Building newBuilding(building.planetId, building.buildingType, building.properties);
      loadGraph.buildings.emplace(planetId, newBuilding);
    }
  }
  return loadGraph;
}

LoadGraph LoadGraph::copyBuildings(const BuildingPlan &plan) {
  LoadGraph loadGraph(plan.strategyData);
  for (const auto &[planetId, building]: plan.buildings) {
    Building newBuilding(planetId, building.first, building.second);
    loadGraph.buildings.emplace(planetId, newBuilding);
  }
  return loadGraph;
}

LoadGraph &LoadGraph::operator=(const LoadGraph &second) {
  for (const auto &[planetId, building]: second.buildings) {
    buildings.at(planetId).load = building.load;
  }
  routes = second.routes;
  robotsOnRoutes = second.robotsOnRoutes;
  return *this;
}

void LoadGraph::add(const LoadGraph &second, double multiplier) {
  for (const auto &[planetId, building]: second.buildings) {
    buildings.at(planetId).load += building.load * multiplier;
  }
  for (const auto &[planetPair, route]: second.routes) {
    const auto &routeIt = routes.emplace(planetPair, route);
    if (routeIt.second) {
      routeIt.first->second.robots *= multiplier;
      routeIt.first->second.resources *= multiplier;
    } else {
      routeIt.first->second.robots += route.robots * multiplier;
      routeIt.first->second.resources += route.resources * multiplier;
    }
  }
  robotsOnRoutes += second.robotsOnRoutes * multiplier;
}

LoadGraph::Route::Route(uint32_t startPlanetId, uint32_t endPlanetId, double robots, double resources)
    : startPlanetId(startPlanetId), endPlanetId(endPlanetId), robots(robots), resources(resources) {}

LoadGraph::Building::Building(uint32_t planetId, model::BuildingType buildingType,
                              const model::BuildingProperties &properties)
    : planetId(planetId), buildingType(buildingType), properties(properties) {}