//
// Created by nikita on 11/1/21.
//
#pragma once

#include <model/BuildingProperties.hpp>

struct PlanBuilding {
  uint32_t planetId;
  uint32_t number, planNumber;
  model::BuildingType type;
  model::BuildingProperties properties;
  uint32_t deficiency;

  PlanBuilding(uint32_t planetId, uint32_t number, uint32_t planNumber, model::BuildingType type,
               model::BuildingProperties properties, uint32_t deficiency);
};

struct PlanRoute {
  uint32_t startPlanetId, endPlanetId;
  uint32_t number, planNumber;
  model::Resource resource;
  size_t routeId;
  uint32_t deficiency;

  PlanRoute(uint32_t startPlanetId, uint32_t endPlanetId, uint32_t number, uint32_t planNumber,
            model::Resource resource, size_t routeId, uint32_t deficiency);
};

struct PlanetPair {
  uint32_t startPlanetId, endPlanetId;

  PlanetPair(uint32_t startPlanetId, uint32_t endPlanetId);
};

namespace std {
  template<>
  struct hash<PlanetPair> {
    std::size_t operator()(const PlanetPair &planetPair) const noexcept;
  };

  template<>
  struct equal_to<PlanetPair> {
    std::size_t operator()(const PlanetPair &first, const PlanetPair &second) const noexcept;
  };
}