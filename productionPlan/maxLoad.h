//
// Created by nikita on 11/3/21.
//
#pragma once

#include "LoadGraph.h"

std::pair<LoadGraph, model::Resource> findMaxLoad(uint32_t planetId, const BuildingPlan &buildingPlan);