//
// Created by nikita on 11/3/21.
//
#pragma once

#include "BaseStrategy.h"
#include <productionPlan/ProductionPlans.h>
#include <positionFinder/ClosePlanets.h>

struct BalancerTestStrategy : public BaseStrategy {
  int enhance = 1;
  model::Resource resourceNotFound = model::Resource::STONE;
  ProductionPlans productionPlans;

  explicit BalancerTestStrategy(Tasks &tasks);

  void firstTick() override;

  void newTick(uint32_t tickNumber) override;

  ClosePlanets closePlanets;
  StartPosition startPosition;
  uint32_t replicatorPlanet = 0;
  bool simplePlan = true;
};