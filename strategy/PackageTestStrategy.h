//
// Created by nikita on 10/29/21.
//
#pragma once

#include "BaseStrategy.h"

struct PackageTestStrategy : public BaseStrategy {
  explicit PackageTestStrategy(Tasks &tasks);

  void firstTick() override;

  void newTick(uint32_t tickNumber) override;
};