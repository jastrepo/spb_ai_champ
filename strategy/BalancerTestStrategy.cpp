//
// Created by nikita on 11/3/21.
//

#include <strategyData/strategy/BuildingPlan.h>
#include <productionPlan/maxLoad.h>
#include <queue>
#include <iostream>
#include <chrono>
#include <cmath>
#include "BalancerTestStrategy.h"

#include <iostream>

using std::cout;
using std::endl;

BalancerTestStrategy::BalancerTestStrategy(Tasks &tasks)
    : BaseStrategy(tasks), productionPlans(tasks), closePlanets(strategyData) {}

void BalancerTestStrategy::firstTick() {
  specialty = model::Specialty::PRODUCTION;

  closePlanets.findTeamPlanets();
  for (auto teamPlanet: closePlanets.teamPlanets) {
    strategyData.buildingPlan.addBuilding(teamPlanet, model::BuildingType::QUARRY);
  }
  /*for (auto teamPlanet: closePlanets.teamPlanets) {
    for (const auto &teamPlayer: strategyData.team.teamPlayer) {
      if (strategyData.planets[teamPlanet].workerGroups.front().playerIndex == teamPlayer) {
        tasks.attack.add(teamPlanet, 100, teamPlayer);
        break;
      }
    }
  }*/
  if (closePlanets.findClosePlanetsForTeam()) {
    auto startTime = std::chrono::high_resolution_clock::now();
    startPosition = closePlanets.findBestPosition();
    auto endTime = std::chrono::high_resolution_clock::now();
    std::cout << double((endTime - startTime).count()) * 1e-9 << std::endl;

    replicatorPlanet = startPosition.replicatorPlanet;

    if (replicatorPlanet < strategyData.planets.maxId) {
      /*strategyData.buildingPlan.addBuilding(startPosition.orePlanet, model::BuildingType::MINES);
      strategyData.buildingPlan.addBuilding(startPosition.sandPlanet, model::BuildingType::CAREER);*/
      strategyData.buildingPlan.addBuilding(startPosition.organicsPlanet, model::BuildingType::FARM);
      /*strategyData.buildingPlan.addBuilding(startPosition.metalPlanet, model::BuildingType::FOUNDRY);
      strategyData.buildingPlan.addBuilding(startPosition.siliconPlanet, model::BuildingType::FURNACE);*/
      strategyData.buildingPlan.addBuilding(startPosition.plasticPlanet, model::BuildingType::BIOREACTOR);
      /*strategyData.buildingPlan.addBuilding(startPosition.chipPlanet, model::BuildingType::CHIP_FACTORY);
      strategyData.buildingPlan.addBuilding(startPosition.accumulatorPlanet, model::BuildingType::ACCUMULATOR_FACTORY);
      strategyData.buildingPlan.addBuilding(replicatorPlanet, model::BuildingType::REPLICATOR);*/

      const auto &result = findMaxLoad(startPosition.plasticPlanet, strategyData.buildingPlan);

      resourceNotFound = result.second;
      for (const auto[_, building]: result.first.buildings) {
        productionPlans.addBuilding(building.planetId, building.buildingType,
                                    uint32_t(ceil(building.properties.maxWorkers * building.load)));
      }
      for (const auto[_, route]: result.first.routes) {
        productionPlans.connectBuildings(route.startPlanetId, route.endPlanetId,
                                         strategyData.buildingPlan.buildings.at(
                                             route.startPlanetId).second.produceResource.value(),
                                         route.resources);
      }
    }
  }
  for (auto teamPlanet: closePlanets.teamPlanets) {
    productionPlans.changeBuildingPlanNumber(teamPlanet, strategyData.weights.stayOnStonePlanet);
  }
  productionPlans.newTick();
}

void BalancerTestStrategy::newTick(uint32_t tickNumber) {
  productionPlans.newTick();
  if (simplePlan) {
    if (tickNumber > 4 && productionPlans.totalRobots - productionPlans.planRobots > 50) {
      simplePlan = false;

      strategyData.buildingPlan.addBuilding(startPosition.orePlanet, model::BuildingType::MINES);
      strategyData.buildingPlan.addBuilding(startPosition.sandPlanet, model::BuildingType::CAREER);
      strategyData.buildingPlan.addBuilding(startPosition.metalPlanet, model::BuildingType::FOUNDRY);
      strategyData.buildingPlan.addBuilding(startPosition.siliconPlanet, model::BuildingType::FURNACE);
      strategyData.buildingPlan.addBuilding(startPosition.chipPlanet, model::BuildingType::CHIP_FACTORY);
      strategyData.buildingPlan.addBuilding(startPosition.accumulatorPlanet, model::BuildingType::ACCUMULATOR_FACTORY);
      strategyData.buildingPlan.addBuilding(replicatorPlanet, model::BuildingType::REPLICATOR);

      const auto &result = findMaxLoad(replicatorPlanet, strategyData.buildingPlan);
      resourceNotFound = result.second;
      cout << "replicator used at " << double(int(result.first.buildings.at(replicatorPlanet).load * 1e+3)) / 10
           << "%" << endl;

      for (const auto[_, building]: result.first.buildings) {
        if (productionPlans.planBuildings.count(building.planetId) == 0) {
          productionPlans.addBuilding(building.planetId, building.buildingType,
                                      uint32_t(ceil(building.properties.maxWorkers * building.load)));
        } else {
          productionPlans.changeBuildingPlanNumber(building.planetId, uint32_t(
              ceil(building.properties.maxWorkers * building.load)));
        }
      }
      for (const auto[_, route]: result.first.routes) {
        if (productionPlans.planRoutes.count(PlanetPair(route.startPlanetId, route.endPlanetId)) == 0) {
          productionPlans.connectBuildings(route.startPlanetId, route.endPlanetId,
                                           strategyData.buildingPlan.buildings.at(
                                               route.startPlanetId).second.produceResource.value(),
                                           route.resources);
        } else {
          productionPlans.changeConnectionPlanNumber(route.startPlanetId, route.endPlanetId,
                                                     route.resources);
        }
      }
    }
  } else {
    if (productionPlans.planRobots > 2 * productionPlans.totalRobots) {
      simplePlan = true;
      strategyData.buildingPlan.buildings.clear();
      strategyData.buildingPlan.addBuilding(startPosition.organicsPlanet, model::BuildingType::FARM);
      strategyData.buildingPlan.addBuilding(startPosition.plasticPlanet, model::BuildingType::BIOREACTOR);

      const auto &result = findMaxLoad(startPosition.plasticPlanet, strategyData.buildingPlan);

      for (const auto[_, building]: result.first.buildings) {
        if (productionPlans.planBuildings.count(building.planetId) == 0) {
          productionPlans.addBuilding(building.planetId, building.buildingType,
                                      uint32_t(ceil(building.properties.maxWorkers * building.load)));
        } else {
          productionPlans.changeBuildingPlanNumber(building.planetId, uint32_t(
              ceil(building.properties.maxWorkers * building.load)));
        }
      }
      for (const auto[_, route]: result.first.routes) {
        if (productionPlans.planRoutes.count(PlanetPair(route.startPlanetId, route.endPlanetId)) == 0) {
          productionPlans.connectBuildings(route.startPlanetId, route.endPlanetId,
                                           strategyData.buildingPlan.buildings.at(
                                               route.startPlanetId).second.produceResource.value(),
                                           route.resources);
        } else {
          productionPlans.changeConnectionPlanNumber(route.startPlanetId, route.endPlanetId,
                                                     route.resources);
        }
      }
    } else {
      if (productionPlans.completed) {
        if (enhance == 0 || resourceNotFound == model::Resource::STONE) {
          for (const auto &planet: strategyData.planets) {
            if (strategyData.resourcePlanner.getPlayerRobotsOnPlanet(planet.id, strategyData.constants.myIndex) >
                strategyData.weights.attackGroupMinSize) {
              tasks.attack.add(planet.id, strategyData.resourcePlanner.getPlayerRobotsOnPlanet(planet.id,
                                                                                               strategyData.constants.myIndex),
                               strategyData.constants.myIndex);
            }
          }
        } else {
          --enhance;
          model::BuildingType necessaryBuilding = model::BuildingType::QUARRY;
          std::optional<model::Resource> necessaryResource;
          for (const auto &[buildingType, buildingProperties]: strategyData.constants.buildingProperties) {
            if (buildingProperties.produceResource.has_value() &&
                buildingProperties.produceResource.value() == resourceNotFound) {
              if (buildingProperties.harvest) {
                necessaryResource = resourceNotFound;
              }
              necessaryBuilding = buildingType;
              break;
            }
          }

          if (necessaryBuilding != model::BuildingType::QUARRY) {
            double minScore = Constants::inf;
            uint32_t bestPlanet = Constants::inf;
            for (const auto &planet: strategyData.planets.planets) {
              if (planet.id < strategyData.planets.maxId && planet.harvestableResource == necessaryResource) {
                if (strategyData.buildingPlan.buildings.count(planet.id) == 0) {
                  double currentScore = 0;
                  for (const auto &[_, building]: productionPlans.planBuildings) {
                    double rounds = double(building.planNumber) / building.properties.workAmount;
                    if (building.properties.workResources.count(resourceNotFound) != 0) {
                      currentScore += strategyData.planetGraph.getRealDistance(building.planetId, planet.id) *
                                      building.properties.workResources.at(resourceNotFound) * rounds;
                    } else if (building.properties.produceResource.has_value()) {
                      if (strategyData.constants.buildingProperties.at(necessaryBuilding).workResources.count(
                          building.properties.produceResource.value()) != 0) {
                        currentScore += strategyData.planetGraph.getRealDistance(building.planetId, planet.id) *
                                        building.properties.produceAmount * rounds;
                      }
                      if (building.properties.produceResource.value() == resourceNotFound) {
                        currentScore -= strategyData.planetGraph.getRealDistance(building.planetId, planet.id) *
                                        building.properties.produceAmount * rounds;
                      }
                    }
                  }
                  if (currentScore < minScore) {
                    minScore = currentScore;
                    bestPlanet = planet.id;
                  }
                }
              }
            }

            if (bestPlanet < Constants::inf) {
              strategyData.buildingPlan.addBuilding(bestPlanet, necessaryBuilding);
              const auto &result = findMaxLoad(replicatorPlanet, strategyData.buildingPlan);
              resourceNotFound = result.second;
              cout << "replicator used at " << double(int(result.first.buildings.at(replicatorPlanet).load * 1e+3)) / 10
                   << "%" << endl;

              for (const auto[_, building]: result.first.buildings) {
                if (productionPlans.planBuildings.count(building.planetId) == 0) {
                  productionPlans.addBuilding(building.planetId, building.buildingType,
                                              uint32_t(ceil(building.properties.maxWorkers * building.load)));
                } else {
                  productionPlans.changeBuildingPlanNumber(building.planetId, uint32_t(
                      ceil(building.properties.maxWorkers * building.load)));
                }
              }
              for (const auto[_, route]: result.first.routes) {
                if (productionPlans.planRoutes.count(PlanetPair(route.startPlanetId, route.endPlanetId)) == 0) {
                  productionPlans.connectBuildings(route.startPlanetId, route.endPlanetId,
                                                   strategyData.buildingPlan.buildings.at(
                                                       route.startPlanetId).second.produceResource.value(),
                                                   route.resources);
                } else {
                  productionPlans.changeConnectionPlanNumber(route.startPlanetId, route.endPlanetId,
                                                             route.resources);
                }
              }
            }
          }
        }
      }
    }
  }
  for (auto teamPlanet: closePlanets.teamPlanets) {
    productionPlans.changeBuildingPlanNumber(teamPlanet, strategyData.weights.stayOnStonePlanet);
  }
}
