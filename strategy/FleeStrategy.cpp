//
// Created by nikita on 11/8/21.
//
#include "FleeStrategy.h"

FleeStrategy::FleeStrategy(Tasks &tasks) : BaseStrategy(tasks), productionPlans(tasks) {}

void FleeStrategy::firstTick() {
  for (const auto &planet: strategyData.planets) {
    if (strategyData.planetDanger.relative(planet.id) < .8) {
      if (planet.harvestableResource.has_value()) {
        auto resource = planet.harvestableResource.value();
        if (resource == model::Resource::STONE) {
          productionPlans.addBuilding(planet.id, model::BuildingType::QUARRY, 10);
        } else if (resource == model::Resource::ORE) {
          productionPlans.addBuilding(planet.id, model::BuildingType::MINES, 200);
        } else if (resource == model::Resource::SAND) {
          productionPlans.addBuilding(planet.id, model::BuildingType::CAREER, 400);
        } else if (resource == model::Resource::ORGANICS) {
          productionPlans.addBuilding(planet.id, model::BuildingType::FARM, 800);
        }
      }
    }
  }
  productionPlans.newTick();
}

void FleeStrategy::newTick(uint32_t) {
  /*for (const auto &[_, package]: strategyData.packages.packages) {
    if (!package.finished) {
      if (strategyData.planetDanger.relative(package.endPlanet) > .8) {
        strategyData.packages.cancelWithId(package.id);
      }
    }
  }
  for (const auto &planet: strategyData.planets) {
    uint32_t opponentToPlanet = 0;
    for (const auto &group: strategyData.flyingGroups.logicGroups) {
      if (group.targetPlanet == planet.id && strategyData.team.teamPlayer.count(group.playerIndex) == 0) {
        opponentToPlanet += group.number;
      }
    }
    if (opponentToPlanet > 8) {
      productionPlans.removeBuilding(planet.id);
      bool timeToFlee = false;
      if (planet.building.has_value()) {
        for (const auto &group: strategyData.flyingGroups.logicGroups) {
          if (group.targetPlanet == planet.id && strategyData.team.teamPlayer.count(group.playerIndex) == 0) {
            if (strategyData.planetGraph.getKnownDistance(planet.id, group.nextPlanet) + group.nextPlanetArrivalTick ==
                strategyData.currentTick + 1) {
              timeToFlee = true;
            }
          }
        }
      } else {
        timeToFlee = true;
      }
      if (timeToFlee) {
        for (const auto &planetToFlee: strategyData.planets) {
          if (strategyData.planetDanger.relative(planetToFlee.id) < .8) {
            for (auto playerIndex: strategyData.team.teamPlayer) {
              for (uint32_t resourceIndex = 0; resourceIndex < ResourcePlanner::resourceTypesNumber; ++resourceIndex) {
                strategyData.packages.add(planet.id, planetToFlee.id,
                                          strategyData.resourcePlanner.getPlayerRobotsWithResourceOnPlanet(planet.id,
                                                                                                           playerIndex,
                                                                                                           resourceIndex),
                                          model::Resource(resourceIndex), playerIndex);
              }
              strategyData.packages.add(planet.id, planetToFlee.id,
                                        strategyData.resourcePlanner.getPlayerRobotsOnPlanet(planet.id, playerIndex),
                                        std::nullopt, playerIndex);
            }
          }
        }
      } else {
        strategyData.buildActions.add({planet.id, std::nullopt});
      }
    }

    if (strategyData.planetDanger.relative(planet.id) < .8) {
      if (planet.harvestableResource.has_value()) {
        auto resource = planet.harvestableResource.value();
        if (resource == model::Resource::STONE) {
          productionPlans.addBuilding(planet.id, model::BuildingType::QUARRY, 10);
        } else if (resource == model::Resource::ORE) {
          productionPlans.addBuilding(planet.id, model::BuildingType::MINES, 200);
        } else if (resource == model::Resource::SAND) {
          productionPlans.addBuilding(planet.id, model::BuildingType::CAREER, 400);
        } else if (resource == model::Resource::ORGANICS) {
          productionPlans.addBuilding(planet.id, model::BuildingType::FARM, 800);
        }
      }
    }
  }*/
  productionPlans.newTick();
}
