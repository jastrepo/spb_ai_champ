//
// Created by nikita on 11/3/21.
//

#include "BaseStrategy.h"

BaseStrategy::BaseStrategy(Tasks &tasks) : tasks(tasks),strategyData(tasks.strategyData) {}

uint32_t BaseStrategy::findNearest(uint32_t planetId, bool occupy) {
  auto rangedPlanets = strategyData.planetGraph.rangedPlanets[planetId];

  auto newPlanetIt = std::find_if(rangedPlanets.begin(), rangedPlanets.end(), [&](uint32_t planetId) {
    return planetId < strategyData.planetGraph.maxId / 2 && occupiedPlanets.count(planetId) == 0;
  });
  if (newPlanetIt == rangedPlanets.end()) {
    newPlanetIt = std::find_if(rangedPlanets.begin(), rangedPlanets.end(),
                               [&](uint32_t planetId) { return occupiedPlanets.count(planetId) == 0; });
  }
  auto newPlanet = *newPlanetIt;
  if(occupy) {
    occupiedPlanets.emplace(newPlanet);
  }
  return newPlanet;
}

uint32_t BaseStrategy::findNearestWithResource(uint32_t planetId, model::Resource resource, bool occupy) {
  auto rangedPlanets = strategyData.planetGraph.rangedPlanets[planetId];

  auto newPlanetIt = std::find_if(rangedPlanets.begin(), rangedPlanets.end(), [&](uint32_t planetId) {
    return planetId < strategyData.planetGraph.maxId / 2 && occupiedPlanets.count(planetId) == 0 &&
           strategyData.planets[planetId].harvestableResource.has_value() &&
           strategyData.planets[planetId].harvestableResource.value() == resource;
  });
  if (newPlanetIt == rangedPlanets.end()) {
    newPlanetIt = std::find_if(rangedPlanets.begin(), rangedPlanets.end(), [&](uint32_t planetId) {
      return occupiedPlanets.count(planetId) == 0 && strategyData.planets[planetId].harvestableResource.has_value() &&
             strategyData.planets[planetId].harvestableResource.value() == resource;
    });
  }
  auto newPlanet = *newPlanetIt;
  if(occupy) {
    occupiedPlanets.emplace(newPlanet);
  }
  return newPlanet;
}