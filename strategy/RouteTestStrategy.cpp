//
// Created by nikita on 11/24/21.
//
#include "RouteTestStrategy.h"

RouteTestStrategy::RouteTestStrategy(Tasks &tasks) : BaseStrategy(tasks), productionPlans(tasks) {}

void RouteTestStrategy::firstTick() {
  auto orePlanet = findNearestWithResource(0, model::Resource::ORE);
  auto metalPlanet = findNearest(orePlanet);
  auto metalPlanet2 = findNearest(orePlanet);
  productionPlans.addBuilding(orePlanet, model::BuildingType::MINES, 40);
  productionPlans.addBuilding(metalPlanet, model::BuildingType::FOUNDRY, 10);
  productionPlans.connectBuildings(orePlanet, metalPlanet, model::Resource::ORE, 10);
  productionPlans.addBuilding(metalPlanet2, model::BuildingType::FOUNDRY, 10);
  productionPlans.connectBuildings(orePlanet, metalPlanet2, model::Resource::ORE, 10);
/*  auto sandPlanet = findNearestWithResource(0, model::Resource::SAND);
  auto siliconPlanet = findNearest(sandPlanet);
  productionPlans.addBuilding(sandPlanet, model::BuildingType::CAREER, 20);
  productionPlans.addBuilding(siliconPlanet, model::BuildingType::FURNACE, 10);
  productionPlans.connectBuildings(sandPlanet, siliconPlanet, model::Resource::SAND, 10);
  auto chipPlanet = findNearest(metalPlanet);
  productionPlans.addBuilding(chipPlanet, model::BuildingType::CHIP_FACTORY, 5);
  productionPlans.connectBuildings(siliconPlanet, chipPlanet, model::Resource::SILICON, 5);
  productionPlans.connectBuildings(metalPlanet, chipPlanet, model::Resource::METAL, 5);*/

  productionPlans.newTick();
}

void RouteTestStrategy::newTick(uint32_t) {
  productionPlans.newTick();
}
