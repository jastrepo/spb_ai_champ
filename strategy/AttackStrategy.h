//
// Created by nikita on 11/24/21.
//
#pragma once

#include "BaseStrategy.h"
#include <productionPlan/ProductionPlans.h>

struct AttackStrategy : public BaseStrategy {
  ProductionPlans productionPlans;

  explicit AttackStrategy(Tasks &tasks);

  void firstTick() override;

  void newTick(uint32_t tickNumber) override;
};