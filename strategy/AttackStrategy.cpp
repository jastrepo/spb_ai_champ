//
// Created by nikita on 11/24/21.
//
#include "AttackStrategy.h"

AttackStrategy::AttackStrategy(Tasks &tasks) : BaseStrategy(tasks), productionPlans(tasks) {}

void AttackStrategy::firstTick() {
  productionPlans.addBuilding(0, model::BuildingType::QUARRY, 0);
  strategyData.buildingPlan.addBuilding(0, model::BuildingType::QUARRY);
  tasks.attack.add(0, 950, strategyData.constants.myIndex);
  auto organics = findNearestWithResource(0, model::Resource::ORGANICS);
  productionPlans.addBuilding(organics, model::BuildingType::FARM, 50);
  strategyData.buildingPlan.addBuilding(organics, model::BuildingType::FARM);
  productionPlans.newTick();
}

void AttackStrategy::newTick(uint32_t) {
  productionPlans.newTick();
}
