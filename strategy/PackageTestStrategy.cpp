//
// Created by nikita on 10/29/21.
//
#include "PackageTestStrategy.h"
#include <iostream>

using std::cout;
using std::endl;

PackageTestStrategy::PackageTestStrategy(Tasks &tasks) : BaseStrategy(tasks) {}

void PackageTestStrategy::firstTick() {
  cout << strategyData.planets.maxId << endl;
  for (uint32_t i = 0; i < strategyData.planets.maxId; ++i) {
    tasks.logistics.add(0, i, 1, std::nullopt, strategyData.constants.myIndex);
  }
  uint32_t planet0 = 0;
  auto planet1 = strategyData.planetGraph.rangedPlanets[0][0], planet2 = planet1, planet3 = planet1, planet4 = planet1;
  for (auto planetId: strategyData.planetGraph.rangedPlanets[planet0]) {
    if (planetId != planet0) {
      planet1 = planetId;
      break;
    }
  }
  for (auto planetId: strategyData.planetGraph.rangedPlanets[planet1]) {
    if (planetId != planet0 && planetId != planet1) {
      planet2 = planetId;
      break;
    }
  }
  for (auto planetId: strategyData.planetGraph.rangedPlanets[planet2]) {
    if (planetId != planet0 && planetId != planet1 && planetId != planet2) {
      planet3 = planetId;
      break;
    }
  }
  for (auto planetId: strategyData.planetGraph.rangedPlanets[planet3]) {
    if (planetId != planet0 && planetId != planet1 && planetId != planet2 && planetId != planet3) {
      planet4 = planetId;
      break;
    }
  }
  tasks.routes.add(planet0, planet1, 20, model::Resource::STONE);
  tasks.routes.add(planet1, planet2, 20, model::Resource::STONE);
  tasks.routes.add(planet2, planet3, 20, model::Resource::STONE);
  tasks.routes.add(planet3, planet4, 20, model::Resource::ORE);
}

void PackageTestStrategy::newTick(uint32_t) {

}