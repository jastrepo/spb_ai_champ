//
// Created by nikita on 11/8/21.
//
#pragma once

#include "BaseStrategy.h"
#include <productionPlan/ProductionPlans.h>

struct FleeStrategy : public BaseStrategy {
  ProductionPlans productionPlans;

  explicit FleeStrategy(Tasks &tasks);

  void firstTick() override;

  void newTick(uint32_t tickNumber) override;
};