//
// Created by nikita on 11/3/21.
//
#pragma once

#include <strategyData/StrategyData.h>
#include <tasks/Tasks.h>

struct BaseStrategy {
  std::optional<model::Specialty> specialty;

  explicit BaseStrategy(Tasks &tasks);

  virtual ~BaseStrategy() = default;

  std::unordered_set<uint32_t> occupiedPlanets{0};

  Tasks &tasks;
  StrategyData &strategyData;

  uint32_t findNearest(uint32_t planetId, bool occupy = true);

  uint32_t findNearestWithResource(uint32_t planetId, model::Resource resource, bool occupy = true);

  virtual void firstTick() = 0;

  virtual void newTick(uint32_t tickNumber) = 0;
};