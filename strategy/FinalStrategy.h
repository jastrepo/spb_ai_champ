//
// Created by nikita on 11/30/21.
//
#pragma once

#include "BaseStrategy.h"
#include <productionPlan/ProductionPlans.h>
#include <positionFinder/ClosePlanets.h>

struct FinalStrategy : public BaseStrategy {
  ProductionPlans productionPlans;

  explicit FinalStrategy(Tasks &tasks);

  void firstTick() override;

  void newTick(uint32_t tickNumber) override;

  ClosePlanets closePlanets;
  uint32_t replicatorPlanet = 0;

  std::vector<bool> visited;
  bool positionFound = false, planFound = false;
};