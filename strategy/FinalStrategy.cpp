//
// Created by nikita on 11/30/21.
//
#include <productionPlan/maxLoad.h>
#include <cmath>
#include "FinalStrategy.h"

FinalStrategy::FinalStrategy(Tasks &tasks) : BaseStrategy(tasks), productionPlans(tasks), closePlanets(strategyData) {}

void FinalStrategy::firstTick() {
  visited.resize(strategyData.planets.maxId);
  for (const auto &planet: strategyData.planets) {
    if (!visited[planet.id]) {
      for (const auto &sourcePlanetId: strategyData.planetGraph.rangedPlanets[planet.id]) {
        if (visited[planet.id]) {
          break;
        }
        for (auto playerIndex: strategyData.team.teamPlayer) {
          if (visited[planet.id]) {
            break;
          }
          auto group = tasks.logistics.add(sourcePlanetId, planet.id, 1, std::nullopt, playerIndex);
          if (group != nullptr) {
            visited[planet.id] = true;
          }
        }
      }
    }
  }
}

void FinalStrategy::newTick(uint32_t) {
  if (positionFound) {
    if (planFound) {
      if (productionPlans.completed) {
        for (const auto &planet: strategyData.planets) {
          for (auto playerIndex: strategyData.team.teamPlayer) {
            if (strategyData.resourcePlanner.getPlayerRobotsOnPlanet(planet.id, strategyData.constants.myIndex) >
                strategyData.weights.attackGroupMinSize) {
              tasks.attack.add(planet.id, strategyData.resourcePlanner.getPlayerRobotsOnPlanet(planet.id, playerIndex),
                               playerIndex);
            }
          }
        }
      }
    } else {
      const auto &result0 = findMaxLoad(replicatorPlanet, strategyData.buildingPlan);
      auto resourceNotFound = result0.second;

      if (result0.first.robotsOnRoutes > 1e-5) {
        planFound = true;
        for (const auto[_, building]: result0.first.buildings) {
          productionPlans.addBuilding(building.planetId, building.buildingType,
                                      uint32_t(ceil(building.properties.maxWorkers * building.load)));
        }
        for (const auto[_, route]: result0.first.routes) {
          productionPlans.connectBuildings(route.startPlanetId, route.endPlanetId,
                                           strategyData.buildingPlan.buildings.at(
                                               route.startPlanetId).second.produceResource.value(),
                                           route.resources);
        }

        for (int i = 0; i < 1; ++i) {
          model::BuildingType necessaryBuilding = model::BuildingType::QUARRY;
          std::optional<model::Resource> necessaryResource;
          for (const auto &[buildingType, buildingProperties]: strategyData.constants.buildingProperties) {
            if (buildingProperties.produceResource.has_value() &&
                buildingProperties.produceResource.value() == resourceNotFound) {
              if (buildingProperties.harvest) {
                necessaryResource = resourceNotFound;
              }
              necessaryBuilding = buildingType;
              break;
            }
          }

          if (necessaryBuilding != model::BuildingType::QUARRY) {
            double minScore = Constants::inf;
            uint32_t bestPlanet = Constants::inf;
            for (const auto &planet: strategyData.planets.planets) {
              if (planet.id < strategyData.planets.maxId && planet.harvestableResource == necessaryResource) {
                if (strategyData.buildingPlan.buildings.count(planet.id) == 0) {
                  double currentScore = 0;
                  for (const auto &[_, building]: productionPlans.planBuildings) {
                    double rounds = double(building.planNumber) / building.properties.workAmount;
                    if (building.properties.workResources.count(resourceNotFound) != 0) {
                      currentScore += strategyData.planetGraph.getRealDistance(building.planetId, planet.id) *
                                      building.properties.workResources.at(resourceNotFound) * rounds;
                    } else if (building.properties.produceResource.has_value()) {
                      if (strategyData.constants.buildingProperties.at(necessaryBuilding).workResources.count(
                          building.properties.produceResource.value()) != 0) {
                        currentScore += strategyData.planetGraph.getRealDistance(building.planetId, planet.id) *
                                        building.properties.produceAmount * rounds;
                      }
                      if (building.properties.produceResource.value() == resourceNotFound) {
                        currentScore -= strategyData.planetGraph.getRealDistance(building.planetId, planet.id) *
                                        building.properties.produceAmount * rounds;
                      }
                    }
                  }
                  if (currentScore < minScore) {
                    minScore = currentScore;
                    bestPlanet = planet.id;
                  }
                }
              }
            }

            if (bestPlanet < Constants::inf) {
              strategyData.buildingPlan.addBuilding(bestPlanet, necessaryBuilding);
              const auto &result = findMaxLoad(replicatorPlanet, strategyData.buildingPlan);
              if (result0.first.robotsOnRoutes > 1e-5) {
                resourceNotFound = result.second;

                for (const auto[_, building]: result.first.buildings) {
                  if (productionPlans.planBuildings.count(building.planetId) == 0) {
                    productionPlans.addBuilding(building.planetId, building.buildingType,
                                                uint32_t(ceil(building.properties.maxWorkers * building.load)));
                  } else {
                    productionPlans.changeBuildingPlanNumber(building.planetId, uint32_t(
                        ceil(building.properties.maxWorkers * building.load)));
                  }
                }
                for (const auto[_, route]: result.first.routes) {
                  if (productionPlans.planRoutes.count(PlanetPair(route.startPlanetId, route.endPlanetId)) == 0) {
                    productionPlans.connectBuildings(route.startPlanetId, route.endPlanetId,
                                                     strategyData.buildingPlan.buildings.at(
                                                         route.startPlanetId).second.produceResource.value(),
                                                     route.resources);
                  } else {
                    productionPlans.changeConnectionPlanNumber(route.startPlanetId, route.endPlanetId,
                                                               route.resources);
                  }
                }
              }
            }
          }
        }
      }
    }
  } else {
    closePlanets.findTeamPlanets();
    for (auto teamPlanet: closePlanets.teamPlanets) {
      if (strategyData.planets[teamPlanet].building.has_value() &&
          strategyData.planets[teamPlanet].building.value().buildingType == model::BuildingType::QUARRY) {
        strategyData.buildingPlan.addBuilding(teamPlanet, model::BuildingType::QUARRY);
      }
    }
    if (closePlanets.findClosePlanetsForTeam()) {
      positionFound = true;
      auto startPosition = closePlanets.findBestPosition();
      replicatorPlanet = startPosition.replicatorPlanet;

      if (replicatorPlanet < strategyData.planets.maxId) {
        strategyData.buildingPlan.addBuilding(startPosition.orePlanet, model::BuildingType::MINES);
        strategyData.buildingPlan.addBuilding(startPosition.sandPlanet, model::BuildingType::CAREER);
        strategyData.buildingPlan.addBuilding(startPosition.organicsPlanet, model::BuildingType::FARM);
        strategyData.buildingPlan.addBuilding(startPosition.metalPlanet, model::BuildingType::FOUNDRY);
        strategyData.buildingPlan.addBuilding(startPosition.siliconPlanet, model::BuildingType::FURNACE);
        strategyData.buildingPlan.addBuilding(startPosition.plasticPlanet, model::BuildingType::BIOREACTOR);
        strategyData.buildingPlan.addBuilding(startPosition.chipPlanet, model::BuildingType::CHIP_FACTORY);
        strategyData.buildingPlan.addBuilding(startPosition.accumulatorPlanet,
                                              model::BuildingType::ACCUMULATOR_FACTORY);
        strategyData.buildingPlan.addBuilding(replicatorPlanet, model::BuildingType::REPLICATOR);
      }
    }
  }
  for (auto teamPlanet: closePlanets.teamPlanets) {
    if (strategyData.planets[teamPlanet].building.has_value() &&
        strategyData.planets[teamPlanet].building.value().buildingType == model::BuildingType::QUARRY) {
      productionPlans.changeBuildingPlanNumber(teamPlanet, strategyData.weights.stayOnStonePlanet);
    }
  }
  productionPlans.newTick();

  visited.resize(strategyData.planets.maxId);
  for (const auto &planet: strategyData.planets) {
    if (!visited[planet.id]) {
      for (const auto &sourcePlanetId: strategyData.planetGraph.rangedPlanets[planet.id]) {
        if (visited[planet.id]) {
          break;
        }
        for (auto playerIndex: strategyData.team.teamPlayer) {
          if (visited[planet.id]) {
            break;
          }
          auto group = tasks.logistics.add(sourcePlanetId, planet.id, 1, std::nullopt, playerIndex);
          if (group != nullptr) {
            visited[planet.id] = true;
          }
        }
      }
    }
  }
}
