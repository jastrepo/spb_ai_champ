#ifndef __STREAM_HPP__
#define __STREAM_HPP__

#include <string>

// Input stream interface
class InputStream {
public:
  virtual ~InputStream() = default;

  // Read exactly byteCount bytes into buffer
  virtual void readBytes(char *buffer, size_t byteCount) = 0;

  // Read a bool from this stream
  bool readBool();

  // Read an int32 from this stream
  int32_t readInt32();

  // Read an unsigned int32 from this stream
  uint32_t readUInt32();

  // Read an int64 from this stream
  int64_t readInt64();

  // Read a float from this stream
  float readFloat();

  // Read a double from this stream
  double readDouble();

  // Read a string from this stream
  std::string readString();
};

// Output stream interface
class OutputStream {
public:
  virtual ~OutputStream() = default;

  // Write byte buffer into this stream
  virtual void writeBytes(const char *buffer, size_t byteCount) = 0;

  // Flush this stream
  virtual void flush() = 0;

  // Write a bool into this stream
  void write(bool value);

  // Write an int32 into this stream
  void write(int32_t value);

  // Write an unsigned int32 into this stream
  void write(uint32_t value);

  // Write an int64 into this stream
  void write(int64_t value);

  // Write a float into this stream
  void write(float value);

  // Write a double into this stream
  void write(double value);

  // Write a string into this stream
  void write(const std::string &value);
};

#endif