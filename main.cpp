#include "Runner.h"

using std::string;

void startRunner(const string &host, const string &port, const string &token, bool multiplayer = false) {
    auto runner = Runner(host, port, token);
    runner.run();
}

int main(int argc, char **argv) {
  std::string host = argc < 2 ? "127.0.0.1" : argv[1];
  std::string port = argc < 3 ? "31001" : argv[2];
  std::string token = argc < 4 ? "0000000000000000" : argv[3];
  startRunner(host, port, token, true);
  return 0;
}