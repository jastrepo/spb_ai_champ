//
// Created by nikita on 11/23/21.
//
#pragma once

#include "managers/Logistics.h"
#include <strategyData/StrategyData.h>
#include <tasks/managers/Attack.h>
#include <tasks/managers/Routes.h>
#include <tasks/managers/Builds.h>
#include <tasks/managers/Defence.h>

struct Tasks {
  class TaskIterator;

  class ManagerTasks {
    std::unordered_map<size_t, std::shared_ptr<Task>> &tasks;
    const std::unordered_map<size_t, std::reference_wrapper<TaskManager>> &managerById;
    std::reference_wrapper<TaskManager> manager;

    ManagerTasks(std::unordered_map<size_t, std::shared_ptr<Task>> &newTasks,
                 const std::unordered_map<size_t, std::reference_wrapper<TaskManager>> &newManagerById,
                 const std::reference_wrapper<TaskManager> &newManager);

  public:
    TaskIterator begin();

    TaskIterator end();

    friend Tasks;
  };

  class TaskIterator {
    std::unordered_map<size_t, std::shared_ptr<Task>>::iterator iterator, end;
    const std::unordered_map<size_t, std::reference_wrapper<TaskManager>> &managerById;
    std::reference_wrapper<TaskManager> manager;

    TaskIterator(std::unordered_map<size_t, std::shared_ptr<Task>>::iterator newIterator,
                 std::unordered_map<size_t, std::shared_ptr<Task>>::iterator newEnd,
                 const std::unordered_map<size_t, std::reference_wrapper<TaskManager>> &newManagerById,
                 std::reference_wrapper<TaskManager> newManager);

  public:
    std::shared_ptr<Task> operator*();

    TaskIterator &operator++();

    bool operator==(const TaskIterator &second) const;

    bool operator!=(const TaskIterator &second) const;

    friend ManagerTasks;
  };

  StrategyData strategyData;

  size_t nextId = 0;
  std::unordered_map<size_t, std::shared_ptr<Task>> taskById;
  std::unordered_map<size_t, std::reference_wrapper<TaskManager>> managerById;
  std::vector<std::reference_wrapper<TaskManager>> managers;

  ManagerTasks managerTasks(std::reference_wrapper<TaskManager> manager);

  Tasks();

  void removeFinished();

  void newTick(const model::Game &game);

  void finishTurn();

  void add(const std::shared_ptr<Task> &newTask, std::reference_wrapper<TaskManager> manager);

  void cancel(size_t id);

  bool finished(size_t id) const;

  Defence defence;
  Attack attack;
  Logistics logistics;
  Routes routes;
  Builds builds;
};