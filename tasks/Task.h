//
// Created by nikita on 11/23/21.
//
#pragma once

#include <cstddef>
#include <limits>

struct Task {
  Task() = default;

  virtual ~Task() = default;

  size_t id = std::numeric_limits<size_t>::max();
  bool finished = true;
};