//
// Created by nikita on 11/23/21.
//
#pragma once

#include <tasks/Task.h>
#include <model/FlyingWorkerGroup.hpp>

struct LogisticsTask : public Task {
  bool flying = false, lifting = false;
  model::FlyingWorkerGroup flyingGroup;
  uint32_t currentPlanet = 0, nextPlanet = 0;
  uint32_t endPlanet = 0;
  uint32_t number = 0;
  uint32_t playerIndex = 0;
  std::optional<model::Resource> resource;

  LogisticsTask() = default;

  LogisticsTask(uint32_t startPlanet, uint32_t endPlanet, uint32_t number, uint32_t playerIndex,
                std::optional<model::Resource> resource);
};

namespace std {
  template<>
  struct hash<LogisticsTask> {
    std::size_t operator()(const LogisticsTask &task) const noexcept;
  };

  template<>
  struct equal_to<LogisticsTask> {
    bool operator()(const LogisticsTask &first, const LogisticsTask &second) const noexcept;
  };
}