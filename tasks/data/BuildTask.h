//
// Created by nikita on 11/25/21.
//
#pragma once

#include <tasks/Task.h>
#include <model/BuildingProperties.hpp>
#include <vector>

struct BuildTask : public Task {
  bool started = false;
  size_t baseBuildId = std::numeric_limits<size_t>::max();
  model::BuildingType buildingType = model::BuildingType::QUARRY;
  model::BuildingProperties buildingProperties;
  std::vector<size_t> logisticsIds;
  uint32_t planetId = 0;

  BuildTask() = default;

  BuildTask(size_t baseBuildId, model::BuildingType buildingType, model::BuildingProperties buildingProperties,
            uint32_t planetId);
};