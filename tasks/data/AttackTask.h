//
// Created by nikita on 11/24/21.
//
#pragma once

#include <tasks/Task.h>
#include <model/FlyingWorkerGroup.hpp>

struct AttackTask : public Task {
  bool flying = false, lifting = false;
  model::FlyingWorkerGroup flyingGroup;
  uint32_t currentPlanet = 0, nextPlanet = 0;
  uint32_t playerIndex = 0;
  uint32_t number = 0;

  AttackTask() = default;

  AttackTask(uint32_t currentPlanet, uint32_t playerIndex, uint32_t number);
};