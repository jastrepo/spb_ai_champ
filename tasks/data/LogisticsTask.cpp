//
// Created by nikita on 11/23/21.
//
#include "LogisticsTask.h"

LogisticsTask::LogisticsTask(uint32_t startPlanet, uint32_t endPlanet, uint32_t number, uint32_t playerIndex,
                             std::optional<model::Resource> resource)
    : currentPlanet(startPlanet), nextPlanet(currentPlanet), endPlanet(endPlanet), number(number),
      playerIndex(playerIndex), resource(resource) {}

std::size_t std::hash<LogisticsTask>::operator()(const LogisticsTask &task) const noexcept {
  size_t resourceHash = 0;
  if (task.resource.has_value()) {
    resourceHash = size_t(task.resource.value()) + 1;
  }

  size_t result = 0;
  result = (result << 3) + size_t(task.playerIndex);
  result = (result << 8) + size_t(task.currentPlanet);
  result = (result << 8) + size_t(task.endPlanet);
  result = (result << 4) + resourceHash;
  return result;
}

bool std::equal_to<LogisticsTask>::operator()(const LogisticsTask &first, const LogisticsTask &second) const noexcept {
  return std::hash<LogisticsTask>()(first) == std::hash<LogisticsTask>()(second);
}