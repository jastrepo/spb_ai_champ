//
// Created by nikita on 11/24/21.
//
#include "RouteTask.h"

RouteTask::RouteTask(uint32_t startPlanetId, uint32_t endPlanetId, uint32_t number, model::Resource resource)
    : startPlanetId(startPlanetId), endPlanetId(endPlanetId), number(number), resource(resource) {}