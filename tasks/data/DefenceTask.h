//
// Created by nikita on 11/30/21.
//
#pragma once

#include <tasks/Task.h>
#include <model/FlyingWorkerGroup.hpp>
#include <vector>
#include <unordered_set>
#include <unordered_map>

struct DefenceTask : public Task {
  std::unordered_set<uint32_t> planets;
  std::vector<uint32_t> planetRobots;
  std::vector<model::FlyingWorkerGroup> flyingGroups, liftingGroups;
  model::FlyingWorkerGroup opponentFlyingGroup;
  uint32_t opponentPlanetId = 0;
  uint32_t opponentPlayerIndex = 0;
  std::unordered_map<uint32_t, uint32_t> defenceRobotsForPlanet;
  bool opponentFlying = false;
  uint32_t robotsNumber = 0;
  uint32_t playerIndex = 0;

  DefenceTask() = default;

  DefenceTask(model::FlyingWorkerGroup opponentFlyingGroup, uint32_t playerIndex);
};