//
// Created by nikita on 11/25/21.
//
#include "BuildTask.h"

BuildTask::BuildTask(size_t baseBuildId, model::BuildingType buildingType, model::BuildingProperties buildingProperties,
                     uint32_t planetId)
    : baseBuildId(baseBuildId), buildingType(buildingType), buildingProperties(std::move(buildingProperties)),
      planetId(planetId) {}