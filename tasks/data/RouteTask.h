//
// Created by nikita on 11/24/21.
//
#pragma once

#include <vector>
#include "LogisticsTask.h"

struct RouteTask : public Task {
  uint32_t startPlanetId = 0, endPlanetId = 0;
  uint32_t number = 0, robotsInFly = 0;
  model::Resource resource = model::Resource::STONE;
  std::vector<size_t> logisticsTaskIds;

  RouteTask() = default;

  RouteTask(uint32_t startPlanetId, uint32_t endPlanetId, uint32_t number, model::Resource resource);
};