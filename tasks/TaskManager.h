//
// Created by nikita on 11/23/21.
//
#pragma once

#include <memory>
#include <model/Game.hpp>
#include "Task.h"

struct Tasks;
struct StrategyData;

struct TaskManager {
  Tasks &tasks;
  StrategyData &strategyData;

  explicit TaskManager(Tasks &tasks);

  virtual ~TaskManager() = default;

  bool ownTaskWithId(size_t id);

  bool finished(size_t id);

  void cancelWithId(size_t id);

  virtual void cancel(std::shared_ptr<Task> task) = 0;

  virtual void newTick(const model::Game &game) = 0;

  virtual void finishTurn() {}
};