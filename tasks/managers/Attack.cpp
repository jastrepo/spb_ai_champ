//
// Created by nikita on 11/24/21.
//
#include "Attack.h"
#include <tasks/Tasks.h>
#include <utils/vectorHelper.h>

Attack::Attack(Tasks &tasks) : TaskManager(tasks) {}

void Attack::execute(AttackTask &task) {
  task.flying = false;
  task.finished = true;
  task.lifting = false;

  bool robotsSent = false;
  if (strategyData.planets[task.currentPlanet].building.has_value() &&
      strategyData.buildingPlan.buildings.count(task.currentPlanet) == 0) {
    robotsSent = strategyData.buildActions.add({task.currentPlanet, std::nullopt}, task.playerIndex, task.number);
  } else {
    for (const auto &nextPlanetId: strategyData.planetGraph.rangedPlanets[task.currentPlanet]) {
      if (strategyData.planets[nextPlanetId].building.has_value() &&
          strategyData.buildingPlan.buildings.count(nextPlanetId) == 0) {
        task.nextPlanet = strategyData.planetGraph.nextPlanet[task.currentPlanet][nextPlanetId];
        if (task.nextPlanet != task.currentPlanet) {
          const auto &newGroup = strategyData.moveActions.add(
              {task.currentPlanet, task.nextPlanet, task.number, std::nullopt}, task.playerIndex);
          if (newGroup.number > 0) {
            task.lifting = true;
            task.number = newGroup.number;
            task.flyingGroup = newGroup;
            robotsSent = true;
            break;
          }
        }
      }
    }
  }
  if (!robotsSent) {
    task.number = strategyData.resourcePlanner.takePlayerRobotsFromPlanet(task.currentPlanet, task.playerIndex,
                                                                          task.number);
  }
  if (task.number > 0) {
    task.finished = false;
  }
}

void Attack::newTick(const model::Game &) {
  auto expectedEvents = strategyData.flyingGroups.expected, unexpectedEvents = strategyData.flyingGroups.unexpected;
  auto expectedToFly = expectedEvents.fly, expectedToLift = expectedEvents.lift, expectedToLand = expectedEvents.land,
      unexpectedLostNumber = unexpectedEvents.lostNumber;
  for (const auto &taskPtr: tasks.managerTasks(*this)) {
    AttackTask &task = *std::dynamic_pointer_cast<AttackTask>(taskPtr);
    {
      auto group = findElementAndRemove(unexpectedLostNumber, task.flyingGroup);
      if (group.has_value()) {
        task.flyingGroup = group.value();
        task.number = task.flyingGroup.number;
      }
    }
    if (task.flying) {
      if (!findValueAndRemove(expectedToFly, task.flyingGroup)) {
        if (findValueAndRemove(expectedToLand, task.flyingGroup)) {
          task.flying = false;
          task.currentPlanet = task.nextPlanet;
          execute(task);
        } else {
          task.finished = true;
        }
      }
    } else if (task.lifting) {
      task.lifting = false;

      auto groupIt = std::find_if(expectedToLift.begin(), expectedToLift.end(),
                                  FlyingGroups::groupLifted(task.flyingGroup));
      if (groupIt == expectedToLift.end()) {
        task.finished = true;
      } else {
        task.flyingGroup = *groupIt;
        task.flying = true;
        expectedToLift.erase(groupIt);
      }
    } else {
      execute(task);
    }
  }
}

void Attack::cancel(std::shared_ptr<Task> taskPtr) {
  AttackTask &task = *std::dynamic_pointer_cast<AttackTask>(taskPtr);
  if (task.lifting && !task.finished) {
    strategyData.moveActions.cancel({task.currentPlanet, task.nextPlanet, task.number, std::nullopt}, task.playerIndex);
    task.finished = true;
  }
}

std::shared_ptr<AttackTask> Attack::add(uint32_t startPlanet, uint32_t number, uint32_t playerIndex) {
  AttackTask newTask(startPlanet, playerIndex, number);
  if (number > 0) {
    execute(newTask);
    if (!newTask.finished) {
      auto newTaskPtr = std::make_shared<AttackTask>(newTask);
      tasks.add(newTaskPtr, *this);
      return newTaskPtr;
    }
  }
  return nullptr;
}

AttackTask Attack::withId(size_t id) {
  if (ownTaskWithId(id)) {
    return *std::dynamic_pointer_cast<AttackTask>(tasks.taskById.at(id));
  } else {
    return {};
  }
}