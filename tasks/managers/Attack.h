//
// Created by nikita on 11/24/21.
//
#pragma once

#include <tasks/TaskManager.h>
#include <tasks/data/AttackTask.h>

struct Attack : public TaskManager {
  explicit Attack(Tasks &tasks);

  ~Attack() override = default;

  void execute(AttackTask &task);

  void newTick(const model::Game &game) override;

  void cancel(std::shared_ptr<Task> taskPtr) override;

  std::shared_ptr<AttackTask> add(uint32_t startPlanet, uint32_t number, uint32_t playerIndex);

  AttackTask withId(size_t id);
};