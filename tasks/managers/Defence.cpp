//
// Created by nikita on 11/29/21.
//
#include "Defence.h"
#include <strategyData/StrategyData.h>
#include <tasks/Tasks.h>
#include <utils/vectorHelper.h>
#include <log/Loggers.h>

Defence::Defence(Tasks &tasks) : TaskManager(tasks) {}

void Defence::execute(DefenceTask &task) {
  if (!task.finished) {
    uint32_t opponentNumber;
    model::FlyingWorkerGroup opponentGroup;
    if (task.opponentFlying) {
      opponentGroup = task.opponentFlyingGroup;
      opponentNumber = task.opponentFlyingGroup.number;
    } else {
      opponentGroup = {task.opponentPlayerIndex, opponentRobotsOnPlanet[task.opponentPlanetId], 0,
                       task.opponentPlanetId, strategyData.currentTick, task.opponentPlanetId, task.opponentPlanetId,
                       std::nullopt};
      opponentNumber = opponentRobotsOnPlanet[task.opponentPlanetId];
    }

    task.defenceRobotsForPlanet.clear();
    std::unordered_map<uint32_t, std::vector<uint32_t>> attackVectorsByPlanet;

    bool targetIsProtectedPlanet = false;
    for (const auto &[protectedPlanetId, _]: strategyData.buildingPlan.buildings) {
      if (opponentGroup.targetPlanet == protectedPlanetId) {
        task.defenceRobotsForPlanet[protectedPlanetId] = opponentNumber;
        auto currentPlanet = opponentGroup.nextPlanet;
        while (strategyData.planetGraph.nextPlanet[currentPlanet][protectedPlanetId] != currentPlanet) {
          attackVectorsByPlanet[currentPlanet].emplace_back(protectedPlanetId);
          currentPlanet = strategyData.planetGraph.nextPlanet[currentPlanet][protectedPlanetId];
        }
        targetIsProtectedPlanet = true;
        break;
      }
    }

    if (!targetIsProtectedPlanet) {
      uint32_t potentialLoss = 0;
      std::vector<uint32_t> planetPotentialLoss(strategyData.planets.maxId);

      for (const auto &[protectedPlanetId, _]: strategyData.buildingPlan.buildings) {
        auto opponentNumberOnPlanet = opponentNumber;
        uint32_t opponentArrivalTick =
            strategyData.planetGraph.getRealDistance(protectedPlanetId, opponentGroup.targetPlanet) +
            strategyData.planetGraph.getRealDistance(opponentGroup.targetPlanet, opponentGroup.nextPlanet) +
            opponentGroup.nextPlanetArrivalTick;
        for (const auto &[_, groupIndex]: rangedGroups[protectedPlanetId]) {
          const auto &ownGroup = ownGroups[groupIndex];
          if (opponentNumberOnPlanet > 0) {
            auto groupArrivalTick = strategyData.planetGraph.getRealDistance(protectedPlanetId, ownGroup.targetPlanet) +
                                    strategyData.planetGraph.getRealDistance(ownGroup.targetPlanet,
                                                                             ownGroup.nextPlanet) +
                                    ownGroup.nextPlanetArrivalTick;
            if (groupArrivalTick <= opponentArrivalTick) {
              if (opponentNumberOnPlanet > ownGroup.number) {
                opponentNumberOnPlanet -= ownGroup.number;
              } else {
                opponentNumberOnPlanet = 0;
              }
            }
          }
        }

        planetPotentialLoss[protectedPlanetId] = opponentNumberOnPlanet;
        potentialLoss += opponentNumberOnPlanet;
      }

      if (potentialLoss > 0) {
        uint32_t remainPotentialLoss = potentialLoss;
        for (uint32_t planetId = 0; planetId < planetPotentialLoss.size(); ++planetId) {
          if (planetPotentialLoss[planetId] > 0) {
            auto defenceRobots = opponentNumber * planetPotentialLoss[planetId] / potentialLoss;
            task.defenceRobotsForPlanet.emplace(planetId, defenceRobots);
            remainPotentialLoss -= defenceRobots;
          }
        }
        for (auto &[planetId, defenceRobots]: task.defenceRobotsForPlanet) {
          if (remainPotentialLoss > 0) {
            --remainPotentialLoss;
            ++defenceRobots;
          }
        }

        for (auto &[planetId, defenceRobots]: task.defenceRobotsForPlanet) {
          auto currentPlanet = opponentGroup.nextPlanet;
          while (strategyData.planetGraph.nextPlanet[currentPlanet][planetId] != currentPlanet) {
            attackVectorsByPlanet[currentPlanet].emplace_back(planetId);
            currentPlanet = strategyData.planetGraph.nextPlanet[currentPlanet][planetId];
          }
        }
      } else {
        task.defenceRobotsForPlanet[opponentGroup.targetPlanet] =
            task.robotsNumber > opponentGroup.number ? opponentGroup.number : task.robotsNumber;
        attackVectorsByPlanet[opponentGroup.targetPlanet].emplace_back(opponentGroup.targetPlanet);
      }
    }

    for (auto planetId: task.planets) {
      bool hasTarget = true;
      uint32_t unusedRobots = task.planetRobots[planetId];
      while (hasTarget && unusedRobots > 0) {
        hasTarget = false;
        uint32_t robotsToTarget = 0;
        uint32_t targetPlanetId = planetId;
        uint32_t opponentDistanceToTarget = Constants::inf;

        for (const auto &[startPlanetId, vectorPlanetIds]: attackVectorsByPlanet) {
          uint32_t defenceRobotsNumber = 0;
          for (auto vectorPlanetId: vectorPlanetIds) {
            defenceRobotsNumber += task.defenceRobotsForPlanet[vectorPlanetId];
          }

          if (defenceRobotsNumber > 0) {
            uint32_t opponentArrivalTick =
                strategyData.planetGraph.getRealDistance(startPlanetId, opponentGroup.targetPlanet) +
                strategyData.planetGraph.getRealDistance(opponentGroup.targetPlanet, opponentGroup.nextPlanet) +
                opponentGroup.nextPlanetArrivalTick;
            if (strategyData.planetGraph.getRealDistance(planetId, startPlanetId) + strategyData.currentTick <=
                opponentArrivalTick) {
              if ((opponentDistanceToTarget == opponentArrivalTick &&
                   strategyData.planetGraph.getRealDistance(planetId, startPlanetId) <
                   strategyData.planetGraph.getRealDistance(planetId, targetPlanetId)) ||
                  opponentDistanceToTarget > opponentArrivalTick) {
                robotsToTarget = defenceRobotsNumber;
                targetPlanetId = startPlanetId;
                opponentDistanceToTarget = opponentArrivalTick;
                hasTarget = true;
              }
            }
          }
        }

        if (robotsToTarget > 0) {
          uint32_t robotsToSend = unusedRobots;
          if (robotsToSend > robotsToTarget) {
            robotsToSend = robotsToTarget;
          }
          if (targetPlanetId == planetId) {
            robotsToSend = strategyData.resourcePlanner.takePlayerRobotsFromPlanet(planetId, task.playerIndex,
                                                                                   robotsToSend);
          } else {
            auto group = strategyData.moveActions.add({planetId, targetPlanetId, robotsToSend, std::nullopt},
                                                      task.playerIndex);
            robotsToSend = group.number;
            task.liftingGroups.emplace_back(group);
            task.planetRobots[planetId] -= robotsToSend;
          }
          if (unusedRobots > robotsToSend) {
            unusedRobots -= robotsToSend;
          } else {
            unusedRobots = 0;
          }
          for (auto &vectorPlanetId: attackVectorsByPlanet[targetPlanetId]) {
            if (task.defenceRobotsForPlanet[vectorPlanetId] > robotsToSend) {
              task.defenceRobotsForPlanet[vectorPlanetId] -= robotsToSend;
              robotsToSend = 0;
            } else {
              task.defenceRobotsForPlanet[vectorPlanetId] = 0;
              robotsToSend -= task.defenceRobotsForPlanet[vectorPlanetId];
            }
          }
        }
      }
      task.planetRobots[planetId] -= unusedRobots;
    }
    std::vector<model::FlyingWorkerGroup> usedGroups;
    for (const auto &flyingGroup: task.flyingGroups) {
      bool hasTarget = true;
      uint32_t unusedRobots = flyingGroup.number;
      while (hasTarget && unusedRobots > 0) {
        hasTarget = false;
        uint32_t robotsToTarget = 0;
        uint32_t targetPlanetId = flyingGroup.targetPlanet;
        uint32_t opponentDistanceToTarget = Constants::inf;

        for (const auto &[startPlanetId, vectorPlanetIds]: attackVectorsByPlanet) {
          uint32_t defenceRobotsNumber = 0;
          for (auto vectorPlanetId: vectorPlanetIds) {
            defenceRobotsNumber += task.defenceRobotsForPlanet[vectorPlanetId];
          }

          if (defenceRobotsNumber > 0) {
            uint32_t groupArrivalTick =
                strategyData.planetGraph.getRealDistance(startPlanetId, flyingGroup.targetPlanet) +
                strategyData.planetGraph.getRealDistance(flyingGroup.targetPlanet, flyingGroup.nextPlanet) +
                flyingGroup.nextPlanetArrivalTick;
            uint32_t opponentArrivalTick =
                strategyData.planetGraph.getRealDistance(startPlanetId, opponentGroup.targetPlanet) +
                strategyData.planetGraph.getRealDistance(opponentGroup.targetPlanet, opponentGroup.nextPlanet) +
                opponentGroup.nextPlanetArrivalTick;
            if (groupArrivalTick <= opponentArrivalTick) {
              if ((opponentDistanceToTarget == opponentArrivalTick &&
                   strategyData.planetGraph.getRealDistance(flyingGroup.targetPlanet, startPlanetId) <
                   strategyData.planetGraph.getRealDistance(flyingGroup.targetPlanet, targetPlanetId)) ||
                  opponentDistanceToTarget > opponentArrivalTick) {
                robotsToTarget = defenceRobotsNumber;
                targetPlanetId = startPlanetId;
                opponentDistanceToTarget = opponentArrivalTick;
                hasTarget = true;
              }
            }
          }
        }

        if (robotsToTarget > 0) {
          uint32_t robotsToSend = unusedRobots;
          if (robotsToSend > robotsToTarget) {
            robotsToSend = robotsToTarget;
          }
          if (unusedRobots > robotsToSend) {
            unusedRobots -= robotsToSend;
          } else {
            unusedRobots = 0;
          }
          for (auto &vectorPlanetId: attackVectorsByPlanet[targetPlanetId]) {
            if (task.defenceRobotsForPlanet[vectorPlanetId] > robotsToSend) {
              task.defenceRobotsForPlanet[vectorPlanetId] -= robotsToSend;
              robotsToSend = 0;
            } else {
              task.defenceRobotsForPlanet[vectorPlanetId] = 0;
              robotsToSend -= task.defenceRobotsForPlanet[vectorPlanetId];
            }
          }
        }
      }
      if (unusedRobots < flyingGroup.number) {
        usedGroups.emplace_back(flyingGroup);
      }
      if (unusedRobots != 0) {
        auto restGroup = flyingGroup;
        restGroup.number = unusedRobots;
        for (const auto &planet: strategyData.planets) {
          rangedGroups[planet.id].emplace(strategyData.planetGraph.getRealDistance(planet.id, restGroup.targetPlanet) +
                                          strategyData.planetGraph.getRealDistance(restGroup.targetPlanet,
                                                                                   restGroup.nextPlanet) +
                                          restGroup.nextPlanetArrivalTick - strategyData.currentTick, ownGroups.size());
        }
        ownGroups.emplace_back(restGroup);
      }
    }
    task.flyingGroups.swap(usedGroups);
  }
}

std::function<bool(const model::FlyingWorkerGroup &)> groupIsSameExceptNumber(
    const model::FlyingWorkerGroup &expectedGroup) {
  return [&](const model::FlyingWorkerGroup &realGroup) {
    return realGroup.playerIndex == expectedGroup.playerIndex &&
           realGroup.departureTick == expectedGroup.departureTick &&
           realGroup.departurePlanet == expectedGroup.departurePlanet &&
           realGroup.nextPlanetArrivalTick == expectedGroup.nextPlanetArrivalTick &&
           realGroup.nextPlanet == expectedGroup.nextPlanet &&
           realGroup.targetPlanet == expectedGroup.targetPlanet &&
           realGroup.resource == expectedGroup.resource;
  };
}

void Defence::newTick(const model::Game &game) {
  strategyData.resourcePlanner.considerEmployment = false;

  opponentRobotsOnPlanet.resize(strategyData.planets.maxId);
  std::fill(opponentRobotsOnPlanet.begin(), opponentRobotsOnPlanet.end(), 0);
  for (const auto &planet: strategyData.planets) {
    int robotsNumber = 0;
    for (const auto &group: planet.workerGroups) {
      if (strategyData.team.teamPlayer.count(group.playerIndex) == 0) {
        robotsNumber += int(group.number);
      } else {
        robotsNumber -= int(group.number);
      }
    }
    if (robotsNumber > 0) {
      opponentRobotsOnPlanet[planet.id] = uint32_t(robotsNumber);
    } else {
      opponentRobotsOnPlanet[planet.id] = 0;
    }
  }

  auto expectedEvents = strategyData.flyingGroups.expected, unexpectedEvents = strategyData.flyingGroups.unexpected;
  auto expectedToFly = expectedEvents.fly, expectedToLift = expectedEvents.lift, expectedToLand = expectedEvents.land,
      unexpectedToLand = unexpectedEvents.land;
  for (const auto &taskPtr: tasks.managerTasks(*this)) {
    DefenceTask &task = *std::dynamic_pointer_cast<DefenceTask>(taskPtr);
    if (task.opponentFlying) {
      if (findValueAndRemove(expectedToLand, task.opponentFlyingGroup) ||
          findValueAndRemove(unexpectedToLand, task.opponentFlyingGroup)) {
        task.finished = true;
      }
    } else {
      if (opponentRobotsOnPlanet[task.opponentPlanetId] == 0) {
        task.finished = true;
      }
    }

    if (!task.finished) {
      task.planetRobots.resize(strategyData.planets.maxId);
      std::vector<model::FlyingWorkerGroup> stillFlyingGroups;

      for (const auto &liftingGroup: task.liftingGroups) {
        auto groupIt = std::find_if(expectedToLift.begin(), expectedToLift.end(),
                                    FlyingGroups::groupLifted(liftingGroup));
        if (groupIt != expectedToLift.end()) {
          stillFlyingGroups.emplace_back(*groupIt);
          if (groupIt->number > task.planetRobots[groupIt->departurePlanet]) {
            task.planetRobots[groupIt->departurePlanet] -= groupIt->number;
          } else {
            task.planetRobots[groupIt->departurePlanet] = 0;
            task.planets.erase(groupIt->departurePlanet);
          }
          expectedToLift.erase(groupIt);
        }
      }
      task.liftingGroups.clear();

      for (const auto &flyingGroup: task.flyingGroups) {
        if (findValueAndRemove(expectedToFly, flyingGroup)) {
          stillFlyingGroups.emplace_back(flyingGroup);
        } else if (findValueAndRemove(expectedToLand, flyingGroup)) {
          task.planets.emplace(flyingGroup.targetPlanet);
          task.planetRobots[flyingGroup.targetPlanet] += flyingGroup.number;
        }
      }
      task.flyingGroups.swap(stillFlyingGroups);

      for (uint32_t planetId = 0; planetId < task.planetRobots.size(); ++planetId) {
        if (task.planetRobots[planetId] == 0) {
          task.planets.erase(planetId);
        }
      }
    }

    if (!task.finished) {
      for (auto planetId: task.planets) {
        if (strategyData.resourcePlanner.getPlayerRobotsOnPlanet(planetId, task.playerIndex) <
            task.planetRobots[planetId]) {
          task.planetRobots[planetId] = strategyData.resourcePlanner.getPlayerRobotsOnPlanet(planetId,
                                                                                             task.playerIndex);
        }
      }
      for (uint32_t planetId = 0; planetId < task.planetRobots.size(); ++planetId) {
        if (task.planetRobots[planetId] == 0) {
          task.planets.erase(planetId);
        }
      }
    }
  }

  ownGroups.clear();
  for (const auto &group: game.flyingWorkerGroups) {
    if (strategyData.team.teamPlayer.count(group.playerIndex) > 0) {
      uint32_t groupRobotsNumber = group.number;
      for (const auto &taskPtr: tasks.managerTasks(*this)) {
        DefenceTask &task = *std::dynamic_pointer_cast<DefenceTask>(taskPtr);
        if (task.playerIndex == group.playerIndex) {
          auto groupIt = std::find_if(task.flyingGroups.begin(), task.flyingGroups.end(),
                                      groupIsSameExceptNumber(group));
          if (groupIt != task.flyingGroups.end()) {
            if (groupRobotsNumber > groupIt->number) {
              groupRobotsNumber -= groupIt->number;
            } else {
              groupRobotsNumber = 0;
            }
          }
        }
      }
      if (groupRobotsNumber > 0) {
        auto restGroup = group;
        restGroup.number = groupRobotsNumber;
        ownGroups.emplace_back(restGroup);
      }
    }
  }
  for (const auto &planet: strategyData.planets) {
    for (auto playerIndex: strategyData.team.teamPlayer) {
      uint32_t planetRobotsNumber = strategyData.resourcePlanner.getPlayerRobotsOnPlanet(planet.id, playerIndex);
      for (const auto &taskPtr: tasks.managerTasks(*this)) {
        DefenceTask &task = *std::dynamic_pointer_cast<DefenceTask>(taskPtr);
        if (task.playerIndex == playerIndex && task.planets.count(planet.id) > 0) {
          if (planetRobotsNumber > task.planetRobots[planet.id]) {
            planetRobotsNumber -= task.planetRobots[planet.id];
          } else {
            planetRobotsNumber = 0;
          }
        }
      }
      if (planetRobotsNumber > 0) {
        ownGroups.emplace_back(playerIndex, planetRobotsNumber, 0, planet.id, strategyData.currentTick, planet.id,
                               planet.id, std::nullopt);
      }
    }
  }

  rangedGroups.resize(strategyData.planets.maxId);
  for (const auto &planet: strategyData.planets) {
    rangedGroups[planet.id].clear();
    for (uint32_t groupIndex = 0; groupIndex < ownGroups.size(); ++groupIndex) {
      const auto &group = ownGroups[groupIndex];
      rangedGroups[planet.id].emplace(strategyData.planetGraph.getRealDistance(planet.id, group.targetPlanet) +
                                      strategyData.planetGraph.getRealDistance(group.targetPlanet,
                                                                               group.nextPlanet) +
                                      group.nextPlanetArrivalTick - strategyData.currentTick, groupIndex);
    }
  }

  for (const auto &taskPtr: tasks.managerTasks(*this)) {
    DefenceTask &task = *std::dynamic_pointer_cast<DefenceTask>(taskPtr);
    if (!task.finished) {
      task.robotsNumber = 0;
      for (auto planetId: task.planets) {
        task.robotsNumber += task.planetRobots[planetId];
      }
      for (const auto &flyingGroup: task.flyingGroups) {
        task.robotsNumber += flyingGroup.number;
      }
      if (task.robotsNumber == 0) {
        task.finished = true;
      }
    }

    execute(task);
  }

  for (const auto &taskPtr: tasks.managerTasks(*this)) {
    DefenceTask &task = *std::dynamic_pointer_cast<DefenceTask>(taskPtr);
    if (!task.finished) {
      fill(task);
    }
  }

  for (const auto &planet: strategyData.planets) {
    Loggers::logger("std").startBlock(
        "check planet {" + std::to_string(planet.x) + "," + std::to_string(planet.y) + "}");
    bool completed = false;
    for (const auto &group: planet.workerGroups) {
      if (completed) {
        break;
      }
      if (strategyData.team.teamPlayer.count(group.playerIndex) == 0) {
        for (const auto &[protectedPlanetId, _]: strategyData.buildingPlan.buildings) {
          if (completed) {
            break;
          }
          Loggers::logger("std").startBlock(
              "protect planet {" + std::to_string(strategyData.planets[protectedPlanetId].x) + "," +
              std::to_string(strategyData.planets[protectedPlanetId].y) + "}");
          auto opponentNumberOnPlanet = group.number;
          uint32_t opponentArrivalTick =
              strategyData.planetGraph.getRealDistance(protectedPlanetId, planet.id) + strategyData.currentTick;
          Loggers::logger("std") << "opponent group will arrive at" << opponentArrivalTick << "tick";
          for (const auto &[_, groupIndex]: rangedGroups[protectedPlanetId]) {
            const auto &ownGroup = ownGroups[groupIndex];
            if (opponentNumberOnPlanet == 0) {
              break;
            }
            auto groupArrivalTick =
                strategyData.planetGraph.getRealDistance(protectedPlanetId, ownGroup.targetPlanet) +
                strategyData.planetGraph.getRealDistance(ownGroup.targetPlanet,
                                                         ownGroup.nextPlanet) +
                ownGroup.nextPlanetArrivalTick;
            Loggers::logger("std") << "checking if group from" << ownGroup.departurePlanet << "to"
                                   << ownGroup.targetPlanet << "with" << ownGroup.number << "robots can save";
            Loggers::logger("std") << "this group will arrive at" << groupArrivalTick;
            if (groupArrivalTick <= opponentArrivalTick) {
              if (opponentNumberOnPlanet > ownGroup.number) {
                opponentNumberOnPlanet -= ownGroup.number;
              } else {
                opponentNumberOnPlanet = 0;
              }
            }
            Loggers::logger("std") << "remaining opponent robots" << opponentNumberOnPlanet;
          }

          if (opponentNumberOnPlanet > 0) {
            Loggers::logger("std") << "planet can't be saved, there can be" << opponentNumberOnPlanet
                                   << "opponent robots";
            DefenceTask newTask;
            newTask.planetRobots.resize(strategyData.planets.maxId);
            newTask.opponentPlanetId = planet.id;
            newTask.opponentPlayerIndex = group.playerIndex;
            newTask.playerIndex = strategyData.constants.myIndex;
            newTask.finished = false;
            execute(newTask);
            fill(newTask);
            if (!newTask.finished) {
              auto newTaskPtr = std::make_shared<DefenceTask>(newTask);
              tasks.add(newTaskPtr, *this);
              completed = true;
            }
          }
          Loggers::logger("std").endBlock();
        }
      }
    }
    Loggers::logger("std").endBlock();
  }

  for (const auto &group: game.flyingWorkerGroups) {
    bool completed = false;
    if (strategyData.team.teamPlayer.count(group.playerIndex) == 0) {
      for (const auto &[protectedPlanetId, _]: strategyData.buildingPlan.buildings) {
        if (completed) {
          break;
        }
        auto opponentNumberOnPlanet = group.number;
        uint32_t opponentArrivalTick = strategyData.planetGraph.getRealDistance(protectedPlanetId, group.targetPlanet) +
                                       strategyData.planetGraph.getRealDistance(group.targetPlanet, group.nextPlanet) +
                                       group.nextPlanetArrivalTick;
        for (const auto &[_, groupIndex]: rangedGroups[protectedPlanetId]) {
          const auto &ownGroup = ownGroups[groupIndex];
          if (opponentNumberOnPlanet == 0) {
            break;
          }
          auto groupArrivalTick = strategyData.planetGraph.getRealDistance(protectedPlanetId, ownGroup.targetPlanet) +
                                  strategyData.planetGraph.getRealDistance(ownGroup.targetPlanet,
                                                                           ownGroup.nextPlanet) +
                                  ownGroup.nextPlanetArrivalTick;
          if (groupArrivalTick <= opponentArrivalTick) {
            if (opponentNumberOnPlanet > ownGroup.number) {
              opponentNumberOnPlanet -= ownGroup.number;
            } else {
              opponentNumberOnPlanet = 0;
            }
          }
        }

        if (opponentNumberOnPlanet > 0) {
          DefenceTask newTask;
          newTask.planetRobots.resize(strategyData.planets.maxId);
          newTask.opponentFlying = true;
          newTask.opponentFlyingGroup = group;
          newTask.opponentPlayerIndex = group.playerIndex;
          newTask.playerIndex = strategyData.constants.myIndex;
          newTask.finished = false;
          execute(newTask);
          fill(newTask);
          if (!newTask.finished) {
            auto newTaskPtr = std::make_shared<DefenceTask>(newTask);
            tasks.add(newTaskPtr, *this);
            completed = true;
          }
        }
      }
    }
  }

  strategyData.resourcePlanner.considerEmployment = true;
}

void Defence::cancel(std::shared_ptr<Task> taskPtr) {

}

void Defence::fill(DefenceTask &task) {
  model::FlyingWorkerGroup opponentGroup;
  if (task.opponentFlying) {
    opponentGroup = task.opponentFlyingGroup;
  } else {
    opponentGroup = {task.opponentPlayerIndex, opponentRobotsOnPlanet[task.opponentPlanetId], 0, task.opponentPlanetId,
                     strategyData.currentTick, task.opponentPlanetId, task.opponentPlanetId, std::nullopt};
  }

  for (auto &[planetId, defenceRobots]: task.defenceRobotsForPlanet) {
    auto currentPlanet = opponentGroup.nextPlanet;
    while (strategyData.planetGraph.nextPlanet[currentPlanet][planetId] != currentPlanet) {
      if (defenceRobots == 0) {
        break;
      }
      uint32_t opponentArrivalTick =
          strategyData.planetGraph.getRealDistance(currentPlanet, opponentGroup.targetPlanet) +
          strategyData.planetGraph.getRealDistance(opponentGroup.targetPlanet, opponentGroup.nextPlanet) +
          opponentGroup.nextPlanetArrivalTick;
      for (const auto &[_, groupIndex]: rangedGroups[currentPlanet]) {
        if (defenceRobots == 0) {
          break;
        }
        auto &ownGroup = ownGroups[groupIndex];
        if (ownGroup.number > 0) {
          auto groupArrivalTick = strategyData.planetGraph.getRealDistance(currentPlanet, ownGroup.targetPlanet) +
                                  strategyData.planetGraph.getRealDistance(ownGroup.targetPlanet, ownGroup.nextPlanet) +
                                  ownGroup.nextPlanetArrivalTick;
          if (groupArrivalTick <= opponentArrivalTick) {
            uint32_t robotsToSend = 0;
            if (ownGroup.departurePlanet == ownGroup.targetPlanet) {
              if (currentPlanet == ownGroup.targetPlanet) {
                robotsToSend = strategyData.resourcePlanner.takePlayerRobotsFromPlanet(currentPlanet,
                                                                                       ownGroup.playerIndex,
                                                                                       defenceRobots);
                task.planets.emplace(currentPlanet);
                task.planetRobots[currentPlanet] += robotsToSend;
              } else {
                auto group = strategyData.moveActions.add(
                    {ownGroup.targetPlanet, currentPlanet, defenceRobots, std::nullopt}, task.playerIndex);
                robotsToSend = group.number;
                task.liftingGroups.emplace_back(group);
              }
            } else {
              if (defenceRobots > ownGroup.number) {
                robotsToSend = ownGroup.number;
              } else {
                robotsToSend = defenceRobots;
              }
            }

            ownGroup.number -= robotsToSend;
            defenceRobots -= robotsToSend;
          }
        }
      }
      currentPlanet = strategyData.planetGraph.nextPlanet[currentPlanet][planetId];
    }
  }
}

GroupDistance::GroupDistance(uint32_t distance, uint32_t groupIndex) : distance(distance), groupIndex(groupIndex) {}

bool GroupDistance::operator<(const GroupDistance &second) const {
  if (distance == second.distance) {
    return groupIndex < second.groupIndex;
  } else {
    return distance < second.distance;
  }
}
