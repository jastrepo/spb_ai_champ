//
// Created by nikita on 11/25/21.
//
#include "Builds.h"
#include <tasks/Tasks.h>
#include <log/Loggers.h>

Builds::Builds(Tasks &tasks) : TaskManager(tasks) {}

void Builds::execute(BuildTask &buildTask) {
  if (!tasks.finished(buildTask.baseBuildId)) {
    return;
  }

  const auto &buildingResources = buildTask.buildingProperties.buildResources;
  bool hasAllResources = true;
  for (size_t resourceIndex = 0; resourceIndex < ResourcePlanner::resourceTypesNumber; ++resourceIndex) {
    const auto &resource = model::Resource(resourceIndex);

    uint32_t resourceRequired = 0;
    {
      auto resourceIt = buildingResources.find(resource);
      if (resourceIt != buildingResources.end()) {
        resourceRequired = resourceIt->second;
      }
    }

    auto resourceOnPlanet = strategyData.resourcePlanner.takeResourceFromPlanet(buildTask.planetId, resource,
                                                                                resourceRequired);
    if (resourceOnPlanet < resourceRequired) {
      hasAllResources = false;

      auto resourceSent = resourceOnPlanet;
      for (auto taskId: buildTask.logisticsIds) {
        const auto &logisticsTask = tasks.logistics.withId(taskId);
        if (!logisticsTask.finished && logisticsTask.resource.has_value() &&
            logisticsTask.resource.value() == resource) {
          resourceSent += logisticsTask.number;
        }
      }

      if (resourceSent < resourceRequired) {
        for (auto planetId: strategyData.planetGraph.rangedPlanets[buildTask.planetId]) {
          if (resourceSent >= resourceRequired) {
            break;
          }
          for (auto specialty: {model::Specialty::LOGISTICS, model::Specialty::COMBAT,
                                model::Specialty::PRODUCTION}) {
            if (resourceSent >= resourceRequired) {
              break;
            }
            const auto &logisticsTask = tasks.logistics.add(planetId, buildTask.planetId,
                                                            resourceRequired - resourceSent, resource,
                                                            strategyData.team.teamPlayerBySpecialty[specialty]);
            if (logisticsTask != nullptr) {
              buildTask.logisticsIds.emplace_back(logisticsTask->id);
              resourceSent += logisticsTask->number;
            }
          }
        }
      }

      if (resourceSent < resourceRequired) {
        for (auto planetId: strategyData.planetGraph.rangedPlanets[buildTask.planetId]) {
          if (strategyData.buildingPlan.buildings.count(planetId) > 0) {
            if (resourceSent >= resourceRequired) {
              break;
            }
            auto planetRobots = strategyData.resourcePlanner.getTotalRobotsOnPlanet(planetId);
            auto planetResource = std::min(resourceRequired - resourceSent,
                                           strategyData.resourcePlanner.getResourceOnPlanet(planetId, resource));

            for (auto taskId: buildTask.logisticsIds) {
              const auto &logisticsTask = tasks.logistics.withId(taskId);
              if (!logisticsTask.finished && !logisticsTask.resource.has_value() &&
                  logisticsTask.endPlanet == planetId) {
                planetRobots += logisticsTask.number;
              }
            }

            if (planetResource > 0 && planetRobots < planetResource) {
              for (auto secondPlanetId: strategyData.planetGraph.rangedPlanets[planetId]) {
                if (planetRobots >= planetResource) {
                  break;
                }
                if (resourceSent >= resourceRequired) {
                  break;
                }

                for (auto specialty: {model::Specialty::LOGISTICS, model::Specialty::COMBAT,
                                      model::Specialty::PRODUCTION}) {
                  if (planetRobots >= planetResource) {
                    break;
                  }
                  if (resourceSent >= resourceRequired) {
                    break;
                  }
                  auto robotsToSend = std::min(planetResource - planetRobots, resourceRequired - resourceSent);
                  const auto &logisticsTask = tasks.logistics.add(secondPlanetId, planetId, robotsToSend, std::nullopt,
                                                                  strategyData.team.teamPlayerBySpecialty[specialty]);
                  if (logisticsTask != nullptr) {
                    buildTask.logisticsIds.emplace_back(logisticsTask->id);
                    planetRobots += logisticsTask->number;
                    resourceSent += logisticsTask->number;
                  }
                }
              }
            }
          }
        }
      }

      if (resourceSent < resourceRequired) {
        strategyData.resourcePlanner.considerEmployment = false;
        for (auto planetId: strategyData.planetGraph.rangedPlanets[buildTask.planetId]) {
          if (strategyData.buildingPlan.buildings.count(planetId) > 0) {
            if (resourceSent >= resourceRequired) {
              break;
            }
            auto planetRobots = strategyData.resourcePlanner.getTotalRobotsOnPlanet(planetId);
            auto planetResource = std::min(resourceRequired - resourceSent,
                                           strategyData.resourcePlanner.getResourceOnPlanet(planetId, resource));

            for (auto taskId: buildTask.logisticsIds) {
              const auto &logisticsTask = tasks.logistics.withId(taskId);
              if (!logisticsTask.finished && !logisticsTask.resource.has_value() &&
                  logisticsTask.endPlanet == planetId) {
                planetRobots += logisticsTask.number;
              }
            }

            if (planetResource > 0 && planetRobots < planetResource) {
              for (auto secondPlanetId: strategyData.planetGraph.rangedPlanets[planetId]) {
                if (planetRobots >= planetResource) {
                  break;
                }
                if (resourceSent >= resourceRequired) {
                  break;
                }

                for (auto specialty: {model::Specialty::LOGISTICS, model::Specialty::COMBAT,
                                      model::Specialty::PRODUCTION}) {
                  if (planetRobots >= planetResource) {
                    break;
                  }
                  if (resourceSent >= resourceRequired) {
                    break;
                  }
                  auto robotsToSend = std::min(planetResource - planetRobots, resourceRequired - resourceSent);
                  const auto &logisticsTask = tasks.logistics.add(secondPlanetId, planetId, robotsToSend, std::nullopt,
                                                                  strategyData.team.teamPlayerBySpecialty[specialty]);
                  if (logisticsTask != nullptr) {
                    buildTask.logisticsIds.emplace_back(logisticsTask->id);
                    planetRobots += logisticsTask->number;
                    resourceSent += logisticsTask->number;
                  }
                }
              }
            }
          }
        }
        strategyData.resourcePlanner.considerEmployment = true;
      }
    }
  }

  if (hasAllResources) {
    for (size_t resourceIndex = 0; resourceIndex < ResourcePlanner::resourceTypesNumber; ++resourceIndex) {
      const auto &resource = model::Resource(resourceIndex);

      uint32_t resourceRequired = 0;
      {
        auto resourceIt = buildingResources.find(resource);
        if (resourceIt != buildingResources.end()) {
          resourceRequired = resourceIt->second;
        }
      }

      strategyData.resourcePlanner.returnResourceToPlanet(buildTask.planetId, resource, resourceRequired);
    }

    strategyData.buildActions.add({buildTask.planetId, buildTask.buildingType});
  }
}

void Builds::newTick(const model::Game &) {
  for (const auto &taskPtr: tasks.managerTasks(*this)) {
    BuildTask &task = *std::dynamic_pointer_cast<BuildTask>(taskPtr);

    const auto &planet = strategyData.planets[task.planetId];
    if (planet.building.has_value()) {
      const auto &building = planet.building.value();
      if (building.buildingType == task.buildingType) {
        task.started = true;
        if (building.health == task.buildingProperties.maxHealth) {
          task.finished = true;
        }
      }
    }

    if (!task.finished) {
      task.logisticsIds.erase(std::remove_if(task.logisticsIds.begin(), task.logisticsIds.end(), [&](size_t taskId) {
        return tasks.finished(taskId);
      }), task.logisticsIds.end());

      if (task.started) {
        strategyData.resourcePlanner.takeRobotsEvenlyFromPlanet(task.planetId, strategyData.constants.maxBuilders);
      } else {
        Loggers::logger("std").startBlock("update build " + std::to_string(task.id));
        execute(task);
        Loggers::logger("std").endBlock();
      }
    }
  }

}

void Builds::cancel(std::shared_ptr<Task> taskPtr) {
  BuildTask &task = *std::dynamic_pointer_cast<BuildTask>(taskPtr);
  Loggers::logger("std").startBlock("cancel build " + std::to_string(task.id));
  if (!task.finished) {
    strategyData.buildActions.cancel({task.planetId, task.buildingType});
    for (const auto &taskId: task.logisticsIds) {
      tasks.logistics.cancelWithId(taskId);
    }
    task.finished = true;
    // TODO: return robots
  }
  Loggers::logger("std").endBlock();
}

std::shared_ptr<BuildTask> Builds::add(uint32_t planetId, model::BuildingType buildingType) {
  const auto &buildingProperties = strategyData.constants.buildingProperties.at(buildingType);
  for (const auto &taskPtr: tasks.managerTasks(*this)) {
    auto task = std::dynamic_pointer_cast<BuildTask>(taskPtr);
    if (task->planetId == planetId) {
      return task;
    }
  }

  size_t baseBuildId = std::numeric_limits<size_t>::max();
  const auto &baseBuilding = buildingProperties.baseBuilding;
  if (baseBuilding.has_value()) {
    const auto &baseBuildingType = baseBuilding.value();
    const auto &building = strategyData.planets[planetId].building;
    if (!building.has_value() || building.value().buildingType != baseBuildingType) {
      auto baseBuild = add(planetId, baseBuildingType);
      if (baseBuild == nullptr) {
        return nullptr;
      } else {
        baseBuildId = baseBuild->id;
      }
    }
  } else {
    if (strategyData.planets[planetId].building.has_value()) {
      return nullptr;
    }
  }

  BuildTask newBuild(baseBuildId, buildingType, buildingProperties, planetId);

  Loggers::logger("std").startBlock("add build " + std::to_string(tasks.nextId));
  newBuild.finished = false;
  execute(newBuild);
  auto newTaskPtr = std::make_shared<BuildTask>(newBuild);
  tasks.add(newTaskPtr, *this);
  Loggers::logger("std").endBlock();
  return newTaskPtr;
}

BuildTask Builds::withId(size_t id) {
  if (ownTaskWithId(id)) {
    return *std::dynamic_pointer_cast<BuildTask>(tasks.taskById.at(id));
  } else {
    return {};
  }
}