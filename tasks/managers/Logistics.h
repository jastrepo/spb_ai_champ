//
// Created by nikita on 11/23/21.
//
#pragma once

#include <tasks/TaskManager.h>
#include <tasks/data/LogisticsTask.h>

struct Logistics : public TaskManager {
  explicit Logistics(Tasks &tasks);

  ~Logistics() override = default;

  void execute(LogisticsTask &task);

  void newTick(const model::Game &game) override;

  void cancel(std::shared_ptr<Task> taskPtr) override;

  std::shared_ptr<LogisticsTask> add(uint32_t startPlanet, uint32_t targetPlanet, uint32_t workerNumber,
                                     std::optional<model::Resource> takeResource, uint32_t playerIndex);

  LogisticsTask withId(size_t id);
};