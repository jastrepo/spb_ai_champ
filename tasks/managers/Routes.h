//
// Created by nikita on 11/24/21.
//
#pragma once

#include <tasks/TaskManager.h>
#include <tasks/data/RouteTask.h>

struct Routes : public TaskManager {
  explicit Routes(Tasks &tasks);

  ~Routes() override = default;

  void execute(RouteTask &task);

  void countRobotsInFly(RouteTask &task);

  void sendForward(RouteTask &route, uint32_t number, uint32_t playerIndex);

  void sendBackward(RouteTask &route, uint32_t number, uint32_t playerIndex);

  void newTick(const model::Game &game) override;

  void cancel(std::shared_ptr<Task> taskPtr) override;

  std::shared_ptr<RouteTask> add(uint32_t startPlanet, uint32_t targetPlanet, uint32_t workerNumber,
                                 model::Resource resource);

  std::shared_ptr<RouteTask> setNumber(size_t id, uint32_t newNumber);

  static void addLogisticsTask(RouteTask &routeTask, const std::shared_ptr<LogisticsTask> &logisticsTask);

  std::shared_ptr<RouteTask> addLogisticsTask(size_t id, const std::shared_ptr<LogisticsTask> &logisticsTask);

  RouteTask withId(size_t id);
};