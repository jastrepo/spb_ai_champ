//
// Created by nikita on 11/24/21.
//
#include "Routes.h"
#include <tasks/Tasks.h>
#include <log/Loggers.h>

Routes::Routes(Tasks &tasks) : TaskManager(tasks) {}

void Routes::execute(RouteTask &task) {
  countRobotsInFly(task);
  if (task.robotsInFly < task.number) {
    for (auto specialty: {model::Specialty::LOGISTICS, model::Specialty::COMBAT, model::Specialty::PRODUCTION}) {
      sendForward(task, task.number - task.robotsInFly, strategyData.team.teamPlayerBySpecialty[specialty]);
    }
  }
}

void Routes::countRobotsInFly(RouteTask &task) {
  task.robotsInFly = 0;
  for (const auto &logisticsTaskId: task.logisticsTaskIds) {
    const auto &logisticsTask = tasks.logistics.withId(logisticsTaskId);
    if (!logisticsTask.finished) {
      task.robotsInFly += logisticsTask.number;
    }
  }
}

void Routes::sendForward(RouteTask &route, uint32_t number, uint32_t playerIndex) {
  addLogisticsTask(route,
                   tasks.logistics.add(route.startPlanetId, route.endPlanetId, number, route.resource, playerIndex));
}

void Routes::sendBackward(RouteTask &route, uint32_t number, uint32_t playerIndex) {
  addLogisticsTask(route,
                   tasks.logistics.add(route.endPlanetId, route.startPlanetId, number, std::nullopt, playerIndex));
}

RouteTask Routes::withId(size_t id) {
  if (ownTaskWithId(id)) {
    return *std::dynamic_pointer_cast<RouteTask>(tasks.taskById.at(id));
  } else {
    return {};
  }
}

void Routes::newTick(const model::Game &) {
  uint32_t routeTasksNumber = 0;
  for (const auto &taskPtr: tasks.managerTasks(*this)) {
    RouteTask &routeTask = *std::dynamic_pointer_cast<RouteTask>(taskPtr);
    if (!routeTask.finished) {
      ++routeTasksNumber;
    }
  }
  uint32_t maxLogisticsTasksPerRoute =
      routeTasksNumber == 0 ? 0 : strategyData.weights.maxLogisticsForRoutes / routeTasksNumber;

  for (const auto &taskPtr: tasks.managerTasks(*this)) {
    RouteTask &routeTask = *std::dynamic_pointer_cast<RouteTask>(taskPtr);
    if (!routeTask.finished) {
      Loggers::logger("std").startBlock("update route " + std::to_string(routeTask.id));

      countRobotsInFly(routeTask);
      std::unordered_map<uint32_t, uint32_t> robotsToSendForward, robotsToSendBackward;
      uint32_t logisticsTasksNumber = 0;
      auto logisticsTaskIds = routeTask.logisticsTaskIds;
      for (const auto &logisticsTaskId: logisticsTaskIds) {
        const auto &logisticsTask = tasks.logistics.withId(logisticsTaskId);
        if (logisticsTask.finished) {
          if (logisticsTask.endPlanet == routeTask.endPlanetId) {
            robotsToSendBackward[logisticsTask.playerIndex] += logisticsTask.number;
          } else if (logisticsTask.endPlanet == routeTask.startPlanetId) {
            robotsToSendForward[logisticsTask.playerIndex] += logisticsTask.number;
          }
        } else {
          ++logisticsTasksNumber;
        }
      }

      for (auto specialty: {model::Specialty::LOGISTICS, model::Specialty::COMBAT, model::Specialty::PRODUCTION}) {
        //if (routeTask.logisticsTaskIds.size() < maxLogisticsTasksPerRoute) {
          auto playerIndex = strategyData.team.teamPlayerBySpecialty[specialty];
          sendBackward(routeTask, std::min(routeTask.number - routeTask.robotsInFly, robotsToSendBackward[playerIndex]),
                       playerIndex);
          sendForward(routeTask, std::min(routeTask.number - routeTask.robotsInFly, robotsToSendForward[playerIndex]),
                      playerIndex);
        //}
      }
      execute(routeTask);

      routeTask.logisticsTaskIds.erase(
          std::remove_if(routeTask.logisticsTaskIds.begin(), routeTask.logisticsTaskIds.end(), [&](size_t packageId) {
            return tasks.finished(packageId);
          }), routeTask.logisticsTaskIds.end());

      Loggers::logger("std").endBlock();
    }
  }
}

std::shared_ptr<RouteTask> Routes::add(uint32_t startPlanet, uint32_t targetPlanet, uint32_t workerNumber,
                                       model::Resource resource) {
  if (startPlanet == targetPlanet) {
    workerNumber = 0;
  }
  RouteTask newRoute(startPlanet, targetPlanet, workerNumber, resource);
  if (workerNumber > 0) {
    Loggers::logger("std").startBlock("add route " + std::to_string(tasks.nextId));
    newRoute.finished = false;
    execute(newRoute);
    auto newTaskPtr = std::make_shared<RouteTask>(newRoute);
    tasks.add(newTaskPtr, *this);
    Loggers::logger("std").endBlock();
    return newTaskPtr;
  }
  return nullptr;
}

void Routes::cancel(std::shared_ptr<Task> taskPtr) {
  RouteTask &task = *std::dynamic_pointer_cast<RouteTask>(taskPtr);
  Loggers::logger("std").startBlock("cancel route " + std::to_string(task.id));
  if (!task.finished) {
    for (const auto &logisticsTaskId: task.logisticsTaskIds) {
      tasks.logistics.cancelWithId(logisticsTaskId);
    }
    task.finished = true;
  }
  Loggers::logger("std").endBlock();
}

std::shared_ptr<RouteTask> Routes::setNumber(size_t id, uint32_t newNumber) {
  if (ownTaskWithId(id)) {
    auto taskPtr = std::dynamic_pointer_cast<RouteTask>(tasks.taskById.at(id));
    RouteTask &routeTask = *taskPtr;
    Loggers::logger("std").startBlock("change route " + std::to_string(routeTask.id));
    routeTask.number = newNumber;

    auto logisticsTaskIds = routeTask.logisticsTaskIds;
    for (const auto &logisticsTaskId: logisticsTaskIds) {
      countRobotsInFly(routeTask);
      if (routeTask.robotsInFly <= routeTask.number) {
        break;
      }

      const auto &logisticsTask = tasks.logistics.withId(logisticsTaskId);
      if (logisticsTask.lifting && logisticsTask.endPlanet == routeTask.startPlanetId) {
        tasks.logistics.cancelWithId(logisticsTaskId);
      }
    }
    execute(routeTask);
    Loggers::logger("std").endBlock();
    return taskPtr;
  } else {
    return nullptr;
  }
}

std::shared_ptr<RouteTask> Routes::addLogisticsTask(size_t id, const std::shared_ptr<LogisticsTask> &logisticsTask) {
  if (ownTaskWithId(id)) {
    auto taskPtr = std::dynamic_pointer_cast<RouteTask>(tasks.taskById.at(id));
    RouteTask &routeTask = *taskPtr;
    addLogisticsTask(routeTask, logisticsTask);

    return taskPtr;
  } else {
    return nullptr;
  }
}

void Routes::addLogisticsTask(RouteTask &routeTask, const std::shared_ptr<LogisticsTask> &logisticsTask) {
  if (logisticsTask != nullptr && !logisticsTask->finished) {
    routeTask.logisticsTaskIds.emplace_back(logisticsTask->id);
    routeTask.robotsInFly += logisticsTask->number;
  }
}