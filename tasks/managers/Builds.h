//
// Created by nikita on 11/25/21.
//
#pragma once

#include <tasks/TaskManager.h>
#include <tasks/data/BuildTask.h>

struct Builds : public TaskManager {
  explicit Builds(Tasks &tasks);

  ~Builds() override = default;

  void execute(BuildTask &buildTask);

  void newTick(const model::Game &taskId) override;

  void cancel(std::shared_ptr<Task> taskPtr) override;

  std::shared_ptr<BuildTask> add(uint32_t planetId, model::BuildingType buildingType);

  BuildTask withId(size_t id);
};