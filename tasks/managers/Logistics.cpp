//
// Created by nikita on 11/23/21.
//
#include "Logistics.h"
#include <tasks/Tasks.h>
#include <utils/vectorHelper.h>
#include <log/Loggers.h>

Logistics::Logistics(Tasks &tasks) : TaskManager(tasks) {}

void Logistics::execute(LogisticsTask &task) {
  task.finished = true;
  task.lifting = false;
  task.flying = false;

  task.nextPlanet = strategyData.planetGraph.nextPlanet[task.currentPlanet][task.endPlanet];
  if (task.nextPlanet != task.currentPlanet) {
    task.flyingGroup = strategyData.moveActions.add({task.currentPlanet, task.nextPlanet, task.number, task.resource},
                                                    task.playerIndex);

    if (task.flyingGroup.departureTick == Constants::inf) {
      task.number = strategyData.resourcePlanner.takePlayerRobotsFromPlanet(task.currentPlanet, task.playerIndex,
                                                                            task.number);
      task.lifting = false;
    } else {
      task.lifting = true;
    }

    if (task.flyingGroup.number < task.number) {
      task.number = task.flyingGroup.number;
    }
    if (task.number > 0) {
      task.finished = false;
    }
  }
}

void Logistics::newTick(const model::Game &) {
  auto expectedEvents = strategyData.flyingGroups.expected, unexpectedEvents = strategyData.flyingGroups.unexpected;
  auto expectedToFly = expectedEvents.fly, expectedToLift = expectedEvents.lift, expectedToLand = expectedEvents.land,
      unexpectedLostNumber = unexpectedEvents.lostNumber;
  for (const auto &taskPtr: tasks.managerTasks(*this)) {
    LogisticsTask &task = *std::dynamic_pointer_cast<LogisticsTask>(taskPtr);
    {
      auto group = findElementAndRemove(unexpectedLostNumber, task.flyingGroup);
      if (group.has_value()) {
        task.flyingGroup = group.value();
        task.number = task.flyingGroup.number;
      }
    }
    if (!findValueAndRemove(expectedToFly, task.flyingGroup)) {
      if (task.flying) {
        task.flying = false;
        if (findValueAndRemove(expectedToLand, task.flyingGroup)) {
          if (task.nextPlanet == task.endPlanet) {
            task.finished = true;
          } else {
            task.currentPlanet = task.nextPlanet;
            execute(task);
          }
        } else {
          if (!findValue(unexpectedEvents.land, task.flyingGroup)) {
            // TODO: Log "Package lost"
          }
          task.finished = true;
        }
      } else if (task.lifting) {
        task.lifting = false;
        auto groupIt = std::find_if(expectedToLift.begin(), expectedToLift.end(),
                                    FlyingGroups::groupLifted(task.flyingGroup));
        if (groupIt == expectedToLift.end()) {
          task.finished = true;
        } else {
          task.flying = true;
          task.flyingGroup = *groupIt;
          expectedToLift.erase(groupIt);
        }
      } else {
        execute(task);
      }
    }
  }
}

void Logistics::cancel(std::shared_ptr<Task> taskPtr) {
  LogisticsTask &task = *std::dynamic_pointer_cast<LogisticsTask>(taskPtr);
  if (task.lifting && !task.finished) {
    Loggers::logger("std") << "cancel logistics task:" << task.currentPlanet << task.endPlanet << task.number
                           << (task.resource.has_value() ? resourceToString(task.resource.value()) : "");
    strategyData.moveActions.cancel({task.currentPlanet, task.nextPlanet, task.number, task.resource},
                                    task.playerIndex);
    task.finished = true;
  }
}

std::shared_ptr<LogisticsTask> Logistics::add(uint32_t startPlanet, uint32_t targetPlanet, uint32_t workerNumber,
                                              std::optional<model::Resource> takeResource, uint32_t playerIndex) {
  if (startPlanet == targetPlanet) {
    workerNumber = 0;
  }
  LogisticsTask newTask(startPlanet, targetPlanet, workerNumber, playerIndex, takeResource);
  if (workerNumber > 0) {
    execute(newTask);
    if (!newTask.finished) {
      auto newTaskPtr = std::make_shared<LogisticsTask>(newTask);
      Loggers::logger("std") << "add logistics task:" << newTask.currentPlanet << newTask.endPlanet << newTask.number
                             << (newTask.resource.has_value() ? resourceToString(newTask.resource.value()) : "");
      tasks.add(newTaskPtr, *this);
      return newTaskPtr;
    }
  }
  return nullptr;
}

LogisticsTask Logistics::withId(size_t id) {
  if (ownTaskWithId(id)) {
    return *std::dynamic_pointer_cast<LogisticsTask>(tasks.taskById.at(id));
  } else {
    return {};
  }
}