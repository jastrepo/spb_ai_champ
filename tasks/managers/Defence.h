//
// Created by nikita on 11/29/21.
//
#pragma once

#include <tasks/TaskManager.h>
#include <tasks/data/DefenceTask.h>
#include <set>

struct GroupDistance {
  uint32_t distance = 0;
  uint32_t groupIndex = 0;

  GroupDistance() = default;

  GroupDistance(uint32_t distance, uint32_t groupIndex);

  bool operator<(const GroupDistance &second) const;
};

struct Defence : public TaskManager {
  std::vector<model::FlyingWorkerGroup> ownGroups;
  std::vector<std::set<GroupDistance>> rangedGroups;
  std::vector<uint32_t> opponentRobotsOnPlanet;

  explicit Defence(Tasks &tasks);

  ~Defence() override = default;

  void execute(DefenceTask &task);

  void fill(DefenceTask &task);

  void newTick(const model::Game &game) override;

  void cancel(std::shared_ptr<Task> taskPtr) override;
};