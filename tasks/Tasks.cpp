//
// Created by nikita on 11/23/21.
//
#include "Tasks.h"

Tasks::Tasks() : defence(*this), attack(*this), logistics(*this), routes(*this), builds(*this) {
  //managers.emplace_back(defence);
  managers.emplace_back(attack);
  managers.emplace_back(logistics);
  managers.emplace_back(routes);
  managers.emplace_back(builds);
}

void Tasks::add(const std::shared_ptr<Task> &newTask, std::reference_wrapper<TaskManager> manager) {
  if (newTask != nullptr && !newTask->finished) {
    newTask->id = nextId;
    taskById.emplace(nextId, newTask);
    managerById.emplace(nextId, manager);
    ++nextId;
  }
}

void Tasks::cancel(size_t id) {
  if (managerById.count(id) > 0) {
    const auto &task = taskById.at(id);
    if (!task->finished) {
      managerById.at(id).get().cancel(task);
    }
  }
}

void Tasks::removeFinished() {
  std::vector<size_t> finishedId;
  for (const auto &[id, task]: taskById) {
    if (task->finished) {
      finishedId.emplace_back(id);
    }
  }
  for (auto id: finishedId) {
    taskById.erase(id);
    managerById.erase(id);
  }
}

void Tasks::newTick(const model::Game &game) {
  strategyData.newTick(game);

  removeFinished();
  for (const auto &manager: managers) {
    manager.get().newTick(game);
  }
}

void Tasks::finishTurn() {
  strategyData.finishTurn();

  for (const auto &manager: managers) {
    manager.get().finishTurn();
  }
}

Tasks::ManagerTasks Tasks::managerTasks(std::reference_wrapper<TaskManager> manager) {
  return {taskById, managerById, manager};
}

bool Tasks::finished(size_t id) const {
  auto taskIt = taskById.find(id);
  if (taskIt == taskById.end()) {
    return true;
  } else {
    if (taskIt->second == nullptr) {
      return true;
    } else {
      return taskIt->second->finished;
    }
  }
}

std::shared_ptr<Task> Tasks::TaskIterator::operator*() {
  return iterator->second;
}

Tasks::TaskIterator &Tasks::TaskIterator::operator++() {
  if (iterator != end) {
    ++iterator;
    while (iterator != end &&
           (iterator->second->finished || &managerById.at(iterator->first).get() != &manager.get())) {
      ++iterator;
    }
  }
  return *this;
}

bool Tasks::TaskIterator::operator==(const Tasks::TaskIterator &second) const {
  return iterator == second.iterator;
}

bool Tasks::TaskIterator::operator!=(const Tasks::TaskIterator &second) const {
  return iterator != second.iterator;
}

Tasks::TaskIterator::TaskIterator(std::unordered_map<size_t, std::shared_ptr<Task>>::iterator newIterator,
                                  std::unordered_map<size_t, std::shared_ptr<Task>>::iterator newEnd,
                                  const std::unordered_map<size_t, std::reference_wrapper<TaskManager>> &newManagerById,
                                  std::reference_wrapper<TaskManager> newManager)
    : iterator(newIterator), end(newEnd), managerById(newManagerById), manager(newManager) {
  while (iterator != end && (iterator->second->finished || &managerById.at(iterator->first).get() != &manager.get())) {
    ++iterator;
  }
}

Tasks::TaskIterator Tasks::ManagerTasks::begin() {
  return {tasks.begin(), tasks.end(), managerById, manager};
}

Tasks::TaskIterator Tasks::ManagerTasks::end() {
  return {tasks.end(), tasks.end(), managerById, manager};
}

Tasks::ManagerTasks::ManagerTasks(std::unordered_map<size_t, std::shared_ptr<Task>> &newTasks,
                                  const std::unordered_map<size_t, std::reference_wrapper<TaskManager>> &newManagerById,
                                  const std::reference_wrapper<TaskManager> &newManager)
    : tasks(newTasks), managerById(newManagerById), manager(newManager) {}