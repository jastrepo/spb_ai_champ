//
// Created by nikita on 11/23/21.
//
#include "TaskManager.h"
#include "Tasks.h"

TaskManager::TaskManager(Tasks &tasks) : tasks(tasks), strategyData(tasks.strategyData) {}


bool TaskManager::finished(size_t id) {
  if (ownTaskWithId(id)) {
    return tasks.finished(id);
  } else {
    return true;
  }
}

void TaskManager::cancelWithId(size_t id) {
  if (ownTaskWithId(id)) {
    tasks.cancel(id);
  }
}

bool TaskManager::ownTaskWithId(size_t id) {
  return tasks.managerById.count(id) > 0 && &tasks.managerById.at(id).get() == this;
}