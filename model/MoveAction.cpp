#include "MoveAction.hpp"
#include <sstream>

namespace model {
  MoveAction::MoveAction(uint32_t startPlanet, uint32_t targetPlanet, uint32_t workerNumber,
                         std::optional<model::Resource> takeResource)
      : startPlanet(startPlanet), targetPlanet(targetPlanet), workerNumber(workerNumber), takeResource(takeResource) {}

// Read MoveAction from input stream
  MoveAction MoveAction::readFrom(InputStream &stream) {
    uint32_t startPlanet = stream.readUInt32();
    uint32_t targetPlanet = stream.readUInt32();
    uint32_t workerNumber = stream.readUInt32();
    std::optional<model::Resource> takeResource;
    if (stream.readBool()) {
      model::Resource takeResourceValue = readResource(stream);
      takeResource.emplace(takeResourceValue);
    }
    return {startPlanet, targetPlanet, workerNumber, takeResource};
  }

// Write MoveAction to output stream
  void MoveAction::writeTo(OutputStream &stream) const {
    stream.write(startPlanet);
    stream.write(targetPlanet);
    stream.write(workerNumber);
    if (takeResource.has_value()) {
      stream.write(true);
      const model::Resource &takeResourceValue = takeResource.value();
      stream.write(uint32_t(takeResourceValue));
    } else {
      stream.write(false);
    }
  }

// Get string representation of MoveAction
  std::string MoveAction::toString() const {
    std::stringstream ss;
    ss << "MoveAction { ";
    ss << "startPlanet: ";
    ss << startPlanet;
    ss << ", ";
    ss << "targetPlanet: ";
    ss << targetPlanet;
    ss << ", ";
    ss << "workerNumber: ";
    ss << workerNumber;
    ss << ", ";
    ss << "takeResource: ";
    if (takeResource.has_value()) {
      const model::Resource &takeResourceValue = takeResource.value();
      ss << resourceToString(takeResourceValue);
    } else {
      ss << "none";
    }
    ss << " }";
    return ss.str();
  }
}

std::size_t std::hash<model::MoveAction>::operator()(const model::MoveAction &moveAction) const noexcept {
  size_t resourceHash = 0;
  if (moveAction.takeResource.has_value()) {
    resourceHash = size_t(moveAction.takeResource.value()) + 1;
  }

  size_t result = 0;
  result = (result << 8) + size_t(moveAction.startPlanet);
  result = (result << 8) + size_t(moveAction.targetPlanet);
  result = (result << 4) + resourceHash;
  return result;
}

std::size_t std::equal_to<model::MoveAction>::operator()(const model::MoveAction &first,
                                                         const model::MoveAction &second) const noexcept {
  return std::hash<model::MoveAction>()(first) == std::hash<model::MoveAction>()(second);
}