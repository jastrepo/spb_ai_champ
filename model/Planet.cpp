#include "Planet.hpp"
#include <sstream>
#include <strategyData/game/Constants.h>

namespace model {
  Planet::Planet() : id(Constants::inf), x(0), y(0), harvestableResource(), workerGroups(), resources(), building() {}

  Planet::Planet(uint32_t id, int x, int y, std::optional<model::Resource> harvestableResource,
                 std::vector<model::WorkerGroup> workerGroups, std::unordered_map<model::Resource, uint32_t> resources,
                 std::optional<model::Building> building)
      : id(id), x(x), y(y), harvestableResource(harvestableResource), workerGroups(std::move(workerGroups)),
        resources(std::move(resources)), building(building) {}

// Read Planet from input stream
  Planet Planet::readFrom(InputStream &stream) {
    uint32_t id = stream.readUInt32();
    int x = stream.readInt32();
    int y = stream.readInt32();
    std::optional<model::Resource> harvestableResource = std::optional<model::Resource>();
    if (stream.readBool()) {
      model::Resource harvestableResourceValue = readResource(stream);
      harvestableResource.emplace(harvestableResourceValue);
    }
    std::vector<model::WorkerGroup> workerGroups = std::vector<model::WorkerGroup>();
    size_t workerGroupsSize = stream.readUInt32();
    workerGroups.reserve(workerGroupsSize);
    for (size_t workerGroupsIndex = 0; workerGroupsIndex < workerGroupsSize; workerGroupsIndex++) {
      model::WorkerGroup workerGroupsElement = model::WorkerGroup::readFrom(stream);
      workerGroups.emplace_back(workerGroupsElement);
    }
    size_t resourcesSize = stream.readUInt32();
    std::unordered_map<model::Resource, uint32_t> resources;
    resources.reserve(resourcesSize);
    for (size_t resourcesIndex = 0; resourcesIndex < resourcesSize; resourcesIndex++) {
      model::Resource resourcesKey = readResource(stream);
      uint32_t resourcesValue = stream.readUInt32();
      resources.emplace(resourcesKey, resourcesValue);
    }
    std::optional<model::Building> building = std::optional<model::Building>();
    if (stream.readBool()) {
      model::Building buildingValue = model::Building::readFrom(stream);
      building.emplace(buildingValue);
    }
    return {id, x, y, harvestableResource, workerGroups, resources, building};
  }

// Write Planet to output stream
  void Planet::writeTo(OutputStream &stream) const {
    stream.write(id);
    stream.write(x);
    stream.write(y);
    if (harvestableResource.has_value()) {
      stream.write(true);
      const model::Resource &harvestableResourceValue = harvestableResource.value();
      stream.write(uint32_t(harvestableResourceValue));
    } else {
      stream.write(false);
    }
    stream.write(uint32_t(workerGroups.size()));
    for (const model::WorkerGroup &workerGroupsElement: workerGroups) {
      workerGroupsElement.writeTo(stream);
    }
    stream.write(uint32_t(resources.size()));
    for (const auto &[resourcesKey, resourcesValue]: resources) {
      stream.write(uint32_t(resourcesKey));
      stream.write(resourcesValue);
    }
    if (building.has_value()) {
      stream.write(true);
      const model::Building &buildingValue = building.value();
      buildingValue.writeTo(stream);
    } else {
      stream.write(false);
    }
  }

// Get string representation of Planet
  std::string Planet::toString() const {
    std::stringstream ss;
    ss << "Planet { ";
    ss << "id: ";
    ss << id;
    ss << ", ";
    ss << "x: ";
    ss << x;
    ss << ", ";
    ss << "y: ";
    ss << y;
    ss << ", ";
    ss << "harvestableResource: ";
    if (harvestableResource.has_value()) {
      const model::Resource &harvestableResourceValue = harvestableResource.value();
      ss << resourceToString(harvestableResourceValue);
    } else {
      ss << "none";
    }
    ss << ", ";
    ss << "workerGroups: ";
    ss << "[ ";
    for (size_t workerGroupsIndex = 0; workerGroupsIndex < workerGroups.size(); workerGroupsIndex++) {
      const model::WorkerGroup &workerGroupsElement = workerGroups[workerGroupsIndex];
      if (workerGroupsIndex != 0) {
        ss << ", ";
      }
      ss << workerGroupsElement.toString();
    }
    ss << " ]";
    ss << ", ";
    ss << "planetResources: ";
    ss << "{ ";
    size_t resourcesIndex = 0;
    for (const auto &[resourcesKey, resourcesValue]: resources) {
      if (resourcesIndex != 0) {
        ss << ", ";
      }
      ss << resourceToString(resourcesKey);
      ss << ": ";
      ss << resourcesValue;
      resourcesIndex++;
    }
    ss << " }";
    ss << ", ";
    ss << "building: ";
    if (building.has_value()) {
      const model::Building &buildingValue = building.value();
      ss << buildingValue.toString();
    } else {
      ss << "none";
    }
    ss << " }";
    return ss.str();
  }
}