#ifndef __MODEL_BUILDING_HPP__
#define __MODEL_BUILDING_HPP__

#include "BuildingType.hpp"

namespace model {
// A building
  class Building {
  public:
    // Building's type
    model::BuildingType buildingType;
    // Current health
    uint32_t health;
    // Amount of work done for current task
    uint32_t workDone;
    // Number of taskById finished since last tick
    uint32_t lastTickTasksDone;

    Building(model::BuildingType buildingType, uint32_t health, uint32_t workDone, uint32_t lastTickTasksDone);

    // Read Building from input stream
    static Building readFrom(InputStream &stream);

    // Write Building to output stream
    void writeTo(OutputStream &stream) const;

    // Get string representation of Building
    [[nodiscard]] std::string toString() const;

    bool operator==(const Building &other) const;
  };
}

namespace std {
  template<>
  struct hash<model::Building> {
    size_t operator()(const model::Building &value) const;
  };
}
#endif