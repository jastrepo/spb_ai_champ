#ifndef __MODEL_MOVE_ACTION_HPP__
#define __MODEL_MOVE_ACTION_HPP__

#include <optional>
#include "Resource.hpp"

namespace model {
// Movement order
  class MoveAction {
  public:
    // ID of the planet where workers need to be sent from
    uint32_t startPlanet;
    // ID of the target planet
    uint32_t targetPlanet;
    // Number of workers to send
    uint32_t workerNumber;
    // Resource workers should carry
    std::optional<model::Resource> takeResource;

    MoveAction(uint32_t startPlanet, uint32_t targetPlanet, uint32_t workerNumber,
               std::optional<model::Resource> takeResource);

    // Read MoveAction from input stream
    static MoveAction readFrom(InputStream &stream);

    // Write MoveAction to output stream
    void writeTo(OutputStream &stream) const;

    // Get string representation of MoveAction
    [[nodiscard]] std::string toString() const;
  };
}

namespace std {
  template<>
  struct hash<model::MoveAction> {
    std::size_t operator()(const model::MoveAction &moveAction) const noexcept;
  };

  template<>
  struct equal_to<model::MoveAction> {
    std::size_t operator()(const model::MoveAction &first, const model::MoveAction &second) const noexcept;
  };
}
#endif