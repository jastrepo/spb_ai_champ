#ifndef __MODEL_GAME_HPP__
#define __MODEL_GAME_HPP__

#include "Player.hpp"
#include "Planet.hpp"
#include "FlyingWorkerGroup.hpp"
#include "BuildingProperties.hpp"

namespace model {
// Current game's state
  class Game {
  public:
    // Your player's index
    uint32_t myIndex;
    // Current tick number
    uint32_t currentTick;
    // Max number of ticks in the game
    uint32_t maxTickCount;
    // List of players
    std::vector<model::Player> players;
    // List of planets
    std::vector<model::Planet> planets;
    // List of opponentFlying worker groups
    std::vector<model::FlyingWorkerGroup> flyingWorkerGroups;
    // Max number of opponentFlying worker groups for one player
    uint32_t maxFlyingWorkerGroups;
    // Max distance of direct travel between planets
    uint32_t maxTravelDistance;
    // Additional distance of direct travel between planets for player with Logistics specialty
    uint32_t logisticsUpgrade;
    // Additional work done by player with Production specialty (in percent)
    uint32_t productionUpgrade;
    // Additional strength workers for player with Combat specialty (in percent)
    uint32_t combatUpgrade;
    // Max number of workers performing building on one planet
    uint32_t maxBuilders;
    // Properties of every building type
    std::unordered_map<model::BuildingType, model::BuildingProperties> buildingProperties;
    // Whether choosing specialties is allowed
    bool specialtiesAllowed;
    // View distance
    std::optional<uint32_t> viewDistance;

    Game(uint32_t myIndex, uint32_t currentTick, uint32_t maxTickCount, std::vector<model::Player> players,
         std::vector<model::Planet> planets, std::vector<model::FlyingWorkerGroup> flyingWorkerGroups,
         uint32_t maxFlyingWorkerGroups, uint32_t maxTravelDistance, uint32_t logisticsUpgrade,
         uint32_t productionUpgrade, uint32_t combatUpgrade, uint32_t maxBuilders,
         std::unordered_map<model::BuildingType, model::BuildingProperties> buildingProperties, bool specialtiesAllowed,
         std::optional<uint32_t> viewDistance);

    // Read Game from input stream
    static Game readFrom(InputStream &stream);

    // Write Game to output stream
    void writeTo(OutputStream &stream) const;

    // Get string representation of Game
    std::string toString() const;
  };
}
#endif