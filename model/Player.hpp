#ifndef __MODEL_PLAYER_HPP__
#define __MODEL_PLAYER_HPP__

#include <Stream.hpp>
#include <optional>
#include "Specialty.hpp"

namespace model {
// Player (game participant)
  class Player {
  public:
    // Team index
    uint32_t teamIndex;
    // Current score points
    uint32_t score;
    // Player's specialty
    std::optional<model::Specialty> specialty;

    Player(uint32_t teamIndex, uint32_t score, std::optional<model::Specialty> specialty);

    // Read Player from input stream
    static Player readFrom(InputStream &stream);

    // Write Player to output stream
    void writeTo(OutputStream &stream) const;

    // Get string representation of Player
    [[nodiscard]] std::string toString() const;

    bool operator==(const Player &other) const;
  };
}

namespace std {
  template<>
  struct hash<model::Player> {
    size_t operator()(const model::Player &value) const;
  };
}
#endif