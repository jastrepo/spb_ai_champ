#ifndef __MODEL_BUILDING_PROPERTIES_HPP__
#define __MODEL_BUILDING_PROPERTIES_HPP__

#include <unordered_map>
#include <optional>
#include "Resource.hpp"
#include "BuildingType.hpp"

namespace model {
// Building properties
  class BuildingProperties {
  public:
    // Building type that this building can be upgraded from
    std::optional<model::BuildingType> baseBuilding;
    // PlanetResources required for building
    std::unordered_map<model::Resource, uint32_t> buildResources;
    // Max health points of the building
    uint32_t maxHealth;
    // Max number of workers in the building
    uint32_t maxWorkers;
    // PlanetResources required to start another task
    std::unordered_map<model::Resource, uint32_t> workResources;
    // Whether performing a task spawn new workers
    bool produceWorker;
    // Resource produced when performing a task
    std::optional<model::Resource> produceResource;
    // Amount of planetResources/workers produced when performing one task
    uint32_t produceAmount;
    // Score points given for performing one task
    uint32_t produceScore;
    // Whether building is harvesting. In this case resource can only be produced if it is harvestable on the planet
    bool harvest;
    // Amount of work needed to finish one task
    uint32_t workAmount;

    BuildingProperties();

    BuildingProperties(std::optional<model::BuildingType> baseBuilding,
                       std::unordered_map<model::Resource, uint32_t> buildResources, uint32_t maxHealth,
                       uint32_t maxWorkers, std::unordered_map<model::Resource, uint32_t> workResources,
                       bool produceWorker, std::optional<model::Resource> produceResource, uint32_t produceAmount,
                       uint32_t produceScore, bool harvest, uint32_t workAmount);

    // Read BuildingProperties from input stream
    static BuildingProperties readFrom(InputStream &stream);

    // Write BuildingProperties to output stream
    void writeTo(OutputStream &stream) const;

    // Get string representation of BuildingProperties
    std::string toString() const;
  };
}
#endif