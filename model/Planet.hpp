#ifndef __MODEL_PLANET_HPP__
#define __MODEL_PLANET_HPP__

#include <optional>
#include <vector>
#include <unordered_map>
#include "Resource.hpp"
#include "WorkerGroup.hpp"
#include "Building.hpp"

namespace model {
// A planet
  class Planet {
  public:
    // Unique identifier of the planet
    uint32_t id;
    // X coordinate
    int x;
    // Y coordinate
    int y;
    // Resource that can be harvested on the planet
    std::optional<model::Resource> harvestableResource;
    // List of worker groups
    std::vector<model::WorkerGroup> workerGroups;
    // PlanetResources stored on the planet
    std::unordered_map<model::Resource, uint32_t> resources;
    // Building on the planet
    std::optional<model::Building> building;

    Planet();

    Planet(uint32_t id, int x, int y, std::optional<model::Resource> harvestableResource,
           std::vector<model::WorkerGroup> workerGroups, std::unordered_map<model::Resource, uint32_t> resources,
           std::optional<model::Building> building);

    // Read Planet from input stream
    static Planet readFrom(InputStream &stream);

    // Write Planet to output stream
    void writeTo(OutputStream &stream) const;

    // Get string representation of Planet
    std::string toString() const;
  };
}
#endif