#include "BuildingProperties.hpp"
#include <sstream>

namespace model {
  BuildingProperties::BuildingProperties()
      : baseBuilding(std::nullopt), maxHealth(0), maxWorkers(0), produceWorker(false), produceResource(std::nullopt),
        produceAmount(0), produceScore(0), harvest(false), workAmount(0) {}

  BuildingProperties::BuildingProperties(std::optional<model::BuildingType> baseBuilding,
                                         std::unordered_map<model::Resource, uint32_t> buildResources,
                                         uint32_t maxHealth, uint32_t maxWorkers,
                                         std::unordered_map<model::Resource, uint32_t> workResources,
                                         bool produceWorker, std::optional<model::Resource> produceResource,
                                         uint32_t produceAmount, uint32_t produceScore, bool harvest,
                                         uint32_t workAmount)
      : baseBuilding(baseBuilding), buildResources(std::move(buildResources)), maxHealth(maxHealth),
        maxWorkers(maxWorkers), workResources(std::move(workResources)), produceWorker(produceWorker),
        produceResource(produceResource), produceAmount(produceAmount), produceScore(produceScore), harvest(harvest),
        workAmount(workAmount) {}

// Read BuildingProperties from input stream
  BuildingProperties BuildingProperties::readFrom(InputStream &stream) {
    std::optional<model::BuildingType> baseBuilding;
    if (stream.readBool()) {
      model::BuildingType baseBuildingValue = readBuildingType(stream);
      baseBuilding.emplace(baseBuildingValue);
    }
    size_t buildResourcesSize = stream.readUInt32();
    std::unordered_map<model::Resource, uint32_t> buildResources;
    buildResources.reserve(buildResourcesSize);
    for (size_t buildResourcesIndex = 0; buildResourcesIndex < buildResourcesSize; buildResourcesIndex++) {
      model::Resource buildResourcesKey = readResource(stream);
      uint32_t buildResourcesValue = stream.readUInt32();
      buildResources.emplace(buildResourcesKey, buildResourcesValue);
    }
    uint32_t maxHealth = stream.readUInt32();
    uint32_t maxWorkers = stream.readUInt32();
    size_t workResourcesSize = stream.readUInt32();
    std::unordered_map<model::Resource, uint32_t> workResources;
    workResources.reserve(workResourcesSize);
    for (size_t workResourcesIndex = 0; workResourcesIndex < workResourcesSize; workResourcesIndex++) {
      model::Resource workResourcesKey = readResource(stream);
      size_t workResourcesValue = stream.readUInt32();
      workResources.emplace(workResourcesKey, workResourcesValue);
    }
    bool produceWorker = stream.readBool();
    std::optional<model::Resource> produceResource;
    if (stream.readBool()) {
      model::Resource produceResourceValue = readResource(stream);
      produceResource.emplace(produceResourceValue);
    }
    uint32_t produceAmount = stream.readUInt32();
    uint32_t produceScore = stream.readUInt32();
    bool harvest = stream.readBool();
    uint32_t workAmount = stream.readUInt32();
    return {baseBuilding, buildResources, maxHealth, maxWorkers, workResources, produceWorker, produceResource,
            produceAmount, produceScore, harvest, workAmount};
  }

// Write BuildingProperties to output stream
  void BuildingProperties::writeTo(OutputStream &stream) const {
    if (baseBuilding.has_value()) {
      stream.write(true);
      const model::BuildingType &baseBuildingValue = baseBuilding.value();
      stream.write(uint32_t(baseBuildingValue));
    } else {
      stream.write(false);
    }
    stream.write(uint32_t(buildResources.size()));
    for (const auto &[buildResourcesKey, buildResourcesValue]: buildResources) {
      stream.write(uint32_t(buildResourcesKey));
      stream.write(buildResourcesValue);
    }
    stream.write(maxHealth);
    stream.write(maxWorkers);
    stream.write(uint32_t(workResources.size()));
    for (const auto &[workResourcesKey, workResourcesValue]: workResources) {
      stream.write(uint32_t(workResourcesKey));
      stream.write(workResourcesValue);
    }
    stream.write(produceWorker);
    if (produceResource.has_value()) {
      stream.write(true);
      const model::Resource &produceResourceValue = produceResource.value();
      stream.write(uint32_t(produceResourceValue));
    } else {
      stream.write(false);
    }
    stream.write(produceAmount);
    stream.write(produceScore);
    stream.write(harvest);
    stream.write(workAmount);
  }

// Get string representation of BuildingProperties
  std::string BuildingProperties::toString() const {
    std::stringstream ss;
    ss << "BuildingProperties { ";
    ss << "baseBuilding: ";
    if (baseBuilding.has_value()) {
      const model::BuildingType &baseBuildingValue = baseBuilding.value();
      ss << buildingTypeToString(baseBuildingValue);
    } else {
      ss << "none";
    }
    ss << ", ";
    ss << "buildResources: ";
    ss << "{ ";
    size_t buildResourcesIndex = 0;
    for (const auto &[buildResourcesKey, buildResourcesValue]: buildResources) {
      if (buildResourcesIndex != 0) {
        ss << ", ";
      }
      ss << resourceToString(buildResourcesKey);
      ss << ": ";
      ss << buildResourcesValue;
      buildResourcesIndex++;
    }
    ss << " }";
    ss << ", ";
    ss << "maxHealth: ";
    ss << maxHealth;
    ss << ", ";
    ss << "maxWorkers: ";
    ss << maxWorkers;
    ss << ", ";
    ss << "workResources: ";
    ss << "{ ";
    size_t workResourcesIndex = 0;
    for (const auto &[workResourcesKey, workResourcesValue]: workResources) {
      if (workResourcesIndex != 0) {
        ss << ", ";
      }
      ss << resourceToString(workResourcesKey);
      ss << ": ";
      ss << workResourcesValue;
      workResourcesIndex++;
    }
    ss << " }";
    ss << ", ";
    ss << "produceWorker: ";
    ss << produceWorker;
    ss << ", ";
    ss << "produceResource: ";
    if (produceResource.has_value()) {
      const model::Resource &produceResourceValue = produceResource.value();
      ss << resourceToString(produceResourceValue);
    } else {
      ss << "none";
    }
    ss << ", ";
    ss << "produceAmount: ";
    ss << produceAmount;
    ss << ", ";
    ss << "produceScore: ";
    ss << produceScore;
    ss << ", ";
    ss << "harvest: ";
    ss << harvest;
    ss << ", ";
    ss << "workAmount: ";
    ss << workAmount;
    ss << " }";
    return ss.str();
  }
}