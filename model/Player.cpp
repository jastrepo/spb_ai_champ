#include "Player.hpp"
#include <sstream>

namespace model {
  Player::Player(uint32_t teamIndex, uint32_t score, std::optional<model::Specialty> specialty)
      : teamIndex(teamIndex), score(score), specialty(specialty) {}

// Read Player from input stream
  Player Player::readFrom(InputStream &stream) {
    uint32_t teamIndex = stream.readUInt32();
    uint32_t score = stream.readUInt32();
    std::optional<model::Specialty> specialty;
    if (stream.readBool()) {
      model::Specialty specialtyValue = readSpecialty(stream);
      specialty.emplace(specialtyValue);
    }
    return {teamIndex, score, specialty};
  }

// Write Player to output stream
  void Player::writeTo(OutputStream &stream) const {
    stream.write(teamIndex);
    stream.write(score);
    if (specialty.has_value()) {
      stream.write(true);
      const model::Specialty &specialtyValue = specialty.value();
      stream.write(uint32_t(specialtyValue));
    } else {
      stream.write(false);
    }
  }

// Get string representation of Player
  std::string Player::toString() const {
    std::stringstream ss;
    ss << "Player { ";
    ss << "teamIndex: ";
    ss << teamIndex;
    ss << ", ";
    ss << "score: ";
    ss << score;
    ss << ", ";
    ss << "specialty: ";
    if (specialty.has_value()) {
      const model::Specialty &specialtyValue = specialty.value();
      ss << specialtyToString(specialtyValue);
    } else {
      ss << "none";
    }
    ss << " }";
    return ss.str();
  }

  bool Player::operator==(const Player &other) const {
    return score == other.score;
  }
}

size_t std::hash<model::Player>::operator()(const model::Player &value) const {
  size_t result = 0;
  result ^= std::hash<uint32_t>{}(value.score) + 0x9e3779b9 + (result << 6) + (result >> 2);
  return result;
}