#include "FlyingWorkerGroup.hpp"
#include <sstream>

namespace model {
  FlyingWorkerGroup::FlyingWorkerGroup()
      : playerIndex(0), number(0), departureTick(0), departurePlanet(0), nextPlanetArrivalTick(0), nextPlanet(0),
        targetPlanet(0), resource(std::nullopt) {}

  FlyingWorkerGroup::FlyingWorkerGroup(uint32_t playerIndex, uint32_t number, uint32_t departureTick,
                                       uint32_t departurePlanet, uint32_t nextPlanetArrivalTick, uint32_t nextPlanet,
                                       uint32_t targetPlanet, std::optional<model::Resource> resource)
      : playerIndex(playerIndex), number(number), departureTick(departureTick), departurePlanet(departurePlanet),
        nextPlanetArrivalTick(nextPlanetArrivalTick), nextPlanet(nextPlanet), targetPlanet(targetPlanet),
        resource(resource) {}

// Read FlyingWorkerGroup from input stream
  FlyingWorkerGroup FlyingWorkerGroup::readFrom(InputStream &stream) {
    uint32_t playerIndex = stream.readUInt32();
    uint32_t number = stream.readUInt32();
    uint32_t departureTick = stream.readUInt32();
    uint32_t departurePlanet = stream.readUInt32();
    uint32_t nextPlanetArrivalTick = stream.readUInt32();
    uint32_t nextPlanet = stream.readUInt32();
    uint32_t targetPlanet = stream.readUInt32();
    std::optional<model::Resource> resource;
    if (stream.readBool()) {
      model::Resource resourceValue = readResource(stream);
      resource.emplace(resourceValue);
    }
    return {playerIndex, number, departureTick, departurePlanet, nextPlanetArrivalTick, nextPlanet, targetPlanet,
            resource};
  }

// Write FlyingWorkerGroup to output stream
  void FlyingWorkerGroup::writeTo(OutputStream &stream) const {
    stream.write(playerIndex);
    stream.write(number);
    stream.write(departureTick);
    stream.write(departurePlanet);
    stream.write(nextPlanetArrivalTick);
    stream.write(nextPlanet);
    stream.write(targetPlanet);
    if (resource.has_value()) {
      stream.write(true);
      const model::Resource &resourceValue = resource.value();
      stream.write(uint32_t(resourceValue));
    } else {
      stream.write(false);
    }
  }

// Get string representation of FlyingWorkerGroup
  std::string FlyingWorkerGroup::toString() const {
    std::stringstream ss;
    ss << "FlyingWorkerGroup { ";
    ss << "playerIndex: ";
    ss << playerIndex;
    ss << ", ";
    ss << "number: ";
    ss << number;
    ss << ", ";
    ss << "departureTick: ";
    ss << departureTick;
    ss << ", ";
    ss << "departurePlanet: ";
    ss << departurePlanet;
    ss << ", ";
    ss << "nextPlanetArrivalTick: ";
    ss << nextPlanetArrivalTick;
    ss << ", ";
    ss << "nextPlanet: ";
    ss << nextPlanet;
    ss << ", ";
    ss << "targetPlanet: ";
    ss << targetPlanet;
    ss << ", ";
    ss << "resource: ";
    if (resource.has_value()) {
      const model::Resource &resourceValue = resource.value();
      ss << resourceToString(resourceValue);
    } else {
      ss << "none";
    }
    ss << " }";
    return ss.str();
  }
}

std::size_t std::hash<model::FlyingWorkerGroup>::operator()(const model::FlyingWorkerGroup &group) const noexcept {
  size_t resourceHash = 0;
  if (group.resource.has_value()) {
    resourceHash = size_t(group.resource.value()) + 1;
  }
  size_t result = 0;
  result = (result << 3) + size_t(group.playerIndex);
  result = (result << 12) + size_t(group.number);
  result = (result << 10) + size_t(group.departureTick);
  result = (result << 8) + size_t(group.departurePlanet);
  result = (result << 10) + size_t(group.nextPlanetArrivalTick);
  result = (result << 8) + size_t(group.nextPlanet);
  result = (result << 8) + size_t(group.targetPlanet);
  result = (result << 4) + resourceHash;
  return result;
}

bool std::equal_to<model::FlyingWorkerGroup>::operator()(const model::FlyingWorkerGroup &first,
                                                         const model::FlyingWorkerGroup &second) const noexcept {
  return std::hash<model::FlyingWorkerGroup>()(first) == std::hash<model::FlyingWorkerGroup>()(second);
}