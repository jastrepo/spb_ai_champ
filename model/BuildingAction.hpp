#ifndef __MODEL_BUILDING_ACTION_HPP__
#define __MODEL_BUILDING_ACTION_HPP__

#include <optional>
#include "BuildingType.hpp"

namespace model {
// Building order
  class BuildingAction {
  public:
    // ID of the planet where the action needs to be performed
    uint32_t planet;
    // Type of building to build. If absent, current building will be destroyed
    std::optional<model::BuildingType> buildingType;

    BuildingAction(uint32_t planet, std::optional<model::BuildingType> buildingType);

    // Read BuildingAction from input stream
    static BuildingAction readFrom(InputStream &stream);

    // Write BuildingAction to output stream
    void writeTo(OutputStream &stream) const;

    // Get string representation of BuildingAction
    [[nodiscard]] std::string toString() const;
  };
}

namespace std {
  template<>
  struct hash<model::BuildingAction> {
    std::size_t operator()(const model::BuildingAction &buildingAction) const noexcept;
  };

  template<>
  struct equal_to<model::BuildingAction> {
    std::size_t operator()(const model::BuildingAction &first, const model::BuildingAction &second) const noexcept;
  };
}
#endif