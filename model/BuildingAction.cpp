#include "BuildingAction.hpp"
#include <sstream>

namespace model {
  BuildingAction::BuildingAction(uint32_t planet, std::optional<model::BuildingType> buildingType)
      : planet(planet), buildingType(buildingType) {}

// Read BuildingAction from input stream
  BuildingAction BuildingAction::readFrom(InputStream &stream) {
    uint32_t planet = stream.readUInt32();
    std::optional<model::BuildingType> buildingType;
    if (stream.readBool()) {
      model::BuildingType buildingTypeValue = readBuildingType(stream);
      buildingType.emplace(buildingTypeValue);
    }
    return {planet, buildingType};
  }

// Write BuildingAction to output stream
  void BuildingAction::writeTo(OutputStream &stream) const {
    stream.write(planet);
    if (buildingType.has_value()) {
      stream.write(true);
      const model::BuildingType &buildingTypeValue = buildingType.value();
      stream.write(uint32_t(buildingTypeValue));
    } else {
      stream.write(false);
    }
  }

// Get string representation of BuildingAction
  std::string BuildingAction::toString() const {
    std::stringstream ss;
    ss << "BuildingAction { ";
    ss << "planet: ";
    ss << planet;
    ss << ", ";
    ss << "type: ";
    if (buildingType.has_value()) {
      const model::BuildingType &buildingTypeValue = buildingType.value();
      ss << buildingTypeToString(buildingTypeValue);
    } else {
      ss << "none";
    }
    ss << " }";
    return ss.str();
  }
}

std::size_t std::hash<model::BuildingAction>::operator()(const model::BuildingAction &buildingAction) const noexcept {
  size_t buildingTypeHash = 0;
  if (buildingAction.buildingType.has_value()) {
    buildingTypeHash = size_t(buildingAction.buildingType.value()) + 1;
  }

  size_t result = 0;
  result = (result << 8) + size_t(buildingAction.planet);
  result = (result << 4) + buildingTypeHash;
  return result;
}

std::size_t std::equal_to<model::BuildingAction>::operator()(const model::BuildingAction &first,
                                                             const model::BuildingAction &second) const noexcept {
  return std::hash<model::BuildingAction>()(first) == std::hash<model::BuildingAction>()(second);
}