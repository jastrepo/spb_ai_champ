#ifndef __MODEL_WORKER_GROUP_HPP__
#define __MODEL_WORKER_GROUP_HPP__

#include <Stream.hpp>

namespace model {
// Group of workers on a planet
  class WorkerGroup {
  public:
    // Index of player controlling the workers
    uint32_t playerIndex;
    // Number of workers in the group
    uint32_t number;

    WorkerGroup(uint32_t playerIndex, uint32_t number);

    // Read WorkerGroup from input stream
    static WorkerGroup readFrom(InputStream &stream);

    // Write WorkerGroup to output stream
    void writeTo(OutputStream &stream) const;

    // Get string representation of WorkerGroup
    [[nodiscard]] std::string toString() const;

    bool operator==(const WorkerGroup &other) const;
  };
}

namespace std {
  template<>
  struct hash<model::WorkerGroup> {
    size_t operator()(const model::WorkerGroup &value) const;
  };
}
#endif