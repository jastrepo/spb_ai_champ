#include "Game.hpp"
#include <sstream>

namespace model {
  Game::Game(uint32_t myIndex, uint32_t currentTick, uint32_t maxTickCount, std::vector<model::Player> players,
             std::vector<model::Planet> planets, std::vector<model::FlyingWorkerGroup> flyingWorkerGroups,
             uint32_t maxFlyingWorkerGroups, uint32_t maxTravelDistance, uint32_t logisticsUpgrade,
             uint32_t productionUpgrade, uint32_t combatUpgrade, uint32_t maxBuilders,
             std::unordered_map<model::BuildingType, model::BuildingProperties> buildingProperties,
             bool specialtiesAllowed, std::optional<uint32_t> viewDistance)
      : myIndex(myIndex), currentTick(currentTick), maxTickCount(maxTickCount), players(std::move(players)),
        planets(std::move(planets)), flyingWorkerGroups(std::move(flyingWorkerGroups)),
        maxFlyingWorkerGroups(maxFlyingWorkerGroups), maxTravelDistance(maxTravelDistance),
        logisticsUpgrade(logisticsUpgrade), productionUpgrade(productionUpgrade), combatUpgrade(combatUpgrade),
        maxBuilders(maxBuilders), buildingProperties(std::move(buildingProperties)),
        specialtiesAllowed(specialtiesAllowed), viewDistance(viewDistance) {}

// Read Game from input stream
  Game Game::readFrom(InputStream &stream) {
    uint32_t myIndex = stream.readUInt32();
    uint32_t currentTick = stream.readUInt32();
    uint32_t maxTickCount = stream.readUInt32();
    size_t playersSize = stream.readUInt32();
    std::vector<model::Player> players;
    players.reserve(playersSize);
    for (size_t playersIndex = 0; playersIndex < playersSize; playersIndex++) {
      model::Player playersElement = model::Player::readFrom(stream);
      players.emplace_back(playersElement);
    }
    size_t planetsSize = stream.readUInt32();
    std::vector<model::Planet> planets;
    planets.reserve(planetsSize);
    for (size_t planetsIndex = 0; planetsIndex < planetsSize; planetsIndex++) {
      model::Planet planetsElement = model::Planet::readFrom(stream);
      planets.emplace_back(planetsElement);
    }
    size_t flyingWorkerGroupsSize = stream.readUInt32();
    std::vector<model::FlyingWorkerGroup> flyingWorkerGroups;
    flyingWorkerGroups.reserve(flyingWorkerGroupsSize);
    for (size_t flyingWorkerGroupsIndex = 0;
         flyingWorkerGroupsIndex < flyingWorkerGroupsSize; flyingWorkerGroupsIndex++) {
      model::FlyingWorkerGroup flyingWorkerGroupsElement = model::FlyingWorkerGroup::readFrom(stream);
      flyingWorkerGroups.emplace_back(flyingWorkerGroupsElement);
    }
    uint32_t maxFlyingWorkerGroups = stream.readUInt32();
    uint32_t maxTravelDistance = stream.readUInt32();
    uint32_t logisticsUpgrade = stream.readUInt32();
    uint32_t productionUpgrade = stream.readUInt32();
    uint32_t combatUpgrade = stream.readUInt32();
    uint32_t maxBuilders = stream.readUInt32();
    size_t buildingPropertiesSize = stream.readUInt32();
    std::unordered_map<model::BuildingType, model::BuildingProperties> buildingProperties;
    buildingProperties.reserve(buildingPropertiesSize);
    for (size_t buildingPropertiesIndex = 0;
         buildingPropertiesIndex < buildingPropertiesSize; buildingPropertiesIndex++) {
      model::BuildingType buildingPropertiesKey = readBuildingType(stream);
      model::BuildingProperties buildingPropertiesValue = model::BuildingProperties::readFrom(stream);
      buildingProperties.emplace(std::make_pair(buildingPropertiesKey, buildingPropertiesValue));
    }
    bool specialtiesAllowed = stream.readBool();
    std::optional<uint32_t> viewDistance;
    if (stream.readBool()) {
      uint32_t viewDistanceValue = stream.readUInt32();
      viewDistance.emplace(viewDistanceValue);
    }
    return {myIndex, currentTick, maxTickCount, players, planets, flyingWorkerGroups, maxFlyingWorkerGroups,
            maxTravelDistance, logisticsUpgrade, productionUpgrade, combatUpgrade, maxBuilders, buildingProperties,
            specialtiesAllowed, viewDistance};
  }

// Write Game to output stream
  void Game::writeTo(OutputStream &stream) const {
    stream.write(myIndex);
    stream.write(currentTick);
    stream.write(maxTickCount);
    stream.write(uint32_t(players.size()));
    for (const model::Player &playersElement: players) {
      playersElement.writeTo(stream);
    }
    stream.write(uint32_t(planets.size()));
    for (const model::Planet &planetsElement: planets) {
      planetsElement.writeTo(stream);
    }
    stream.write(uint32_t(flyingWorkerGroups.size()));
    for (const model::FlyingWorkerGroup &flyingWorkerGroupsElement: flyingWorkerGroups) {
      flyingWorkerGroupsElement.writeTo(stream);
    }
    stream.write(maxFlyingWorkerGroups);
    stream.write(maxTravelDistance);
    stream.write(logisticsUpgrade);
    stream.write(productionUpgrade);
    stream.write(combatUpgrade);
    stream.write(maxBuilders);
    stream.write(uint32_t(buildingProperties.size()));
    for (const auto &[buildingPropertiesKey, buildingPropertiesValue]: buildingProperties) {
      stream.write(uint32_t(buildingPropertiesKey));
      buildingPropertiesValue.writeTo(stream);
    }
    stream.write(specialtiesAllowed);
    if (viewDistance.has_value()) {
      stream.write(true);
      bool viewDistanceValue = viewDistance.value();
      stream.write(uint32_t(viewDistanceValue));
    } else {
      stream.write(false);
    }
  }

// Get string representation of Game
  std::string Game::toString() const {
    std::stringstream ss;
    ss << "Game { ";
    ss << "myIndex: ";
    ss << myIndex;
    ss << ", ";
    ss << "currentTick: ";
    ss << currentTick;
    ss << ", ";
    ss << "maxTickCount: ";
    ss << maxTickCount;
    ss << ", ";
    ss << "players: ";
    ss << "[ ";
    for (size_t playersIndex = 0; playersIndex < players.size(); playersIndex++) {
      const model::Player &playersElement = players[playersIndex];
      if (playersIndex != 0) {
        ss << ", ";
      }
      ss << playersElement.toString();
    }
    ss << " ]";
    ss << ", ";
    ss << "planets: ";
    ss << "[ ";
    for (size_t planetsIndex = 0; planetsIndex < planets.size(); planetsIndex++) {
      const model::Planet &planetsElement = planets[planetsIndex];
      if (planetsIndex != 0) {
        ss << ", ";
      }
      ss << planetsElement.toString();
    }
    ss << " ]";
    ss << ", ";
    ss << "flyingWorkerGroups: ";
    ss << "[ ";
    for (size_t flyingWorkerGroupsIndex = 0;
         flyingWorkerGroupsIndex < flyingWorkerGroups.size(); flyingWorkerGroupsIndex++) {
      const model::FlyingWorkerGroup &flyingWorkerGroupsElement = flyingWorkerGroups[flyingWorkerGroupsIndex];
      if (flyingWorkerGroupsIndex != 0) {
        ss << ", ";
      }
      ss << flyingWorkerGroupsElement.toString();
    }
    ss << " ]";
    ss << ", ";
    ss << "maxFlyingWorkerGroups: ";
    ss << maxFlyingWorkerGroups;
    ss << ", ";
    ss << "maxTravelDistance: ";
    ss << maxTravelDistance;
    ss << ", ";
    ss << "logisticsUpgrade: ";
    ss << logisticsUpgrade;
    ss << ", ";
    ss << "productionUpgrade: ";
    ss << productionUpgrade;
    ss << ", ";
    ss << "combatUpgrade: ";
    ss << combatUpgrade;
    ss << ", ";
    ss << "maxBuilders: ";
    ss << maxBuilders;
    ss << ", ";
    ss << "properties: ";
    ss << "{ ";
    size_t buildingPropertiesIndex = 0;
    for (const auto &[buildingPropertiesKey, buildingPropertiesValue]: buildingProperties) {
      if (buildingPropertiesIndex != 0) {
        ss << ", ";
      }
      ss << buildingTypeToString(buildingPropertiesKey);
      ss << ": ";
      ss << buildingPropertiesValue.toString();
      buildingPropertiesIndex++;
    }
    ss << " }";
    ss << ", ";
    ss << "specialtiesAllowed: ";
    ss << specialtiesAllowed;
    ss << ", ";
    ss << "viewDistance: ";
    if (viewDistance.has_value()) {
      bool viewDistanceValue = viewDistance.value();
      ss << viewDistanceValue;
    } else {
      ss << "none";
    }
    ss << " }";
    return ss.str();
  }
}