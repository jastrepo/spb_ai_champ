#include "Action.hpp"
#include <sstream>

namespace model {
  Action::Action(std::vector<model::MoveAction> moves, std::vector<model::BuildingAction> buildings,
                 std::optional<model::Specialty> chooseSpecialty)
      : moves(std::move(moves)), buildings(std::move(buildings)), chooseSpecialty(chooseSpecialty) {}

// Read Action from input stream
  Action Action::readFrom(InputStream &stream) {
    std::vector<model::MoveAction> moves;
    size_t movesSize = stream.readUInt32();
    moves.reserve(movesSize);
    for (size_t movesIndex = 0; movesIndex < movesSize; movesIndex++) {
      model::MoveAction movesElement = model::MoveAction::readFrom(stream);
      moves.emplace_back(movesElement);
    }
    std::vector<model::BuildingAction> buildings;
    size_t buildingsSize = stream.readUInt32();
    buildings.reserve(buildingsSize);
    for (size_t buildingsIndex = 0; buildingsIndex < buildingsSize; buildingsIndex++) {
      model::BuildingAction buildingsElement = model::BuildingAction::readFrom(stream);
      buildings.emplace_back(buildingsElement);
    }
    std::optional<model::Specialty> chooseSpecialty;
    if (stream.readBool()) {
      model::Specialty chooseSpecialtyValue = readSpecialty(stream);
      chooseSpecialty.emplace(chooseSpecialtyValue);
    }
    return {moves, buildings, chooseSpecialty};
  }

// Write Action to output stream
  void Action::writeTo(OutputStream &stream) const {
    stream.write(uint32_t(moves.size()));
    for (const model::MoveAction &movesElement: moves) {
      movesElement.writeTo(stream);
    }
    stream.write(uint32_t(buildings.size()));
    for (const model::BuildingAction &buildingsElement: buildings) {
      buildingsElement.writeTo(stream);
    }
    if (chooseSpecialty.has_value()) {
      stream.write(true);
      const model::Specialty &chooseSpecialtyValue = chooseSpecialty.value();
      stream.write(uint32_t(chooseSpecialtyValue));
    } else {
      stream.write(false);
    }
  }

// Get string representation of Action
  std::string Action::toString() const {
    std::stringstream ss;
    ss << "Action { ";
    ss << "moveActions: ";
    ss << "[ ";
    for (size_t movesIndex = 0; movesIndex < moves.size(); movesIndex++) {
      const model::MoveAction &movesElement = moves[movesIndex];
      if (movesIndex != 0) {
        ss << ", ";
      }
      ss << movesElement.toString();
    }
    ss << " ]";
    ss << ", ";
    ss << "buildings: ";
    ss << "[ ";
    for (size_t buildingsIndex = 0; buildingsIndex < buildings.size(); buildingsIndex++) {
      const model::BuildingAction &buildingsElement = buildings[buildingsIndex];
      if (buildingsIndex != 0) {
        ss << ", ";
      }
      ss << buildingsElement.toString();
    }
    ss << " ]";
    ss << ", ";
    ss << "chooseSpecialty: ";
    if (chooseSpecialty.has_value()) {
      const model::Specialty &chooseSpecialtyValue = chooseSpecialty.value();
      ss << specialtyToString(chooseSpecialtyValue);
    } else {
      ss << "none";
    }
    ss << " }";
    return ss.str();
  }
}