#ifndef __MODEL_FLYING_WORKER_GROUP_HPP__
#define __MODEL_FLYING_WORKER_GROUP_HPP__

#include <optional>
#include "Resource.hpp"

namespace model {
// A opponentFlying worker group
  class FlyingWorkerGroup {
  public:
    // Index of player controlling workers
    uint32_t playerIndex;
    // Number of workers in the group
    uint32_t number;
    // Tick when workers left previous planet on their path
    uint32_t departureTick;
    // ID of the previous planet on the path
    uint32_t departurePlanet;
    // Tick when workers will arrive to the next planet in their path
    uint32_t nextPlanetArrivalTick;
    // ID of the next planet in the path
    uint32_t nextPlanet;
    // ID of the target planet
    uint32_t targetPlanet;
    // Resource that workers are carrying
    std::optional<model::Resource> resource;

    FlyingWorkerGroup();

    FlyingWorkerGroup(uint32_t playerIndex, uint32_t number, uint32_t departureTick, uint32_t departurePlanet,
                      uint32_t nextPlanetArrivalTick, uint32_t nextPlanet, uint32_t targetPlanet,
                      std::optional<model::Resource> resource);

    // Read FlyingWorkerGroup from input stream
    static FlyingWorkerGroup readFrom(InputStream &stream);

    // Write FlyingWorkerGroup to output stream
    void writeTo(OutputStream &stream) const;

    // Get string representation of FlyingWorkerGroup
    [[nodiscard]] std::string toString() const;
  };
}

namespace std {
  template<>
  struct hash<model::FlyingWorkerGroup> {
    std::size_t operator()(const model::FlyingWorkerGroup &group) const noexcept;
  };

  template<>
  struct equal_to<model::FlyingWorkerGroup> {
    bool operator()(const model::FlyingWorkerGroup &first, const model::FlyingWorkerGroup &second) const noexcept;
  };
}
#endif