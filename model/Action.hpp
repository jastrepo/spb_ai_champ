#ifndef __MODEL_ACTION_HPP__
#define __MODEL_ACTION_HPP__

#include <vector>
#include "MoveAction.hpp"
#include "BuildingAction.hpp"
#include "Specialty.hpp"

namespace model {
// Player's actions
  class Action {
  public:
    // List of movement orders
    std::vector<model::MoveAction> moves;
    // List of building orders
    std::vector<model::BuildingAction> buildings;
    // Choosing specialty
    std::optional<model::Specialty> chooseSpecialty;

    Action(std::vector<model::MoveAction> moves, std::vector<model::BuildingAction> buildings,
           std::optional<model::Specialty> chooseSpecialty);

    // Read Action from input stream
    static Action readFrom(InputStream &stream);

    // Write Action to output stream
    void writeTo(OutputStream &stream) const;

    // Get string representation of Action
    [[nodiscard]] std::string toString() const;
  };
}
#endif