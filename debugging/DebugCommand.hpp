#ifndef __MODEL_DEBUG_COMMAND_HPP__
#define __MODEL_DEBUG_COMMAND_HPP__

#include <memory>
#include <model/DebugData.hpp>

namespace debugging {
// Debug commands that can be sent while debugging with the app
  class DebugCommand {
  public:
    virtual ~DebugCommand() = default;

    // Add debug data to current tick
    class Add;

    // Clear current tick's debug data
    class Clear;

    // Enable/disable auto performing of commands
    class SetAutoFlush;

    // Perform all previously sent commands
    class Flush;

    // Read DebugCommand from input stream
    static std::shared_ptr<DebugCommand> readFrom(InputStream &stream);

    // Write DebugCommand to output stream
    virtual void writeTo(OutputStream &stream) const = 0;

    // Get string representation of DebugCommand
    [[nodiscard]] virtual std::string toString() const = 0;
  };

// Add debug data to current tick
  class DebugCommand::Add : public DebugCommand {
  public:
    static const int TAG = 0;
    // Data to add
    model::DebugData data;

    explicit Add(model::DebugData data);

    // Read Add from input stream
    static Add readFrom(InputStream &stream);

    // Write Add to output stream
    void writeTo(OutputStream &stream) const override;

    // Get string representation of Add
    [[nodiscard]] std::string toString() const override;

    bool operator==(const Add &other) const;
  };

// Clear current tick's debug data
  class DebugCommand::Clear : public DebugCommand {
  public:
    static const int TAG = 1;

    Clear() = default;

    // Read Clear from input stream
    static Clear readFrom(InputStream &stream);

    // Write Clear to output stream
    void writeTo(OutputStream &stream) const override;

    // Get string representation of Clear
    [[nodiscard]] std::string toString() const override;

    bool operator==(const Clear &other) const;
  };

// Enable/disable auto performing of commands
  class DebugCommand::SetAutoFlush : public DebugCommand {
  public:
    static const int TAG = 2;
    // Enable/disable autoflush
    bool enable;

    explicit SetAutoFlush(bool enable);

    // Read SetAutoFlush from input stream
    static SetAutoFlush readFrom(InputStream &stream);

    // Write SetAutoFlush to output stream
    void writeTo(OutputStream &stream) const override;

    // Get string representation of SetAutoFlush
    [[nodiscard]] std::string toString() const override;

    bool operator==(const SetAutoFlush &other) const;
  };

// Perform all previously sent commands
  class DebugCommand::Flush : public DebugCommand {
  public:
    static const int TAG = 3;

    Flush() = default;

    // Read Flush from input stream
    static Flush readFrom(InputStream &stream);

    // Write Flush to output stream
    void writeTo(OutputStream &stream) const override;

    // Get string representation of Flush
    [[nodiscard]] std::string toString() const override;

    bool operator==(const Flush &other) const;
  };

}

#endif