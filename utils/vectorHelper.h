//
// Created by nikita on 10/30/21.
//

#pragma once

#include <vector>

template<typename T>
bool findValue(const std::vector<T> &collection, const T &value) {
  for (const auto &element: collection) {
    if (std::equal_to<T>()(element, value)) {
      return true;
    }
  }
  return false;
}

template<typename T>
bool findValueAndRemove(std::vector<T> &collection, const T &value) {
  for (auto elementIt = collection.begin(); elementIt != collection.end(); ++elementIt) {
    if (std::equal_to<T>()(*elementIt, value)) {
      collection.erase(elementIt);
      return true;
    }
  }
  return false;
}

template<typename T>
typename std::optional<T> findElementAndRemove(std::vector<T> &collection, const T &value) {
  for (auto elementIt = collection.begin(); elementIt != collection.end(); ++elementIt) {
    if (std::equal_to<T>()(*elementIt, value)) {
      std::optional<T> element(*elementIt);
      collection.erase(elementIt);
      return element;
    }
  }
  return std::nullopt;
}