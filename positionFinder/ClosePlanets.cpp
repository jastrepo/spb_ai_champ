//
// Created by nikita on 11/7/21.
//
#include <queue>
#include "ClosePlanets.h"
#include "StartPosition.h"

ClosePlanets::ClosePlanets(StrategyData &strategyData) : strategyData(strategyData) {}

void ClosePlanets::findTeamPlanets() {
  teamPlanets.clear();
  opponentPlanets.clear();
  for (const auto &planet: strategyData.planets) {
    if (strategyData.resourcePlanner.getTotalRobotsOnPlanet(planet.id) > 0) {
      teamPlanets.emplace_back(planet.id);
    }
    if (strategyData.planets.opponentRobotsAfterCombat[planet.id] > 0) {
      opponentPlanets.emplace_back(planet.id);
    }
  }
  distanceFromTeam.resize(strategyData.planets.maxId, PlanetGraph::infinity);
  for (const auto &planet: strategyData.planets) {
    auto planetId = planet.id;
    uint32_t distance = 0;
    for (auto teamPlanet: teamPlanets) {
      distance += strategyData.planetGraph.getKnownDistance(teamPlanet, planetId);
      if (distance >= PlanetGraph::infinity) {
        break;
      }
    }
    if (distance < PlanetGraph::infinity) {
      distanceFromTeam[planetId] = distance;
    } else {
      distanceFromTeam[planetId] = PlanetGraph::infinity;
    }
  }
}

bool ClosePlanets::findClosePlanetsForTeam() {
  auto requiredPlanetNumber = 9 + teamPlanets.size() + opponentPlanets.size();
  if (strategyData.planets.planetsNumber < requiredPlanetNumber) {
    return false;
  }

  std::unordered_set<uint32_t> usedPlanets;
  usedPlanets.insert(teamPlanets.begin(), teamPlanets.end());
  usedPlanets.insert(opponentPlanets.begin(), opponentPlanets.end());

  closePlanets.clear();
  for (const auto &planet: strategyData.planets) {
    auto planetId = planet.id;

    if (usedPlanets.count(planetId) == 0) {
      bool isClose = false;

      for (auto teamPlanet: teamPlanets) {
        auto distanceToTeamPlanet = strategyData.planetGraph.getKnownDistance(planetId, teamPlanet);

        bool planetCloseToTeam = true;
        for (auto opponentPlanet: opponentPlanets) {
          if (distanceToTeamPlanet >= strategyData.planetGraph.getKnownDistance(planetId, opponentPlanet)) {
            planetCloseToTeam = false;
            break;
          }
        }

        if (planetCloseToTeam) {
          isClose = true;
          break;
        }
      }

      if (isClose) {
        closePlanets.emplace_back(planetId);
        usedPlanets.emplace(planetId);
      }
    }
  }

  closeOre.clear();
  closeSand.clear();
  closeOrganics.clear();
  for (auto planetId: closePlanets) {
    const auto &planetResource = strategyData.planets[planetId].harvestableResource;
    if (planetResource.has_value() && planetResource.value() == model::Resource::ORE) {
      closeOre.emplace_back(planetId);
    }
    if (planetResource.has_value() && planetResource.value() == model::Resource::SAND) {
      closeSand.emplace_back(planetId);
    }
    if (planetResource.has_value() && planetResource.value() == model::Resource::ORGANICS) {
      closeOrganics.emplace_back(planetId);
    }
  }

  if (closeOre.empty() || closeSand.empty() || closeOrganics.empty() || closePlanets.size() < requiredPlanetNumber) {
    std::vector<std::pair<uint32_t, uint32_t>> planetsWithDistances(strategyData.planets.maxId);
    for (const auto &planet: strategyData.planets) {
      auto planetId = planet.id;
      if (usedPlanets.count(planetId) == 0) {
        planetsWithDistances.emplace_back(distanceFromTeam[planetId], planetId);
      }
    }

    std::sort(planetsWithDistances.begin(), planetsWithDistances.end());

    if (closeOre.empty()) {
      for (const auto &[_, planetId]: planetsWithDistances) {
        const auto &planetResource = strategyData.planets[planetId].harvestableResource;
        if (planetResource.has_value() && planetResource.value() == model::Resource::ORE) {
          closeOre.emplace_back(planetId);
          closePlanets.emplace_back(planetId);
          break;
        }
      }
    }
    if (closeSand.empty()) {
      for (const auto &[_, planetId]: planetsWithDistances) {
        const auto &planetResource = strategyData.planets[planetId].harvestableResource;
        if (planetResource.has_value() && planetResource.value() == model::Resource::SAND) {
          closeSand.emplace_back(planetId);
          closePlanets.emplace_back(planetId);
          break;
        }
      }
    }
    if (closeOrganics.empty()) {
      for (const auto &[_, planetId]: planetsWithDistances) {
        const auto &planetResource = strategyData.planets[planetId].harvestableResource;
        if (planetResource.has_value() && planetResource.value() == model::Resource::ORGANICS) {
          closeOrganics.emplace_back(planetId);
          closePlanets.emplace_back(planetId);
          break;
        }
      }
    }
    if (closePlanets.size() < requiredPlanetNumber) {
      for (const auto &[_, planetId]: planetsWithDistances) {
        closePlanets.emplace_back(planetId);
        if (closePlanets.size() >= requiredPlanetNumber) {
          break;
        }
      }
    }

    if (closeOre.empty() || closeSand.empty() || closeOrganics.empty() || closePlanets.size() < requiredPlanetNumber) {
      return false;
    }
  }

  return true;
}

StartPosition ClosePlanets::findBestPosition() {
  StartPosition bestStartPosition;
  bestPosition = CompressedPosition();
  checkedPositions.clear();
  for (auto orePlanet: closeOre) {
    for (auto sandPlanet: closeSand) {
      for (auto organicsPlanet: closeOrganics) {
        std::vector<uint32_t> localToGlobal;
        std::vector<uint8_t> globalToLocal(strategyData.planets.maxId, CompressedPosition::limitIndex);
        for (auto closePlanet: closePlanets) {
          if (closePlanet != orePlanet && closePlanet != sandPlanet && closePlanet != organicsPlanet) {
            globalToLocal[closePlanet] = uint8_t(localToGlobal.size());
            localToGlobal.emplace_back(closePlanet);
          }
        }
        auto maxLocalIndex = uint8_t(localToGlobal.size());

        auto startPosition = findInitialPosition({orePlanet, sandPlanet, organicsPlanet});

        std::vector<uint8_t> distanceMatrix(maxLocalIndex * maxLocalIndex), oreDistances(maxLocalIndex), sandDistances(
            maxLocalIndex), organicsDistances(maxLocalIndex);
        for (uint8_t localPlanetIndex = 0; localPlanetIndex < maxLocalIndex; ++localPlanetIndex) {
          auto globalPlanetId = localToGlobal[localPlanetIndex];
          oreDistances[localPlanetIndex] = uint8_t(
              strategyData.planetGraph.getKnownDistance(globalPlanetId, orePlanet));
          sandDistances[localPlanetIndex] = uint8_t(
              strategyData.planetGraph.getKnownDistance(globalPlanetId, sandPlanet));
          organicsDistances[localPlanetIndex] = uint8_t(
              strategyData.planetGraph.getKnownDistance(globalPlanetId, organicsPlanet));
          for (uint8_t localSecondPlanetIndex = 0; localSecondPlanetIndex < maxLocalIndex; ++localSecondPlanetIndex) {
            auto globalSecondPlanetId = localToGlobal[localSecondPlanetIndex];
            distanceMatrix[size_t(localPlanetIndex * maxLocalIndex + localSecondPlanetIndex)] = uint8_t(
                strategyData.planetGraph.getKnownDistance(globalPlanetId, globalSecondPlanetId));
          }
        }

        std::vector<std::vector<bool>> adjacencyMatrix(maxLocalIndex, std::vector<bool>(maxLocalIndex));
        for (uint8_t localPlanetIndex = 0; localPlanetIndex < maxLocalIndex; ++localPlanetIndex) {
          auto globalPlanetId = localToGlobal[localPlanetIndex];
          for (auto globalSecondPlanetId: strategyData.planetGraph.neighbourPlanets[globalPlanetId]) {
            auto localSecondPlanetIndex = globalToLocal[globalSecondPlanetId];
            if (localSecondPlanetIndex < maxLocalIndex) {
              adjacencyMatrix[localPlanetIndex][localSecondPlanetIndex] = true;
            }
          }
          if (strategyData.planetGraph.getKnownDistance(globalPlanetId, orePlanet) <=
              strategyData.constants.maxTravelDistance) {
            for (auto globalSecondPlanetId: strategyData.planetGraph.neighbourPlanets[orePlanet]) {
              auto localSecondPlanetIndex = globalToLocal[globalSecondPlanetId];
              if (localSecondPlanetIndex < maxLocalIndex) {
                adjacencyMatrix[localPlanetIndex][localSecondPlanetIndex] = true;
              }
            }
          }
          if (strategyData.planetGraph.getKnownDistance(globalPlanetId, sandPlanet) <=
              strategyData.constants.maxTravelDistance) {
            for (auto globalSecondPlanetId: strategyData.planetGraph.neighbourPlanets[sandPlanet]) {
              auto localSecondPlanetIndex = globalToLocal[globalSecondPlanetId];
              if (localSecondPlanetIndex < maxLocalIndex) {
                adjacencyMatrix[localPlanetIndex][localSecondPlanetIndex] = true;
              }
            }
          }
          if (strategyData.planetGraph.getKnownDistance(globalPlanetId, organicsPlanet) <=
              strategyData.constants.maxTravelDistance) {
            for (auto globalSecondPlanetId: strategyData.planetGraph.neighbourPlanets[organicsPlanet]) {
              auto localSecondPlanetIndex = globalToLocal[globalSecondPlanetId];
              if (localSecondPlanetIndex < maxLocalIndex) {
                adjacencyMatrix[localPlanetIndex][localSecondPlanetIndex] = true;
              }
            }
          }
          for (auto teamPlanet: teamPlanets) {
            if (strategyData.planetGraph.getKnownDistance(globalPlanetId, teamPlanet) <=
                strategyData.constants.maxTravelDistance) {
              for (auto globalSecondPlanetId: strategyData.planetGraph.neighbourPlanets[teamPlanet]) {
                auto localSecondPlanetIndex = globalToLocal[globalSecondPlanetId];
                if (localSecondPlanetIndex < maxLocalIndex) {
                  adjacencyMatrix[localPlanetIndex][localSecondPlanetIndex] = true;
                }
              }
            }
          }
        }

        std::vector<std::vector<uint8_t>> adjacencyList(maxLocalIndex);
        for (uint8_t localPlanetIndex = 0; localPlanetIndex < maxLocalIndex; ++localPlanetIndex) {
          for (uint8_t localSecondPlanetIndex = 0; localSecondPlanetIndex < maxLocalIndex; ++localSecondPlanetIndex) {
            if (localPlanetIndex != localSecondPlanetIndex &&
                adjacencyMatrix[localPlanetIndex][localSecondPlanetIndex]) {
              adjacencyList[localPlanetIndex].emplace_back(localSecondPlanetIndex);
            }
          }
        }

        double resourcesWeight =
            distanceFromTeam[orePlanet] + distanceFromTeam[sandPlanet] + distanceFromTeam[organicsPlanet];
        resourcesWeight /= double(teamPlanets.size());
        resourcesWeight *= strategyData.weights.distanceOverRobots;
        if (improveBestPosition(
            CompressedPosition(globalToLocal[startPosition.metalPlanet], globalToLocal[startPosition.siliconPlanet],
                               globalToLocal[startPosition.plasticPlanet], globalToLocal[startPosition.chipPlanet],
                               globalToLocal[startPosition.accumulatorPlanet],
                               globalToLocal[startPosition.replicatorPlanet]), uint16_t(resourcesWeight), maxLocalIndex,
            adjacencyList, oreDistances.data(), sandDistances.data(), organicsDistances.data(),
            distanceMatrix.data())) {
          bestStartPosition.orePlanet = orePlanet;
          bestStartPosition.sandPlanet = sandPlanet;
          bestStartPosition.organicsPlanet = organicsPlanet;
          bestStartPosition.metalPlanet = localToGlobal[bestPosition.metalIndex];
          bestStartPosition.siliconPlanet = localToGlobal[bestPosition.siliconIndex];
          bestStartPosition.plasticPlanet = localToGlobal[bestPosition.plasticIndex];
          bestStartPosition.chipPlanet = localToGlobal[bestPosition.chipIndex];
          bestStartPosition.accumulatorPlanet = localToGlobal[bestPosition.accumulatorIndex];
          bestStartPosition.replicatorPlanet = localToGlobal[bestPosition.replicatorIndex];
        }
      }
    }
  }
  return bestStartPosition;
}

StartPosition ClosePlanets::findInitialPosition(StartPosition position) const {
  uint32_t orePlanet = position.orePlanet, sandPlanet = position.sandPlanet, organicsPlanet = position.organicsPlanet,
      metalPlanet = strategyData.planets.maxId, siliconPlanet = strategyData.planets.maxId,
      plasticPlanet = strategyData.planets.maxId, chipPlanet = strategyData.planets.maxId,
      accumulatorPlanet = strategyData.planets.maxId, replicatorPlanet = strategyData.planets.maxId;
  std::unordered_set<uint32_t> usedPlanets{orePlanet, sandPlanet, organicsPlanet};

  for (auto planetId: closePlanets) {
    if (usedPlanets.count(planetId) == 0) {
      if (metalPlanet == strategyData.planets.maxId ||
          strategyData.planetGraph.getKnownDistance(planetId, orePlanet) <
          strategyData.planetGraph.getKnownDistance(metalPlanet, orePlanet)) {
        metalPlanet = planetId;
      }
    }
  }
  usedPlanets.emplace(metalPlanet);

  for (auto planetId: closePlanets) {
    if (usedPlanets.count(planetId) == 0) {
      if (siliconPlanet == strategyData.planets.maxId ||
          strategyData.planetGraph.getKnownDistance(planetId, sandPlanet) <
          strategyData.planetGraph.getKnownDistance(siliconPlanet, sandPlanet)) {
        siliconPlanet = planetId;
      }
    }
  }
  usedPlanets.emplace(siliconPlanet);

  for (auto planetId: closePlanets) {
    if (usedPlanets.count(planetId) == 0) {
      if (plasticPlanet == strategyData.planets.maxId ||
          strategyData.planetGraph.getKnownDistance(planetId, organicsPlanet) <
          strategyData.planetGraph.getKnownDistance(plasticPlanet, organicsPlanet)) {
        plasticPlanet = planetId;
      }
    }
  }
  usedPlanets.emplace(plasticPlanet);

  for (auto planetId: closePlanets) {
    if (usedPlanets.count(planetId) == 0) {
      if (chipPlanet == strategyData.planets.maxId ||
          strategyData.planetGraph.getKnownDistance(planetId, metalPlanet) <
          strategyData.planetGraph.getKnownDistance(chipPlanet, metalPlanet)) {
        chipPlanet = planetId;
      }
    }
  }
  usedPlanets.emplace(chipPlanet);

  for (auto planetId: closePlanets) {
    if (usedPlanets.count(planetId) == 0) {
      if (accumulatorPlanet == strategyData.planets.maxId ||
          strategyData.planetGraph.getKnownDistance(planetId, metalPlanet) <
          strategyData.planetGraph.getKnownDistance(accumulatorPlanet, metalPlanet)) {
        accumulatorPlanet = planetId;
      }
    }
  }
  usedPlanets.emplace(accumulatorPlanet);

  for (auto planetId: closePlanets) {
    if (usedPlanets.count(planetId) == 0) {
      if (replicatorPlanet == strategyData.planets.maxId ||
          strategyData.planetGraph.getKnownDistance(planetId, metalPlanet) <
          strategyData.planetGraph.getKnownDistance(replicatorPlanet, metalPlanet)) {
        replicatorPlanet = planetId;
      }
    }
  }

  return {orePlanet, sandPlanet, organicsPlanet, metalPlanet, siliconPlanet, plasticPlanet, chipPlanet,
          accumulatorPlanet, replicatorPlanet};
}

bool ClosePlanets::improveBestPosition(CompressedPosition position, uint16_t baseScore,
                                       uint8_t maxIndex, const std::vector<std::vector<uint8_t>> &adjacencyList,
                                       const uint8_t *oreDistances, const uint8_t *sandDistances,
                                       const uint8_t *organicsDistances, const uint8_t *distanceMatrix) {
  std::priority_queue<CompressedPosition> validPositions, invalidPositions;
  auto hashFunction = std::hash<CompressedPosition>();
  checkedPositions.emplace(hashFunction(position));
  position.calculateScore(maxIndex, oreDistances, sandDistances, organicsDistances, distanceMatrix);
  validPositions.emplace(position);
  uint32_t step = 0, improvementStep = 0;
  bool takeValid = true;
  while (checkedPositions.size() < strategyData.weights.maxChecks &&
         step - improvementStep < strategyData.weights.waitStepsForImprovement) {
    if (validPositions.empty()) {
      if (invalidPositions.empty()) {
        break;
      } else {
        position = invalidPositions.top();
        invalidPositions.pop();
      }
    } else {
      if (invalidPositions.empty()) {
        position = validPositions.top();
        validPositions.pop();
      } else {
        if (takeValid) {
          position = validPositions.top();
          validPositions.pop();
        } else {
          position = invalidPositions.top();
          invalidPositions.pop();
        }
        takeValid = !takeValid;
      }
    }
    ++step;

    for (const auto &neighbour: adjacencyList[position.metalIndex]) {
      CompressedPosition newPosition(neighbour, position.siliconIndex, position.plasticIndex, position.chipIndex,
                                     position.accumulatorIndex, position.replicatorIndex);
      if (checkedPositions.emplace(hashFunction(newPosition)).second) {
        newPosition.calculateScore(maxIndex, oreDistances, sandDistances, organicsDistances, distanceMatrix);
        newPosition.score += baseScore;
        if (newPosition.isValid()) {
          validPositions.emplace(newPosition);
          if (newPosition.score < bestPosition.score) {
            improvementStep = step;
            bestPosition = newPosition;
          }
        } else {
          invalidPositions.emplace(newPosition);
        }
      }
    }
    for (const auto &neighbour: adjacencyList[position.siliconIndex]) {
      CompressedPosition newPosition(position.metalIndex, neighbour, position.plasticIndex, position.chipIndex,
                                     position.accumulatorIndex, position.replicatorIndex);
      if (checkedPositions.emplace(hashFunction(newPosition)).second) {
        newPosition.calculateScore(maxIndex, oreDistances, sandDistances, organicsDistances, distanceMatrix);
        newPosition.score += baseScore;
        if (newPosition.isValid()) {
          validPositions.emplace(newPosition);
          if (newPosition.score < bestPosition.score) {
            improvementStep = step;
            bestPosition = newPosition;
          }
        } else {
          invalidPositions.emplace(newPosition);
        }
      }
    }
    for (const auto &neighbour: adjacencyList[position.plasticIndex]) {
      CompressedPosition newPosition(position.metalIndex, position.siliconIndex, neighbour, position.chipIndex,
                                     position.accumulatorIndex, position.replicatorIndex);
      if (checkedPositions.emplace(hashFunction(newPosition)).second) {
        newPosition.calculateScore(maxIndex, oreDistances, sandDistances, organicsDistances, distanceMatrix);
        newPosition.score += baseScore;
        if (newPosition.isValid()) {
          validPositions.emplace(newPosition);
          if (newPosition.score < bestPosition.score) {
            improvementStep = step;
            bestPosition = newPosition;
          }
        } else {
          invalidPositions.emplace(newPosition);
        }
      }
    }
    for (const auto &neighbour: adjacencyList[position.chipIndex]) {
      CompressedPosition newPosition(position.metalIndex, position.siliconIndex, position.plasticIndex, neighbour,
                                     position.accumulatorIndex, position.replicatorIndex);
      if (checkedPositions.emplace(hashFunction(newPosition)).second) {
        newPosition.calculateScore(maxIndex, oreDistances, sandDistances, organicsDistances, distanceMatrix);
        newPosition.score += baseScore;
        if (newPosition.isValid()) {
          validPositions.emplace(newPosition);
          if (newPosition.score < bestPosition.score) {
            improvementStep = step;
            bestPosition = newPosition;
          }
        } else {
          invalidPositions.emplace(newPosition);
        }
      }
    }
    for (const auto &neighbour: adjacencyList[position.accumulatorIndex]) {
      CompressedPosition newPosition(position.metalIndex, position.siliconIndex, position.plasticIndex,
                                     position.chipIndex, neighbour, position.replicatorIndex);
      if (checkedPositions.emplace(hashFunction(newPosition)).second) {
        newPosition.calculateScore(maxIndex, oreDistances, sandDistances, organicsDistances, distanceMatrix);
        newPosition.score += baseScore;
        if (newPosition.isValid()) {
          validPositions.emplace(newPosition);
          if (newPosition.score < bestPosition.score) {
            improvementStep = step;
            bestPosition = newPosition;
          }
        } else {
          invalidPositions.emplace(newPosition);
        }
      }
    }
    for (const auto &neighbour: adjacencyList[position.replicatorIndex]) {
      CompressedPosition newPosition(position.metalIndex, position.siliconIndex, position.plasticIndex,
                                     position.chipIndex, position.accumulatorIndex, neighbour);
      if (checkedPositions.emplace(hashFunction(newPosition)).second) {
        newPosition.calculateScore(maxIndex, oreDistances, sandDistances, organicsDistances, distanceMatrix);
        newPosition.score += baseScore;
        if (newPosition.isValid()) {
          validPositions.emplace(newPosition);
          if (newPosition.score < bestPosition.score) {
            improvementStep = step;
            bestPosition = newPosition;
          }
        } else {
          invalidPositions.emplace(newPosition);
        }
      }
    }
  }

  return improvementStep > 0;
}
