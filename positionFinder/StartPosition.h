//
// Created by nikita on 11/7/21.
//
#pragma once

#include <strategyData/game/Constants.h>

struct CompressedPosition {
  static constexpr uint8_t limitIndex = std::numeric_limits<uint8_t>::max();

  uint8_t metalIndex: 8;
  uint8_t siliconIndex: 8;
  uint8_t plasticIndex: 8;
  uint8_t chipIndex: 8;
  uint8_t accumulatorIndex: 8;
  uint8_t replicatorIndex: 8;
  uint16_t score: 16;

  CompressedPosition();

  CompressedPosition(uint8_t metalIndex, uint8_t siliconIndex, uint8_t plasticIndex, uint8_t chipIndex,
                     uint8_t accumulatorIndex, uint8_t replicatorIndex);

  CompressedPosition(uint8_t metalIndex, uint8_t siliconIndex, uint8_t plasticIndex, uint8_t chipIndex,
                     uint8_t accumulatorIndex, uint8_t replicatorIndex, uint16_t score);

  void calculateScore(uint8_t maxIndex, const uint8_t *oreDistances, const uint8_t *sandDistances,
                      const uint8_t *organicsDistances, const uint8_t *distanceMatrix);

  [[nodiscard]] bool isValid() const;
};

namespace std {
  template<>
  struct hash<CompressedPosition> {
    std::size_t operator()(const CompressedPosition &state) const noexcept;
  };

  template<>
  struct equal_to<CompressedPosition> {
    std::size_t operator()(const CompressedPosition &first, const CompressedPosition &second) const noexcept;
  };

  template<>
  struct less<CompressedPosition> {
    std::size_t operator()(const CompressedPosition &first, const CompressedPosition &second) const noexcept;
  };
}

struct StartPosition {
  uint32_t orePlanet = Constants::inf, sandPlanet = Constants::inf, organicsPlanet = Constants::inf,
      metalPlanet = Constants::inf, siliconPlanet = Constants::inf, plasticPlanet = Constants::inf,
      chipPlanet = Constants::inf, accumulatorPlanet = Constants::inf, replicatorPlanet = Constants::inf;

  StartPosition() = default;

  StartPosition(uint32_t orePlanet, uint32_t sandPlanet, uint32_t organicsPlanet);

  StartPosition(uint32_t orePlanet, uint32_t sandPlanet, uint32_t organicsPlanet, uint32_t metalPlanet,
                uint32_t siliconPlanet, uint32_t plasticPlanet, uint32_t chipPlanet, uint32_t accumulatorPlanet,
                uint32_t replicatorPlanet);
};