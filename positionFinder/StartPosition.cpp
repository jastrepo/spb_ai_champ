//
// Created by nikita on 11/7/21.
//

#include "StartPosition.h"

CompressedPosition::CompressedPosition()
    : metalIndex(limitIndex), siliconIndex(limitIndex), plasticIndex(limitIndex), chipIndex(limitIndex),
      accumulatorIndex(limitIndex), replicatorIndex(limitIndex), score(std::numeric_limits<uint16_t>::max()) {}

CompressedPosition::CompressedPosition(uint8_t metalIndex, uint8_t siliconIndex, uint8_t plasticIndex,
                                       uint8_t chipIndex, uint8_t accumulatorIndex, uint8_t replicatorIndex)
    : metalIndex(metalIndex), siliconIndex(siliconIndex), plasticIndex(plasticIndex), chipIndex(chipIndex),
      accumulatorIndex(accumulatorIndex), replicatorIndex(replicatorIndex),
      score(std::numeric_limits<uint16_t>::max()) {}

CompressedPosition::CompressedPosition(uint8_t metalIndex, uint8_t siliconIndex, uint8_t plasticIndex,
                                       uint8_t chipIndex, uint8_t accumulatorIndex, uint8_t replicatorIndex,
                                       uint16_t score)
    : metalIndex(metalIndex), siliconIndex(siliconIndex), plasticIndex(plasticIndex), chipIndex(chipIndex),
      accumulatorIndex(accumulatorIndex), replicatorIndex(replicatorIndex), score(score) {}

void CompressedPosition::calculateScore(uint8_t maxIndex, const uint8_t *oreDistances, const uint8_t *sandDistances,
                                        const uint8_t *organicsDistances, const uint8_t *distanceMatrix) {
  score = uint16_t(oreDistances[metalIndex] * 16 + distanceMatrix[metalIndex * maxIndex + replicatorIndex] * 2 +
                   distanceMatrix[metalIndex * maxIndex + chipIndex] * 4 +
                   distanceMatrix[metalIndex * maxIndex + accumulatorIndex] * 2 + sandDistances[siliconIndex] * 8 +
                   distanceMatrix[chipIndex * maxIndex + siliconIndex] * 4 + organicsDistances[plasticIndex] * 4 +
                   distanceMatrix[accumulatorIndex * maxIndex + plasticIndex] * 2 +
                   distanceMatrix[accumulatorIndex * maxIndex + replicatorIndex] +
                   distanceMatrix[chipIndex * maxIndex + replicatorIndex] * 2);
}

bool CompressedPosition::isValid() const {
  return metalIndex != siliconIndex && metalIndex != plasticIndex && metalIndex != chipIndex &&
         metalIndex != accumulatorIndex && metalIndex != replicatorIndex && siliconIndex != plasticIndex &&
         siliconIndex != chipIndex && siliconIndex != accumulatorIndex && siliconIndex != replicatorIndex &&
         plasticIndex != chipIndex && plasticIndex != accumulatorIndex && plasticIndex != replicatorIndex &&
         chipIndex != accumulatorIndex && chipIndex != replicatorIndex && accumulatorIndex != replicatorIndex;
}

std::size_t std::hash<CompressedPosition>::operator()(const CompressedPosition &state) const noexcept {
  return *reinterpret_cast<const size_t *>(&state);
}

std::size_t std::equal_to<CompressedPosition>::operator()(const CompressedPosition &first,
                                                          const CompressedPosition &second) const noexcept {
  return std::hash<CompressedPosition>()(first) == std::hash<CompressedPosition>()(second);
}

std::size_t std::less<CompressedPosition>::operator()(const CompressedPosition &first,
                                                      const CompressedPosition &second) const noexcept {
  return std::hash<CompressedPosition>()(first) > std::hash<CompressedPosition>()(second);
}

StartPosition::StartPosition(uint32_t orePlanet, uint32_t sandPlanet, uint32_t organicsPlanet)
    : orePlanet(orePlanet), sandPlanet(sandPlanet), organicsPlanet(organicsPlanet) {}

StartPosition::StartPosition(uint32_t orePlanet, uint32_t sandPlanet, uint32_t organicsPlanet, uint32_t metalPlanet,
                             uint32_t siliconPlanet, uint32_t plasticPlanet, uint32_t chipPlanet,
                             uint32_t accumulatorPlanet, uint32_t replicatorPlanet)
    : orePlanet(orePlanet), sandPlanet(sandPlanet), organicsPlanet(organicsPlanet), metalPlanet(metalPlanet),
      siliconPlanet(siliconPlanet), plasticPlanet(plasticPlanet), chipPlanet(chipPlanet),
      accumulatorPlanet(accumulatorPlanet), replicatorPlanet(replicatorPlanet) {}
