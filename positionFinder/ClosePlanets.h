//
// Created by nikita on 11/7/21.
//
#pragma once

#include <strategyData/StrategyData.h>
#include "StartPosition.h"

struct ClosePlanets {
  StrategyData &strategyData;

  explicit ClosePlanets(StrategyData &strategyData);

  void findTeamPlanets();

  bool findClosePlanetsForTeam();

  StartPosition findBestPosition();

  StartPosition findInitialPosition(StartPosition position) const;

  bool improveBestPosition(CompressedPosition position, uint16_t baseScore, uint8_t maxIndex,
                           const std::vector<std::vector<uint8_t>> &adjacencyList, const uint8_t *oreDistances,
                           const uint8_t *sandDistances, const uint8_t *organicsDistances,
                           const uint8_t *distanceMatrix);

  std::vector<uint32_t> distanceFromTeam;
  CompressedPosition bestPosition;
  std::unordered_set<size_t> checkedPositions;
  std::vector<uint32_t> teamPlanets, opponentPlanets;
  std::vector<uint32_t> closePlanets;
  std::vector<uint32_t> closeOre, closeSand, closeOrganics;
};