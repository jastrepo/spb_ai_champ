//
// Created by nikita on 10/31/21.
//
#include "StrategyData.h"

StrategyData::StrategyData()
    : constants(*this), team(*this), planets(*this), planetGraph(*this), planetDanger(*this), safePlanetGraph(*this),
      resourcePlanner(*this), flyingGroups(*this), moveActions(*this), buildActions(*this), buildingPlan(*this) {}

void StrategyData::newTick(const model::Game &game) {
  currentTick = game.currentTick;
  maxPlayerIndex = std::max(maxPlayerIndex, uint32_t(game.players.size()));
  constants.newTick(game);
  team.newTick(game);
  planets.newTick(game);
  planetGraph.newTick();
  planetDanger.newTick(game);
  safePlanetGraph.newTick(planetDanger.absoluteDangerMatrix());
  resourcePlanner.newTick();
  flyingGroups.newTick(game);
}

void StrategyData::finishTurn() {
  moveActions.finishTurn();
  buildActions.finishTurn();
}
