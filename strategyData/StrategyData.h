//
// Created by nikita on 10/31/21.
//
#pragma once

#include <model/Game.hpp>
#include "game/Constants.h"
#include "game/PlanetGraph.h"
#include "strategyData/strategy/Weights.h"
#include "game/Planets.h"
#include "game/Team.h"
#include "strategy/BuildingPlan.h"
#include "strategy/PlanetDanger.h"
#include "strategy/ResourcePlanner.h"
#include "strategy/FlyingGroups.h"
#include "strategy/MoveActions.h"
#include "strategy/BuildActions.h"

struct StrategyData {
  uint32_t currentTick = 0;
  uint32_t maxPlayerIndex = 0;
  Weights weights;
  Constants constants;
  Team team;
  Planets planets;
  PlanetGraph planetGraph;
  PlanetDanger planetDanger;
  PlanetGraph safePlanetGraph;
  ResourcePlanner resourcePlanner;
  FlyingGroups flyingGroups;
  MoveActions moveActions;
  BuildActions buildActions;
  BuildingPlan buildingPlan;

  StrategyData();

  void newTick(const model::Game &game);

  void finishTurn();
};