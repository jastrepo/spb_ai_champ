//
// Created by nikita on 11/3/21.
//
#pragma once

#include <model/BuildingProperties.hpp>

struct StrategyData;

struct BuildingPlan {
  StrategyData &strategyData;

  explicit BuildingPlan(StrategyData &strategyData);

  std::unordered_map<uint32_t, std::pair<model::BuildingType, model::BuildingProperties>> buildings;

  void addBuilding(uint32_t planetId, model::BuildingType buildingType);
};