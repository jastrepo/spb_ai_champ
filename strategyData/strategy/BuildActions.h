//
// Created by nikita on 10/31/21.
//
#pragma once

#include <model/BuildingAction.hpp>
#include <vector>

struct TickData;
struct StrategyData;

struct BuildActions {
  StrategyData &strategyData;

  explicit BuildActions(StrategyData &strategyData);

  std::vector<model::BuildingAction> builds;

  void finishTurn();

  bool check(model::BuildingAction buildAction);

  bool add(model::BuildingAction buildAction);

  bool add(model::BuildingAction buildAction, uint32_t playerIndex);

  bool add(model::BuildingAction buildAction, uint32_t playerIndex, uint32_t robotsAmount);

  void cancel(model::BuildingAction buildAction);
};