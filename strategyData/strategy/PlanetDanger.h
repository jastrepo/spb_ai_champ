//
// Created by nikita on 11/8/21.
//
#pragma once

#include <limits>
#include <model/Game.hpp>

struct StrategyData;

struct PlanetDanger {
  static constexpr double inf = std::numeric_limits<double>::max();
  StrategyData &strategyData;

  explicit PlanetDanger(StrategyData &strategyData);

  void clear();

  void newTick(const model::Game &game);

  double maxDanger = 0;

  std::vector<double> planets;

  [[nodiscard]] double absolute(uint32_t planetId) const;

  [[nodiscard]] double relative(uint32_t planetId) const;

  [[nodiscard]] std::vector<std::vector<uint32_t>> absoluteDangerMatrix() const;
};