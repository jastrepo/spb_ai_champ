//
// Created by nikita on 10/31/21.
//
#pragma once

#include <vector>
#include <model/Resource.hpp>

struct StrategyData;

struct ResourcePlanner {
  static constexpr size_t resourceTypesNumber = 9;

  StrategyData &strategyData;

  explicit ResourcePlanner(StrategyData &strategyData);

  void newTick();

  [[nodiscard]] uint32_t getTotalRobotsOnPlanet(uint32_t planetId) const;

  [[nodiscard]] uint32_t getPlayerRobotsOnPlanet(uint32_t planetId, uint32_t playerIndex) const;

  [[nodiscard]] uint32_t getResourceOnPlanet(uint32_t planetId, model::Resource resource) const;

  [[nodiscard]] uint32_t getResourceOnPlanet(uint32_t planetId, uint32_t resourceIndex) const;

  [[nodiscard]] uint32_t getTotalRobotsWithResourceOnPlanet(uint32_t planetId, model::Resource resource) const;

  [[nodiscard]] uint32_t getPlayerRobotsWithResourceOnPlanet(uint32_t planetId, uint32_t playerIndex,
                                                             model::Resource resource) const;

  [[nodiscard]] uint32_t getPlayerRobotsWithResourceOnPlanet(uint32_t planetId, uint32_t playerIndex,
                                                             uint32_t resourceIndex) const;

  std::vector<uint32_t> takeRobotsEvenlyFromPlanet(uint32_t planetId, uint32_t amount);

  uint32_t takePlayerRobotsFromPlanet(uint32_t planetId, uint32_t playerIndex, uint32_t amount);

  void returnPlayerRobotsToPlanet(uint32_t planetId, uint32_t playerIndex, uint32_t amount);

  uint32_t takeResourceFromPlanet(uint32_t planetId, model::Resource resource, uint32_t amount);

  void returnResourceToPlanet(uint32_t planetId, model::Resource resource, uint32_t amount);

  std::vector<uint32_t> takeRobotsWithResourceEvenlyFromPlanet(uint32_t planetId, model::Resource resource,
                                                               uint32_t amount);

  uint32_t takePlayerRobotsWithResourceFromPlanet(uint32_t planetId, uint32_t playerIndex, model::Resource resource,
                                                  uint32_t amount);

  [[nodiscard]] std::vector<uint32_t> distributeAmount(uint32_t planetId, uint32_t amount) const;

  void hireRobotsOnPlanet(uint32_t planetId, uint32_t amount);

  void fireRobotsOnPlanet(uint32_t planetId, uint32_t amount);

  bool considerEmployment = true;

private:

  std::vector<std::vector<uint32_t>> resourcesOnPlanet;
  std::vector<uint32_t> totalRobotsOnPlanet;
  std::vector<std::vector<uint32_t>> robotsOnPlanetByPlayer;
  std::vector<uint32_t> employedRobots;
};