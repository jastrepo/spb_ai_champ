//
// Created by nikita on 11/3/21.
//
#include "BuildingPlan.h"
#include <strategyData/StrategyData.h>

BuildingPlan::BuildingPlan(StrategyData &strategyData) : strategyData(strategyData) {}

void BuildingPlan::addBuilding(uint32_t planetId, model::BuildingType buildingType) {
  buildings.emplace(planetId, std::make_pair(buildingType, strategyData.constants.buildingProperties.at(buildingType)));
}