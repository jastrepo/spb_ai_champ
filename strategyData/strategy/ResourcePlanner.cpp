//
// Created by nikita on 10/31/21.
//

#include "ResourcePlanner.h"
#include <strategyData/StrategyData.h>
#include <cmath>
#include <iostream>

using std::cout;
using std::endl;

void ResourcePlanner::newTick() {
  auto maxId = strategyData.planets.maxId;
  totalRobotsOnPlanet.resize(maxId);
  resourcesOnPlanet.resize(maxId, std::vector<uint32_t>(resourceTypesNumber));
  robotsOnPlanetByPlayer.resize(maxId, std::vector<uint32_t>(strategyData.maxPlayerIndex));
  employedRobots.resize(maxId);

  for (const auto &planet: strategyData.planets) {
    auto planetId = planet.id;
    std::fill(robotsOnPlanetByPlayer[planetId].begin(), robotsOnPlanetByPlayer[planetId].end(), 0);

    int robotsNumber = 0;
    for (const auto &group: planet.workerGroups) {
      if (strategyData.team.teamPlayer.count(group.playerIndex) == 0) {
        robotsNumber -= int(group.number);
        robotsOnPlanetByPlayer[planetId][group.playerIndex] = 0;
      } else {
        robotsNumber += int(group.number);
        robotsOnPlanetByPlayer[planetId][group.playerIndex] = group.number;
      }
    }

    if (robotsNumber > 0) {
      const auto &amounts = distributeAmount(planetId, uint32_t(robotsNumber));
      for (uint32_t playerIndex = 0; playerIndex < robotsOnPlanetByPlayer[planetId].size(); ++playerIndex) {
        robotsOnPlanetByPlayer[planetId][playerIndex] = amounts[playerIndex];
      }

      totalRobotsOnPlanet[planetId] = uint32_t(robotsNumber);
    } else {
      std::fill(robotsOnPlanetByPlayer[planetId].begin(), robotsOnPlanetByPlayer[planetId].end(), 0);

      totalRobotsOnPlanet[planetId] = 0;
    }

    for (const auto &[resource, amount]: planet.resources) {
      resourcesOnPlanet[planetId][uint32_t(resource)] = amount;
    }
  }
}

ResourcePlanner::ResourcePlanner(StrategyData &strategyData) : strategyData(strategyData) {}

uint32_t ResourcePlanner::getTotalRobotsOnPlanet(uint32_t planetId) const {
  if (considerEmployment) {
    if (totalRobotsOnPlanet[planetId] > employedRobots[planetId]) {
      return totalRobotsOnPlanet[planetId] - employedRobots[planetId];
    } else {
      return 0;
    }
  } else {
    return totalRobotsOnPlanet[planetId];
  }
}

uint32_t ResourcePlanner::getPlayerRobotsOnPlanet(uint32_t planetId, uint32_t playerIndex) const {
  if (considerEmployment) {
    auto employedRobotsOnPlanet = distributeAmount(planetId, employedRobots[planetId]);
    if (employedRobotsOnPlanet.size() > playerIndex &&
        robotsOnPlanetByPlayer[planetId][playerIndex] > employedRobotsOnPlanet[playerIndex]) {
      return robotsOnPlanetByPlayer[planetId][playerIndex] - employedRobotsOnPlanet[playerIndex];
    } else {
      return 0;
    }
  } else {
    return robotsOnPlanetByPlayer[planetId][playerIndex];
  }
}


uint32_t ResourcePlanner::getResourceOnPlanet(uint32_t planetId, model::Resource resource) const {
  return getResourceOnPlanet(planetId, uint32_t(resource));
}

uint32_t ResourcePlanner::getResourceOnPlanet(uint32_t planetId, uint32_t resourceIndex) const {
  return resourcesOnPlanet[planetId][resourceIndex];
}

uint32_t ResourcePlanner::getTotalRobotsWithResourceOnPlanet(uint32_t planetId, model::Resource resource) const {
  auto robotsAmount = getTotalRobotsOnPlanet(planetId);
  auto resourceAmount = getResourceOnPlanet(planetId, resource);

  if (robotsAmount > resourceAmount) {
    return resourceAmount;
  } else {
    return robotsAmount;
  }
}

uint32_t ResourcePlanner::getPlayerRobotsWithResourceOnPlanet(uint32_t planetId, uint32_t playerIndex,
                                                              model::Resource resource) const {
  return getPlayerRobotsWithResourceOnPlanet(planetId, playerIndex, uint32_t(resource));
}

uint32_t ResourcePlanner::getPlayerRobotsWithResourceOnPlanet(uint32_t planetId, uint32_t playerIndex,
                                                              uint32_t resourceIndex) const {
  auto robotsAmount = getPlayerRobotsOnPlanet(planetId, playerIndex);
  auto resourceAmount = getResourceOnPlanet(planetId, resourceIndex);

  if (robotsAmount > resourceAmount) {
    return resourceAmount;
  } else {
    return robotsAmount;
  }
}

std::vector<uint32_t> ResourcePlanner::takeRobotsEvenlyFromPlanet(uint32_t planetId, uint32_t amount) {
  if (amount == 0) {
    return {};
  }
  auto availableAmount = getTotalRobotsOnPlanet(planetId);
  if (availableAmount == 0) {
    return {};
  }
  if (availableAmount < amount) {
    amount = availableAmount;
  }

  totalRobotsOnPlanet[planetId] -= amount;

  auto &robotsOnPlanet = robotsOnPlanetByPlayer[planetId];
  const auto &amounts = distributeAmount(planetId, amount);
  for (uint32_t playerId = 0; playerId < amounts.size(); ++playerId) {
    robotsOnPlanet[playerId] -= amounts[playerId];
  }

  return amounts;
}

uint32_t ResourcePlanner::takePlayerRobotsFromPlanet(uint32_t planetId, uint32_t playerIndex, uint32_t amount) {
  auto availableAmount = getPlayerRobotsOnPlanet(planetId, playerIndex);
  if (availableAmount < amount) {
    amount = availableAmount;
  }
  totalRobotsOnPlanet[planetId] -= amount;
  robotsOnPlanetByPlayer[planetId][playerIndex] -= amount;
  return amount;
}

void ResourcePlanner::returnPlayerRobotsToPlanet(uint32_t planetId, uint32_t playerIndex, uint32_t amount) {
  totalRobotsOnPlanet[planetId] += amount;
  robotsOnPlanetByPlayer[planetId][playerIndex] += amount;
}

uint32_t ResourcePlanner::takeResourceFromPlanet(uint32_t planetId, model::Resource resource, uint32_t amount) {
  auto availableAmount = getResourceOnPlanet(planetId, resource);
  if (availableAmount < amount) {
    amount = availableAmount;
  }

  resourcesOnPlanet[planetId][uint32_t(resource)] -= amount;
  return amount;
}

void ResourcePlanner::returnResourceToPlanet(uint32_t planetId, model::Resource resource, uint32_t amount) {
  resourcesOnPlanet[planetId][uint32_t(resource)] += amount;
}

std::vector<uint32_t> ResourcePlanner::takeRobotsWithResourceEvenlyFromPlanet(uint32_t planetId,
                                                                              model::Resource resource,
                                                                              uint32_t amount) {
  if (amount == 0) {
    return {};
  }
  auto availableAmount = getTotalRobotsWithResourceOnPlanet(planetId, resource);
  if (availableAmount == 0) {
    return {};
  }
  if (availableAmount < amount) {
    amount = availableAmount;
  }

  resourcesOnPlanet[planetId][uint32_t(resource)] -= amount;
  return takeRobotsEvenlyFromPlanet(planetId, amount);
}

uint32_t ResourcePlanner::takePlayerRobotsWithResourceFromPlanet(uint32_t planetId, uint32_t playerIndex,
                                                                 model::Resource resource, uint32_t amount) {
  auto availableAmount = getPlayerRobotsWithResourceOnPlanet(planetId, playerIndex, resource);
  if (availableAmount < amount) {
    amount = availableAmount;
  }
  totalRobotsOnPlanet[planetId] -= amount;
  robotsOnPlanetByPlayer[planetId][playerIndex] -= amount;
  resourcesOnPlanet[planetId][uint32_t(resource)] -= amount;
  return amount;
}

void ResourcePlanner::hireRobotsOnPlanet(uint32_t planetId, uint32_t amount) {
  employedRobots[planetId] += amount;
}

void ResourcePlanner::fireRobotsOnPlanet(uint32_t planetId, uint32_t amount) {
  if (employedRobots[planetId] > amount) {
    employedRobots[planetId] -= amount;
  } else {
    employedRobots[planetId] = 0;
  }
}

std::vector<uint32_t> ResourcePlanner::distributeAmount(uint32_t planetId, uint32_t amount) const {
  auto &robotsOnPlanet = robotsOnPlanetByPlayer[planetId];
  const auto &players = robotsOnPlanet.size();

  double totalRobots = 0;
  for (uint32_t playerIndex = 0; playerIndex < players; ++playerIndex) {
    totalRobots += robotsOnPlanet[playerIndex];
  }

  if (totalRobots < 1e-5) {
    return {};
  } else {
    double startAmount = amount;
    std::vector<uint32_t> amounts(players);
    for (uint32_t playerIndex = 0; playerIndex < players; ++playerIndex) {
      auto amountForPlayer = uint32_t(round(double(robotsOnPlanet[playerIndex]) / totalRobots * startAmount));
      if (amount >= amountForPlayer) {
        amounts[playerIndex] = amountForPlayer;
        amount -= amountForPlayer;
      } else {
        amounts[playerIndex] = amount;
        amount = 0;
      }
    }
    if (amount > 0) {
      for (uint32_t playerIndex = 0; playerIndex < players; ++playerIndex) {
        if (strategyData.team.teamPlayer.count(playerIndex) > 0) {
          amounts[playerIndex] += amount;
          break;
        }
      }
    }
    /*for (uint32_t playerIndex = 0; playerIndex < players; ++playerIndex) {
      if (strategyData.team.teamPlayer.count(playerIndex) == 0 && amounts[playerIndex] > 0) {
        cout << "aaaaaaaa" << endl;
      }
    }*/
    amount = 0;
    for (auto playerAmount: amounts) {
      amount += playerAmount;
    }
    /*if (std::abs(startAmount - amount) > 1e-5) {
      cout << "aaaaaaaaaaaaaa" << endl;
    }*/

    return amounts;
  }
}
