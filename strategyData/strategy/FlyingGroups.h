//
// Created by nikita on 10/31/21.
//
#pragma once

#include <model/MoveAction.hpp>
#include <functional>
#include <model/Game.hpp>

struct StrategyData;

struct FlyingGroupChanges {
  std::vector<model::FlyingWorkerGroup> land, lift, fly, changePlanet, lostNumber;

  void clear();
};

struct FlyingGroups {
  std::vector<model::FlyingWorkerGroup> logicGroups, newGroups;
  StrategyData &strategyData;

  explicit FlyingGroups(StrategyData &strategyData);

  static std::function<bool(const model::FlyingWorkerGroup &)> groupLifted(
      const model::FlyingWorkerGroup &expectedGroup);

  static std::function<bool(const model::FlyingWorkerGroup &)> groupHasGreatOrEqualNumber(
      const model::FlyingWorkerGroup &expectedGroup);

  FlyingGroupChanges expected, unexpected;

  void findChanges(const model::Game &game);

  void newTick(const model::Game &game);

  model::FlyingWorkerGroup add(const model::MoveAction &move, uint32_t playerIndex);

  bool cancel(const model::MoveAction &move, uint32_t playerIndex);
};