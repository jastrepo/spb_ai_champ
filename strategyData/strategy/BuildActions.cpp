//
// Created by nikita on 10/31/21.
//
#include "BuildActions.h"
#include <strategyData/StrategyData.h>

BuildActions::BuildActions(StrategyData &strategyData) : strategyData(strategyData) {}

void BuildActions::finishTurn() {
  builds.clear();
}

bool BuildActions::check(model::BuildingAction buildAction) {
  if (strategyData.resourcePlanner.getTotalRobotsOnPlanet(buildAction.planet) == 0) {
    return false;
  }

  if (buildAction.buildingType.has_value()) {
    const auto &buildingProperties = strategyData.constants.buildingProperties.at(buildAction.buildingType.value());

    const auto &planet = strategyData.planets[buildAction.planet];
    if (buildingProperties.baseBuilding.has_value()) {
      const auto &baseBuilding = buildingProperties.baseBuilding.value();
      if (!planet.building.has_value()) {
        return false;
      }
      if (planet.building.value().buildingType != baseBuilding) {
        return false;
      }
    } else {
      if (planet.building.has_value()) {
        return false;
      }
    }

    for (const auto &[resource, amount]: buildingProperties.buildResources) {
      if (strategyData.resourcePlanner.getResourceOnPlanet(buildAction.planet, resource) < amount) {
        return false;
      }
    }
  } else {
    const auto &planet = strategyData.planets[buildAction.planet];
    if (!planet.building.has_value()) {
      return false;
    }
  }
  return true;
}

bool BuildActions::add(model::BuildingAction buildAction) {
  if (!check(buildAction)) {
    return false;
  }

  if (buildAction.buildingType.has_value()) {
    const auto &buildingProperties = strategyData.constants.buildingProperties.at(buildAction.buildingType.value());

    for (const auto &[resource, amount]: buildingProperties.buildResources) {
      strategyData.resourcePlanner.takeResourceFromPlanet(buildAction.planet, resource, amount);
    }
  }

  strategyData.resourcePlanner.takeRobotsEvenlyFromPlanet(buildAction.planet, strategyData.constants.maxBuilders);

  builds.emplace_back(buildAction);
  return true;
}

bool BuildActions::add(model::BuildingAction buildAction, uint32_t playerIndex) {
  return add(buildAction, playerIndex, strategyData.constants.maxBuilders);
}

bool BuildActions::add(model::BuildingAction buildAction, uint32_t playerIndex, uint32_t robotsAmount) {
  if (robotsAmount == 0 || !check(buildAction)) {
    return false;
  }

  if (buildAction.buildingType.has_value()) {
    const auto &buildingProperties = strategyData.constants.buildingProperties.at(buildAction.buildingType.value());

    for (const auto &[resource, amount]: buildingProperties.buildResources) {
      strategyData.resourcePlanner.takeResourceFromPlanet(buildAction.planet, resource, amount);
    }
  }

  strategyData.resourcePlanner.takePlayerRobotsFromPlanet(buildAction.planet, playerIndex, robotsAmount);

  builds.emplace_back(buildAction);
  return true;
}

void BuildActions::cancel(model::BuildingAction buildAction) {
  builds.erase(std::remove_if(builds.begin(), builds.end(), [&](const model::BuildingAction &buildingAction) {
    return buildingAction.planet == buildAction.planet && buildingAction.buildingType == buildAction.buildingType;
  }), builds.end());
}
