//
// Created by nikita on 10/31/21.
//
#include "FlyingGroups.h"
#include <strategyData/StrategyData.h>
#include <utils/vectorHelper.h>
#include <log/Loggers.h>

std::function<bool(const model::FlyingWorkerGroup &)> FlyingGroups::groupHasGreatOrEqualNumber(
    const model::FlyingWorkerGroup &expectedGroup) {
  return [&](const model::FlyingWorkerGroup &realGroup) {
    return realGroup.playerIndex == expectedGroup.playerIndex &&
           realGroup.number >= expectedGroup.number &&
           realGroup.departureTick == expectedGroup.departureTick &&
           realGroup.departurePlanet == expectedGroup.departurePlanet &&
           realGroup.nextPlanetArrivalTick == expectedGroup.nextPlanetArrivalTick &&
           realGroup.nextPlanet == expectedGroup.nextPlanet &&
           realGroup.targetPlanet == expectedGroup.targetPlanet &&
           realGroup.resource == expectedGroup.resource;
  };
}

std::function<bool(const model::FlyingWorkerGroup &)> FlyingGroups::groupLifted(
    const model::FlyingWorkerGroup &expectedGroup) {
  return [&](const model::FlyingWorkerGroup &realGroup) {
    return realGroup.playerIndex == expectedGroup.playerIndex &&
           realGroup.departureTick == expectedGroup.departureTick &&
           realGroup.departurePlanet == expectedGroup.departurePlanet &&
           realGroup.targetPlanet == expectedGroup.targetPlanet &&
           realGroup.resource == expectedGroup.resource;
  };
}

void FlyingGroups::findChanges(const model::Game &game) {
  expected.clear();
  unexpected.clear();
  auto groups = game.flyingWorkerGroups;

  for (const auto &logicGroup: logicGroups) {
    auto realGroupIt = std::find_if(groups.begin(), groups.end(), groupHasGreatOrEqualNumber(logicGroup));
    if (realGroupIt == groups.end()) {
      if (logicGroup.nextPlanetArrivalTick == game.currentTick) {
        expected.land.emplace_back(logicGroup);
      } else {
        unexpected.land.emplace_back(logicGroup);
      }
    } else {
      auto &realGroup = *realGroupIt;

      if (logicGroup.nextPlanetArrivalTick == strategyData.currentTick) {
        unexpected.lift.emplace_back(logicGroup);
      } else {
        expected.fly.emplace_back(logicGroup);
      }

      if (realGroup.number > logicGroup.number) {
        realGroup.number -= logicGroup.number;
      } else {
        groups.erase(realGroupIt);
      }
    }
  }

  for (const auto &newGroup: newGroups) {
    auto realGroupIt = std::find_if(groups.begin(), groups.end(), groupLifted(newGroup));
    if (realGroupIt == groups.end()) {
      unexpected.land.emplace_back(newGroup);
    } else {
      auto logicGroup = *realGroupIt;
      if (logicGroup.number > newGroup.number) {
        logicGroup.number = newGroup.number;
        realGroupIt->number -= newGroup.number;
      } else {
        if (logicGroup.number < newGroup.number) {
          unexpected.lostNumber.emplace_back(logicGroup);
        }
        groups.erase(realGroupIt);
      }
      expected.lift.emplace_back(logicGroup);
    }
  }

  for (const auto &realGroup: groups) {
    if (realGroup.departureTick + 1 == strategyData.currentTick) {
      unexpected.lift.emplace_back(realGroup);
    } else {
      unexpected.fly.emplace_back(realGroup);
    }
  }
}

void FlyingGroups::newTick(const model::Game &game) {
  findChanges(game);
  logicGroups.clear();
  newGroups.clear();
  for (const auto &group: expected.fly) {
    logicGroups.emplace_back(group);
  }
  for (const auto &group: expected.lift) {
    logicGroups.emplace_back(group);
  }
  for (const auto &group: unexpected.fly) {
    logicGroups.emplace_back(group);
  }
  for (const auto &group: unexpected.lift) {
    logicGroups.emplace_back(group);
  }
}

model::FlyingWorkerGroup FlyingGroups::add(const model::MoveAction &move, uint32_t playerIndex) {
  model::FlyingWorkerGroup newGroup(playerIndex, move.workerNumber, strategyData.currentTick, move.startPlanet,
                                    Constants::inf, Constants::inf, move.targetPlanet, move.takeResource);
  if (strategyData.team.teamPlayer.count(playerIndex) == 0 || newGroup.departurePlanet == newGroup.targetPlanet) {
    newGroup.number = 0;
  } else {
    if (newGroup.resource.has_value()) {
      newGroup.number = strategyData.resourcePlanner.takePlayerRobotsWithResourceFromPlanet(newGroup.departurePlanet,
                                                                                            playerIndex,
                                                                                            newGroup.resource.value(),
                                                                                            newGroup.number);
    } else {
      newGroup.number = strategyData.resourcePlanner.takePlayerRobotsFromPlanet(newGroup.departurePlanet, playerIndex,
                                                                                newGroup.number);
    }
  }
  if (newGroup.number > 0) {
    newGroups.emplace_back(newGroup);
  } else {
    //Loggers::logger("std") << "rejected";
  }
  return newGroup;
}

FlyingGroups::FlyingGroups(StrategyData &strategyData) : strategyData(strategyData) {}

bool FlyingGroups::cancel(const model::MoveAction &move, uint32_t playerIndex) {
  if (strategyData.team.teamPlayer.count(playerIndex) == 0 || move.startPlanet == move.targetPlanet ||
      move.workerNumber == 0) {
    return false;
  } else {
    model::FlyingWorkerGroup newGroup(playerIndex, move.workerNumber, strategyData.currentTick, move.startPlanet,
                                      Constants::inf, Constants::inf, move.targetPlanet, move.takeResource);
    auto newGroupIt = findElementAndRemove(newGroups, newGroup);
    if (newGroupIt.has_value()) {
      strategyData.resourcePlanner.returnPlayerRobotsToPlanet(newGroup.departurePlanet, newGroup.playerIndex,
                                                              newGroup.number);
      if (newGroup.resource.has_value()) {
        strategyData.resourcePlanner.returnResourceToPlanet(newGroup.departurePlanet, newGroup.resource.value(),
                                                            newGroup.number);
      }
      return true;
    } else {
      return false;
    }
  }
}

void FlyingGroupChanges::clear() {
  land.clear();
  lift.clear();
  fly.clear();
  lostNumber.clear();
}
