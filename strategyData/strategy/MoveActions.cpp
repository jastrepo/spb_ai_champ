//
// Created by nikita on 10/31/21.
//

#include "MoveActions.h"
#include <strategyData/StrategyData.h>

std::vector<model::MoveAction> MoveActions::get() const {
  return {moves.begin(), moves.end()};
}

MoveActions::MoveActions(StrategyData &strategyData) : strategyData(strategyData) {}

model::FlyingWorkerGroup MoveActions::add(model::MoveAction moveAction, uint32_t playerIndex) {
  const auto &moveIt = moves.find(moveAction);
  if (moveIt == moves.end() && moves.size() >= strategyData.constants.maxFlyingWorkerGroups) {
    return {playerIndex, moveAction.workerNumber, Constants::inf, moveAction.startPlanet, Constants::inf,
            Constants::inf, moveAction.targetPlanet, moveAction.takeResource};
  }

  const auto &group = strategyData.flyingGroups.add(moveAction, playerIndex);
  if (group.number > 0 && playerIndex == strategyData.constants.myIndex) {
    moveAction.workerNumber = group.number;
    if (moveIt == moves.end()) {
      moves.insert(moveAction);
    } else {
      auto mergedMove = *moveIt;
      mergedMove.workerNumber += moveAction.workerNumber;
      moves.erase(moveIt);
      moves.emplace(mergedMove);
    }
  }
  return group;
}

void MoveActions::finishTurn() {
  moves.clear();
}

void MoveActions::cancel(model::MoveAction moveAction, uint32_t playerIndex) {
  if (strategyData.flyingGroups.cancel(moveAction, playerIndex)) {
    const auto &moveIt = moves.find(moveAction);
    if (moveIt != moves.end()) {
      if (moveIt->workerNumber > moveAction.workerNumber) {
        auto newMove = *moveIt;
        newMove.workerNumber -= moveAction.workerNumber;
        moves.erase(moveIt);
        moves.emplace(newMove);
      } else {
        moves.erase(moveIt);
      }
    }
  }
}
