//
// Created by nikita on 10/31/21.
//
#pragma once

#include <model/MoveAction.hpp>
#include <model/FlyingWorkerGroup.hpp>
#include <vector>
#include <unordered_set>

struct StrategyData;

struct MoveActions {
  StrategyData &strategyData;

  explicit MoveActions(StrategyData &strategyData);

  std::unordered_set<model::MoveAction> moves;

  [[nodiscard]] std::vector<model::MoveAction> get() const;

  void finishTurn();

  model::FlyingWorkerGroup add(model::MoveAction moveAction, uint32_t playerIndex);

  void cancel(model::MoveAction moveAction, uint32_t playerIndex);
};