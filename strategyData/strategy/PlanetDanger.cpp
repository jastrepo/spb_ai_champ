//
// Created by nikita on 11/8/21.
//

#include "PlanetDanger.h"
#include <strategyData/StrategyData.h>

PlanetDanger::PlanetDanger(StrategyData &strategyData) : strategyData(strategyData) {}

void PlanetDanger::newTick(const model::Game &game) {
  /*maxDanger = 0;
  planets.resize(strategyData.planets.maxId);
  std::fill(planets.begin(), planets.end(), inf);
  for (const auto &planet: strategyData.planets) {
    auto planetId = planet.id;
    planets[planetId] = 0;
    for (const auto &group: planet.workerGroups) {
      double coefficient = 1;
      if (game.players[group.playerIndex].specialty == model::Specialty::COMBAT) {
        coefficient *= 1 + double(game.combatUpgrade) / 100;
      }
      if (strategyData.team.teamPlayer.count(group.playerIndex) == 0) {
        coefficient *= strategyData.weights.dangerCoefficient;
      } else {
        coefficient *= -1;
      }
      planets[planetId] += group.number * coefficient;
    }
    for (const auto &secondPlanet: strategyData.planets) {
      auto secondPlanetId = secondPlanet.id;
      for (const auto &group: secondPlanet.workerGroups) {
        auto distance = double(strategyData.planetGraph.getKnownDistance(planetId, secondPlanetId));
        if (distance > 0) {
          double coefficient = 1;
          if (game.players[group.playerIndex].specialty == model::Specialty::COMBAT) {
            coefficient *= 1 + double(game.combatUpgrade) / 100;
          }
          if (strategyData.team.teamPlayer.count(group.playerIndex) == 0) {
            coefficient *= strategyData.weights.dangerCoefficient;
          } else {
            coefficient *= -1;
          }
          planets[planetId] += group.number * coefficient / distance;
        }
      }
    }
    for (const auto &group: strategyData.flyingGroups.logicGroups) {
      auto distance = double((group.nextPlanetArrivalTick +
                              strategyData.planetGraph.getKnownDistance(group.nextPlanet, group.targetPlanet) +
                              strategyData.planetGraph.getKnownDistance(planetId, group.targetPlanet)) * group.number);
      if (distance > 0) {
        double coefficient = 1;
        if (game.players[group.playerIndex].specialty == model::Specialty::COMBAT) {
          coefficient *= 1 + double(game.combatUpgrade) / 100;
        }
        if (strategyData.team.teamPlayer.count(group.playerIndex) == 0) {
          coefficient *= strategyData.weights.dangerCoefficient;
        } else {
          coefficient *= -1;
        }
        planets[planetId] += group.number * coefficient / distance;
      }
    }
    maxDanger = std::max(maxDanger, planets[planetId]);
    planets[planetId] = std::max(0., planets[planetId]);
  }*/
}

double PlanetDanger::absolute(uint32_t planetId) const {
  if (planetId < planets.size()) {
    if (maxDanger > 0.1) {
      return planets[planetId];
    } else {
      return 0;
    }
  } else {
    return Constants::inf;
  }
}

void PlanetDanger::clear() {
  std::fill(planets.begin(), planets.end(), 0);
}

double PlanetDanger::relative(uint32_t planetId) const {
  if (planetId < planets.size()) {
    if (maxDanger > 0.1) {
      return planets[planetId] / maxDanger;
    } else {
      return 0;
    }
  } else {
    return 1;
  }
}

std::vector<std::vector<uint32_t>> PlanetDanger::absoluteDangerMatrix() const {
  const auto maxId = strategyData.planets.maxId;
  std::vector<std::vector<uint32_t>> dangerMatrix(maxId, std::vector<uint32_t>(maxId));
  for (uint32_t planetId = 0; planetId < maxId; ++planetId) {
    for (uint32_t secondPlanetId = 0; secondPlanetId < maxId; ++secondPlanetId) {
      dangerMatrix[planetId][secondPlanetId] = uint32_t(absolute(planetId) + absolute(secondPlanetId));
    }
  }
  return dangerMatrix;
}
