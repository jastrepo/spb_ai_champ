//
// Created by nikita on 10/30/21.
//
#pragma once
// I have no idea how I will train these values
struct Weights {
  double distanceOverRobots = 0.3; // how much distance add to score of a start position
  uint32_t maxChecks = 600'000; // how much to check start positions
  uint32_t waitStepsForImprovement = 1000; // after which step without changes cancelWithId the improvement
  uint32_t stayOnStonePlanet = 5; // how many robots to keep on start stone planet
  uint32_t attackGroupMinSize = 50; // how many robots at least are in an attack group
  double dangerCoefficient = 2;
  double dangerToPathLength = 20;
  uint32_t maxLogisticsForRoutes = 90; // how much logistics tasks can be executed by route tasks
};