//
// Created by nikita on 11/3/21.
//
#pragma once

#include <model/BuildingProperties.hpp>
#include <limits>
#include <model/Game.hpp>

struct StrategyData;

struct Constants {
  static constexpr uint32_t inf = std::numeric_limits<uint32_t>::max();
  static constexpr int maxCoordinate = 30;
  uint32_t myIndex = inf, teamIndex = inf;
  uint32_t maxBuilders = inf;
  uint32_t maxTravelDistance = inf;
  uint32_t maxFlyingWorkerGroups = inf;
  std::optional<uint32_t> viewDistance;
  bool specialtiesAllowed = false;
  std::unordered_map<model::BuildingType, model::BuildingProperties> buildingProperties;

  StrategyData &strategyData;

  explicit Constants(StrategyData &strategyData);

  void newTick(const model::Game &game);
};