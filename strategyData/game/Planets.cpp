//
// Created by nikita on 10/31/21.
//
#include "Planets.h"
#include <strategyData/StrategyData.h>

Planets::Planets(StrategyData &strategyData) : strategyData(strategyData) {}

void Planets::reflect(bool copyGroups) {
  std::vector<uint32_t> planetIds(Constants::maxCoordinate * Constants::maxCoordinate, Constants::inf);
  for (const auto &planet: planets) {
    planetIds[size_t(std::abs(planet.x * Constants::maxCoordinate + planet.y))] = planet.id;
  }

  uint32_t newPlanetId = maxId;
  std::vector<model::Planet> newPlanets;
  for (const auto &planet: planets) {
    auto pairPlanetX = Constants::maxCoordinate - planet.x - 1;
    auto pairPlanetY = Constants::maxCoordinate - planet.y - 1;

    auto planetCoordinate = size_t(std::abs(pairPlanetX * Constants::maxCoordinate + pairPlanetY));
    if (planetIds[planetCoordinate] == Constants::inf) {
      model::Planet pairPlanet(newPlanetId, pairPlanetX, pairPlanetY, planet.harvestableResource, {}, {}, {});
      if (copyGroups) {
        for (const auto &group: planet.workerGroups) {
          auto pairGroup = group;
          pairGroup.playerIndex = uint32_t(2 * strategyData.team.teamPlayer.size() - group.playerIndex - 1);
          pairPlanet.workerGroups.emplace_back(pairGroup);
        }
        pairPlanet.building = planet.building;
      }

      newPlanets.emplace_back(pairPlanet);
      planetIds[planetCoordinate] = newPlanetId;
      ++newPlanetId;
    }
  }

  processPlanets(newPlanets);
}

void Planets::processPlanets(const std::vector<model::Planet> &newPlanets) {
  planetsNumber += uint32_t(newPlanets.size());

  for (const auto &planet: newPlanets) {
    if (planet.id >= maxId) {
      maxId = planet.id + 1;
    }
  }

  planets.resize(maxId);
  opponentRobotsAfterCombat.resize(maxId);

  for (const auto &planet: newPlanets) {
    planets[planet.id] = planet;

    int robotsNumber = 0;
    for (const auto &group: planet.workerGroups) {
      if (strategyData.team.teamPlayer.count(group.playerIndex) == 0) {
        robotsNumber -= int(group.number);
      } else {
        robotsNumber += int(group.number);
      }
    }

    if (robotsNumber >= 0) {
      opponentRobotsAfterCombat[planet.id] = 0;
    } else {
      opponentRobotsAfterCombat[planet.id] = uint32_t(-robotsNumber);
    }
  }
}

void Planets::newTick(const model::Game &game) {
  planetsNumber = 0;
  processPlanets(game.planets);
  /*if (strategyData.constants.viewDistance.has_value()) {
    reflect(strategyData.currentTick == 0);
  }*/
}

model::Planet Planets::operator[](uint32_t planetId) const {
  if (planetId >= planets.size()) {
    return {};
  } else {
    return planets.at(planetId);
  }
}

Planets::PlanetsIterator Planets::begin() {
  return Planets::PlanetsIterator(planets.begin(), planets.end());
}

Planets::PlanetsIterator Planets::end() {
  return Planets::PlanetsIterator(planets.end(), planets.end());
}

Planets::PlanetsIterator::PlanetsIterator(const std::vector<model::Planet>::iterator &newIterator,
                                          const std::vector<model::Planet>::iterator &newEnd) :
    iterator(newIterator), end(newEnd) {
  while (iterator != end && iterator->id >= Constants::inf) {
    ++iterator;
  }
}

model::Planet Planets::PlanetsIterator::operator*() const {
  return *iterator;
}

model::Planet &Planets::PlanetsIterator::operator*() {
  return *iterator;
}

Planets::PlanetsIterator &Planets::PlanetsIterator::operator++() {
  if (iterator != end) {
    ++iterator;
    while (iterator != end && iterator->id >= Constants::inf) {
      ++iterator;
    }
  }

  return *this;
}

Planets::PlanetsIterator Planets::PlanetsIterator::operator++(int) const {
  return ++PlanetsIterator(*this);
}

bool Planets::PlanetsIterator::operator==(const Planets::PlanetsIterator &second) const {
  return iterator == second.iterator;
}

bool Planets::PlanetsIterator::operator!=(const Planets::PlanetsIterator &second) const {
  return iterator != second.iterator;
}
