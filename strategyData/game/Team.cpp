//
// Created by nikita on 11/5/21.
//
#include "Team.h"
#include <strategyData/StrategyData.h>

void Team::newTick(const model::Game &game) {
  index = strategyData.constants.teamIndex;
  teamPlayerBySpecialty.clear();

  for (uint32_t playerIndex = 0; playerIndex < game.players.size(); ++playerIndex) {
    const auto &player = game.players[playerIndex];
    if (player.teamIndex == index) {
      teamPlayer.emplace(playerIndex);
      if (player.specialty.has_value()) {
        teamPlayerBySpecialty[player.specialty.value()] = playerIndex;
      }
    } else {
      opponentPlayer.emplace(playerIndex);
    }
  }

  for (auto specialty: {model::Specialty::PRODUCTION, model::Specialty::LOGISTICS, model::Specialty::COMBAT}) {
    if (teamPlayerBySpecialty.count(specialty) == 0) {
      if (strategyData.constants.specialtiesAllowed) {
        teamPlayerBySpecialty[specialty] = Constants::inf;
      } else {
        teamPlayerBySpecialty[specialty] = strategyData.constants.myIndex;
      }
    }
  }
}

Team::Team(StrategyData &strategyData) : strategyData(strategyData) {}
