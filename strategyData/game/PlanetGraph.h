//
// Created by nikita on 10/29/21.
//
#pragma once

#include <model/Planet.hpp>
#include "Constants.h"

struct Changes;
struct StrategyData;

struct PlanetGraph {
  static constexpr uint32_t infinity = Constants::inf / 4;
  static constexpr std::pair<int, int> infinityPoint = {infinity, infinity};

  uint32_t maxId = 0;
  std::vector<std::pair<int, int>> planetCoordinate;
  std::vector<std::vector<uint32_t>> neighbourPlanets;

  std::vector<std::vector<uint32_t>> realDistance;
  std::vector<std::vector<uint32_t>> nextPlanet;
  std::vector<std::vector<uint32_t>> rangedPlanets;
  StrategyData &strategyData;

  explicit PlanetGraph(StrategyData &strategyData);

  void newTick();

  void newTick(const std::vector<std::vector<uint32_t>> &additionalDistance);

  [[nodiscard]] uint32_t getDistance(uint32_t startId, uint32_t endId) const;

  static uint32_t getDistance(const std::pair<int, int> &startPlanet, const std::pair<int, int> &endPlanet);

  static uint32_t getDistance(const model::Planet &startPlanet, const model::Planet &endPlanet);

  [[nodiscard]] uint32_t getRealDistance(uint32_t startId, uint32_t endId) const;

  [[nodiscard]] uint32_t getKnownDistance(uint32_t startId, uint32_t endId) const;
};