//
// Created by nikita on 10/29/21.
//
#include "PlanetGraph.h"
#include <strategyData/StrategyData.h>
#include <log/Loggers.h>

PlanetGraph::PlanetGraph(StrategyData &strategyData) : strategyData(strategyData) {}

void PlanetGraph::newTick() {
  std::vector<std::vector<uint32_t>> additionalDistance(strategyData.planets.maxId, std::vector<uint32_t>(strategyData.planets.maxId));
  newTick(additionalDistance);
}

void PlanetGraph::newTick(const std::vector<std::vector<uint32_t>> &additionalDistance) {
  bool newMaxId = strategyData.planets.maxId != maxId;
  maxId = strategyData.planets.maxId;
  auto maxTravelDistance = strategyData.constants.maxTravelDistance;

  planetCoordinate.resize(maxId);
  for (uint32_t planetId = 0; planetId < planetCoordinate.size(); ++planetId) {
    const auto &planet = strategyData.planets[planetId];
    if (planet.id == planetId) {
      planetCoordinate[planetId] = {planet.x, planet.y};
    } else {
      planetCoordinate[planetId] = infinityPoint;
    }
  }

  if (newMaxId) {
    realDistance.resize(maxId, std::vector<uint32_t>(maxId));
    for (auto &row: realDistance) {
      row.resize(maxId);
    }
    nextPlanet.resize(maxId, std::vector<uint32_t>(maxId));
    for (auto &row: nextPlanet) {
      row.resize(maxId);
    }
  }
  Loggers::logger("std") << "newMaxId" << int(newMaxId) << "maxId" << maxId << "nextPlanet.size()" << nextPlanet.size();
  for (uint32_t planetId = 0; planetId < maxId; ++planetId) {
    const auto &planet = planetCoordinate[planetId];
    for (uint32_t secondPlanetId = 0; secondPlanetId < maxId; ++secondPlanetId) {
      nextPlanet[planetId][secondPlanetId] = secondPlanetId;
      if (planetId == secondPlanetId) {
        realDistance[planetId][secondPlanetId] = 0;
      } else {
        const auto &secondPlanet = planetCoordinate[secondPlanetId];
        realDistance[planetId][secondPlanetId] = getDistance(planet, secondPlanet);
        if (realDistance[planetId][secondPlanetId] > maxTravelDistance) {
          realDistance[planetId][secondPlanetId] = infinity;
          nextPlanet[planetId][secondPlanetId] = planetId;
        } else {
          realDistance[planetId][secondPlanetId] *= 2;
          realDistance[planetId][secondPlanetId] += additionalDistance[planetId][secondPlanetId];
        }
      }
    }
  }

  for (uint32_t k = 0; k < maxId; ++k) {
    for (uint32_t i = 0; i < maxId; ++i) {
      for (uint32_t j = 0; j < maxId; ++j) {
        if (realDistance[i][j] > realDistance[i][k] + realDistance[k][j]) {
          realDistance[i][j] = realDistance[i][k] + realDistance[k][j];
          nextPlanet[i][j] = nextPlanet[i][k];
        }
      }
    }
  }

  neighbourPlanets.resize(maxId);
  if (newMaxId) {
    rangedPlanets.resize(maxId, std::vector<uint32_t>(maxId - 1));
    for (auto &row: rangedPlanets) {
      row.resize(maxId - 1);
    }
  }
  rangedPlanets.resize(maxId, std::vector<uint32_t>(maxId - 1));
  std::vector<std::pair<uint32_t, uint32_t>> planetsWithDistances;
  planetsWithDistances.reserve(maxId - 1);
  for (uint32_t planetId = 0; planetId < maxId; ++planetId) {
    planetsWithDistances.clear();
    for (uint32_t secondPlanetId = 0; secondPlanetId < maxId; ++secondPlanetId) {
      if (planetId != secondPlanetId) {
        if (realDistance[planetId][secondPlanetId] < infinity) {
          realDistance[planetId][secondPlanetId] += additionalDistance[planetId][secondPlanetId];
          realDistance[planetId][secondPlanetId] /= 2;
          planetsWithDistances.emplace_back(realDistance[planetId][secondPlanetId], secondPlanetId);
        } else {
          realDistance[planetId][secondPlanetId] = infinity;
          planetsWithDistances.emplace_back(getDistance(planetId, secondPlanetId), secondPlanetId);
        }
      }
    }

    std::sort(planetsWithDistances.begin(), planetsWithDistances.end());

    rangedPlanets[planetId].clear();
    for (const auto &[planetDistance, secondPlanetId]: planetsWithDistances) {
      rangedPlanets[planetId].push_back(secondPlanetId);
      if (planetDistance <= maxTravelDistance) {
        neighbourPlanets[planetId].push_back(secondPlanetId);
      }
    }
  }
}

uint32_t PlanetGraph::getDistance(uint32_t startId, uint32_t endId) const {
  return getDistance(planetCoordinate[startId], planetCoordinate[endId]);
}

uint32_t PlanetGraph::getDistance(const std::pair<int, int> &startPlanet, const std::pair<int, int> &endPlanet) {
  return uint32_t(std::abs(startPlanet.first - endPlanet.first) + std::abs(startPlanet.second - endPlanet.second));
}

uint32_t PlanetGraph::getDistance(const model::Planet &startPlanet, const model::Planet &endPlanet) {
  return getDistance({startPlanet.x, startPlanet.y}, {endPlanet.x, endPlanet.y});
}

uint32_t PlanetGraph::getRealDistance(uint32_t startId, uint32_t endId) const {
  return realDistance[startId][endId];
}

uint32_t PlanetGraph::getKnownDistance(uint32_t startId, uint32_t endId) const {
  auto realDistanceValue = realDistance[startId][endId];
  if (realDistanceValue < infinity) {
    return realDistanceValue;
  } else {
    return getDistance(startId, endId);
  }
}
