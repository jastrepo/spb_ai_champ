//
// Created by nikita on 10/31/21.
//
#pragma once

#include <model/Planet.hpp>
#include <model/Game.hpp>

struct StrategyData;

struct Planets {
  class PlanetsIterator {
    std::vector<model::Planet>::iterator iterator, end;

    explicit PlanetsIterator(const std::vector<model::Planet>::iterator &newIterator,
                             const std::vector<model::Planet>::iterator &newEnd);

  public:
    model::Planet operator*() const;

    model::Planet &operator*();

    PlanetsIterator &operator++();

    PlanetsIterator operator++(int) const;

    bool operator==(const PlanetsIterator &second) const;

    bool operator!=(const PlanetsIterator &second) const;

    friend Planets;
  };

  StrategyData &strategyData;

  explicit Planets(StrategyData &strategyData);

  uint32_t maxId = 0;
  uint32_t planetsNumber = 0;
  std::vector<model::Planet> planets;
  std::vector<uint32_t> opponentRobotsAfterCombat;

  void reflect(bool copyGroups = false);

  void processPlanets(const std::vector<model::Planet> &newPlanets);

  void newTick(const model::Game &game);

  model::Planet operator[](uint32_t planetId) const;

  PlanetsIterator begin();

  PlanetsIterator end();
};