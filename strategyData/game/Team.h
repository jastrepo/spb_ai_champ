//
// Created by nikita on 11/5/21.
//
#pragma once

#include <set>
#include <model/Specialty.hpp>
#include "Constants.h"

struct StrategyData;

struct Team {
  uint32_t index = Constants::inf;
  std::set<uint32_t> teamPlayer, opponentPlayer;
  std::unordered_map<model::Specialty, uint32_t> teamPlayerBySpecialty;
  StrategyData &strategyData;

  explicit Team(StrategyData &strategyData);

  void newTick(const model::Game &game);
};