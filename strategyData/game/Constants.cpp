//
// Created by nikita on 11/3/21.
//

#include "Constants.h"

Constants::Constants(StrategyData &strategyData) : strategyData(strategyData) {}

void Constants::newTick(const model::Game &game) {
  myIndex = game.myIndex;
  teamIndex = game.players[myIndex].teamIndex;
  maxBuilders = game.maxBuilders;
  maxTravelDistance = game.maxTravelDistance;
  maxFlyingWorkerGroups = game.maxFlyingWorkerGroups;
  buildingProperties = game.buildingProperties;
  viewDistance = game.viewDistance;
  specialtiesAllowed = game.specialtiesAllowed;
}
